﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButton : MonoBehaviour
{
    float currentTime;
    Vector2 newScale = new Vector2(1f, 1f);
    void Update()
    {
        if (currentTime < 0.1f)
        {
            currentTime += Time.deltaTime;
            this.transform.localScale = Vector2.Lerp(this.transform.localScale, newScale, currentTime * 10);
        }
    }
    public void In()
    {
        currentTime = 0;
        newScale = new Vector2(1.05f, 1.05f);
    }
    public void Out()
    {
        currentTime = 0;
        newScale = new Vector2(1f, 1f);
    }
    public void Click()
    {
        this.transform.localScale = Vector2.one;
    }
}
