﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scroll : MonoBehaviour
{
    public ListMaker script;
    public int stage;
    public float currentTime = 0;
    public float delay = 2;
    public float origin;
    public float interval;
    readonly float reference = 88.35f;
    public Transform cam;
    public bool mouse = true;
    public bool moving = false;
    Image blur;
    private void Start()
    {
        blur = GameObject.Find("DrawBlur").GetComponent<Image>();
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
        stage = 1;
        currentTime = 2.1f;
        origin = cam.position.x;
        mouse = true;
        interval = reference / 19;
    }
    void Update()
    {
        if (mouse && !script.drawOn && !script.drawing)
        {
            if (Input.mouseScrollDelta.y < 0 && stage < 20)
            {
                if (currentTime < 0.5f && stage >= 18) { }
                else if (stage < script.domainScroll)
                {
                    if (script.audioOn)
                    {
                        script.sound2.clip = script.clips[0];
                        script.sound2.volume = script.volume * 1.5f;
                        script.sound2.Play();
                    }
                    stage++;
                    currentTime = 0;
                    moving = true;
                    if(stage == 19)
                    {
                        blur.sprite = Resources.Load<Sprite>("BlurRight");
                    }
                    else if (stage == 1)
                    {
                        blur.sprite = Resources.Load<Sprite>("Blur");
                    }
                }
            }
            else if (Input.mouseScrollDelta.y > 0 && stage > 0)
            {
                if (currentTime < 0.5f && stage <= 1) { }
                else
                {
                    if (script.audioOn)
                    {
                        script.sound2.clip = script.clips[0];
                        script.sound2.volume = script.volume * 1.5f;
                        script.sound2.Play();
                    }
                    currentTime = 0;
                    stage--;
                    moving = true;
                    if (stage == 0)
                    {
                        blur.sprite = Resources.Load<Sprite>("BlurLeft");
                    }
                    else if (stage == 18)
                    {
                        blur.sprite = Resources.Load<Sprite>("Blur");
                    }
                }
            }
        }
        if (stage < 20)
        {
            if (currentTime <= delay)
            {
                currentTime += Time.deltaTime;
                cam.position = new Vector3(Mathf.Lerp(cam.position.x, origin + (stage * interval), currentTime / delay), cam.position.y, cam.position.z);
            }
            else
            {
                moving = false;
                cam.position = new Vector3(origin + (stage * interval), cam.position.y, cam.position.z);
            }
        }
        else
        {
            if (currentTime <= delay)
            {
                currentTime += Time.deltaTime;
                cam.position = new Vector3(Mathf.Lerp(cam.position.x, origin + ((stage - 1) * interval), currentTime / delay), cam.position.y, cam.position.z);
            }
            else
            {
                moving = false;
                cam.position = new Vector3(origin + ((stage - 1) * interval), cam.position.y, cam.position.z);
            }
        }
    }
}