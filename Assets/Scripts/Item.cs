﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class Item
{
    public string Name;
    public string Type;
    public List<string> SubGenders;
    public List<int> SubRefs;
    public List<TextMeshProUGUI> SubTexts;
    public double Price;
    public int Total;
    public Transform Package;
    public Transform card;
    public int Era;
    public Sprite icon;
    public Item(Transform pack, string name, string type)
    {
        Package = pack;
        Name = name;
        Type = type;
    }
    public Item(string name, string type)
    {
        Name = name;
        Type = type;
    }
}
