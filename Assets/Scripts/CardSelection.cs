﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CardSelection : MonoBehaviour
{
    public ListMaker script;
    public Artist artist;
    public int domain;
    public double price;
    bool introSelected;
    public bool outOfDomain = false;
    CardSelection thisScript;
    public GameObject unlockText;
    public GameObject border;
    List<string> currents = new List<string>();
    float currentTime = 0.5f;
    Vector2 newScale = new Vector2(0.8f, 0.8f);
    private void Start()
    {
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
        thisScript = this.GetComponent<CardSelection>();
        if (artist.Currents.Count > 0)
        {
            if (artist.Currents[0] != "None")
            {
                for (int i = 0; i < artist.Currents.Count; i++)
                {
                    string basis = "";
                    for (int y = 0; y < script.effectList.Effect.Count; y++)
                    {
                        if (script.effectList.Effect[y].Name == artist.Currents[i])
                        {
                            basis = script.effectList.Effect[y].SubType;
                            break;
                        }
                    }
                    if (basis.Length >= 8)
                    {
                        int index = basis.Length - 8;
                        basis = basis.Remove(index);
                    }
                    bool same = false;
                    for (int y = 0; y < currents.Count; y++)
                    {
                        if (currents[y] == basis)
                        {
                            same = true;
                        }
                    }
                    if (!same)
                    {
                        currents.Add(basis);
                    }
                }
            }
        }
    }
    private void Update()
    {
        if (currentTime < 0.25f)
        {
            currentTime += Time.deltaTime;
            transform.localScale = Vector2.Lerp(transform.localScale, newScale, currentTime * 4);
        }
    }
    public void CardOver()
    {
        if (!script.drawing)
        {
            if (script.audioOn)
            {
                script.sound.clip = script.clips[3];
                script.sound.volume = script.volume * 0.25f;
                script.sound.Play();
            }
            currentTime = 0;
            newScale = new Vector2(0.85f, 0.85f);
            if (artist.Locked)
            {
                for (int i = 0; i < currents.Count; i++)
                {
                    for (int y = 0; y < script.activeItems.Count; y++)
                    {
                        if (script.activeItems[y].Name == currents[i])
                        {
                            currents.Remove(currents[i]);
                            i--;
                            break;
                        }
                    }
                }
                if (currents.Count == 1)
                {
                    script.panel.position = new Vector3(transform.position.x, transform.position.y + 3f, 1000);
                    script.panel.gameObject.SetActive(true);
                    script.panelTitle.text = "This Artist is locked";
                    script.panelText.text = "You must unlock " + currents[0] + " in the \"New Horizons\" Shop to make this Artist available. If it isn't in the Shop yet, you need to enroll younger Artists.";
                }
                else if (currents.Count == 2)
                {
                    script.panel.position = new Vector3(transform.position.x, transform.position.y + 3f, 1000);
                    script.panel.gameObject.SetActive(true);
                    script.panelTitle.text = "This Artist is locked";
                    script.panelText.text = "You must unlock " + currents[0] + " and " + currents[1] + " in the \"New Horizons\" Shop to make this Artist available. If these aren't in the Shop yet, you need to enroll younger Artists.";
                }
                else if (currents.Count == 3)
                {
                    script.panel.position = new Vector3(transform.position.x, transform.position.y + 3f, 1000);
                    script.panel.gameObject.SetActive(true);
                    script.panelTitle.text = "This Artist is locked";
                    script.panelText.text = "You must unlock " + currents[0] + " and " + currents[1] + " and " + currents[2] + " in the \"New Horizons\" Shop to make this Artist available. If these aren't in the Shop yet, you need to enroll younger Artists.";
                }
                else
                {
                    artist.Locked = false;
                }
            }
            if (!script.gameOn && !script.drawing)
            {
                unlockText.SetActive(true);
            }
            script.overDraw = true;
        }
    }
    public void CardOut()
    {
        currentTime = 0;
        newScale = new Vector2(0.8f, 0.8f);
        if (artist.Locked)
        {
            script.panel.gameObject.SetActive(false);
            script.panelTitle.text = "";
            script.panelText.text = "";
        }
        if (!script.gameOn && !script.drawing)
        {
            unlockText.SetActive(false);
        }
        script.overDraw = false;
    }
    public void CardClick()
    {
        if (script.gameOn)
        {
            if (script.drawOn)
            {
                if (script.stock >= price)
                {
                    if (!artist.Locked)
                    {
                        transform.localScale = new Vector2(0.7f, 0.7f);
                        script.stock -= price;
                        StartCoroutine(script.CardSelect(domain, artist, this.transform, outOfDomain));
                        thisScript.enabled = false;
                    }
                    else
                    {
                        print("Card Locked");
                        if (script.audioOn)
                        {
                            script.sound.clip = script.clips[7];
                            script.sound.volume = script.volume * 0.5f;
                            script.sound.Play();
                        }
                    }
                }
                else
                {
                    if (script.audioOn)
                    {
                        script.sound.clip = script.clips[7];
                        script.sound.volume = script.volume * 0.5f;
                        script.sound.Play();
                    }
                    script.panel.gameObject.SetActive(true);
                    script.panelTitle.text = "Not enough ames!";
                    if ((price - script.stock) > 1000000)
                    {
                        script.panelText.text = "You need " + Math.Round((price - script.stock) * 0.000001f) + "M more ames before you can enroll this Artist.";
                    }
                    else if ((price - script.stock) > 1000)
                    {
                        script.panelText.text = "You need " + Math.Round((price - script.stock) * 0.001f) + "K more ames before you can enroll this Artist.";
                    }
                    else
                    {
                        script.panelText.text = "You need " + Math.Round((price - script.stock)) + " more ames before you can enroll this Artist.";
                    }
                    script.panel.position = new Vector3(transform.position.x, transform.position.y + 3f, 1000);
                }
            }
        }
        else if (script.firstDrawOn)
        {
            if (script.audioOn)
            {
                script.sound.clip = script.clips[13];
                script.sound.volume = script.volume;
                script.sound.Play();
            }
            StartCoroutine(script.Beginning2(artist, this.transform));
            unlockText.SetActive(false);
        }
        else if (script.secondDrawOn)
        {
            if (!introSelected)
            {
                if (script.audioOn)
                {
                    script.sound.clip = script.clips[13];
                    script.sound.volume = script.volume;
                    script.sound.Play();
                }
                script.selected.Add(artist);
                border.SetActive(true);
                introSelected = true;
            }
            else
            {
                if (script.audioOn)
                {
                    script.sound.clip = script.clips[7];
                    script.sound.volume = script.volume * 0.5f;
                    script.sound.Play();
                }
                script.selected.Remove(artist);
                border.SetActive(false);
                introSelected = false;
            }
        }
    }
}
