﻿using System.Collections.Generic;

[System.Serializable]
public class EffectList
{
    public List<Effect> Effect = new List<Effect>();
}
