﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItMouseDetector : MonoBehaviour
{
    ListMaker script;
    RectTransform detector;
    Vector2 origin;
    Vector2 originScale;
    Vector2 partlyOpen;
    Vector2 partlyOpenScale;
    float closedPos = 9.5f;
    float openPos = -72;
    public BoxMouse box;
    public Transform nextCard;
    private bool mouseOver = false;
    private bool open = false;
    float currentTime;
    public RectTransform background;
    float backOrigin = 65;
    public bool small = false;
    void Awake()
    {
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
        detector = this.gameObject.GetComponent<RectTransform>();
        origin = new Vector2(0, 37.3f);
        originScale = new Vector2(115, 14);
        partlyOpen = new Vector2(0, 0);
        partlyOpenScale = new Vector2(115, 88.7f);
    }
    public void Small()
    {
        origin = new Vector2(0, 11.64999f);
        originScale = new Vector2(115, 12.7f);
        partlyOpen = new Vector2(0, 0);
        partlyOpenScale = new Vector2(115, 36.17F);
        openPos = -50;
        closedPos = -15;
        detector.localPosition = origin;
        detector.sizeDelta = new Vector2(originScale.x, originScale.y);
        nextCard.localPosition = new Vector2(nextCard.localPosition.x, closedPos);
        small = true;
    }
    public void PointerIn()
    {
        if (!script.drawOn)
        {
            if (script.audioOn)
            {
                script.sound.clip = script.clips[3];
                script.sound.volume = script.volume * 0.75f;
                script.sound.Play();
            }
            mouseOver = true;
            if (!open && !box.moving)
            {
                currentTime = 0;
                StartCoroutine(PointerInCo());
            }
        }
    }
    public void PointerOut()
    {
        mouseOver = false;
        if (!open)
        {
            currentTime = 0;
            StartCoroutine(PointerOutCo());
        }
    }
    public void PointerClick()
    {
        if (!open && !script.drawOn)
        {
            if (script.audioOn)
            {
                script.sound.clip = script.clips[13];
                script.sound.volume = script.volume;
                script.sound.Play();
            }
            open = true;
        }
        else if (!script.drawOn)
        {
            if (script.audioOn)
            {
                script.sound.clip = script.clips[4];
                script.sound.volume = 0.075f;
                script.sound.Play();
            }
            open = false;
        }
    }
    public IEnumerator PointerInCo()
    {
        while (currentTime < 0.5f && mouseOver && !open)
        {
            currentTime += Time.deltaTime;
            if (!small)
            {
                background.sizeDelta = new Vector2(background.sizeDelta.x, Mathf.Lerp(background.sizeDelta.y, 0, currentTime * 2));
            }
            detector.localPosition = Vector2.Lerp(detector.localPosition, partlyOpen, currentTime * 2);
            detector.sizeDelta = new Vector2(originScale.x, Mathf.Lerp(detector.sizeDelta.y, partlyOpenScale.y, currentTime * 2));
            nextCard.localPosition = Vector2.Lerp(nextCard.localPosition, new Vector2(nextCard.localPosition.x, openPos), currentTime * 2);
            yield return null;
        }
        yield break;
    }
    public IEnumerator PointerOutCo()
    {
        while (currentTime < 0.5f && !mouseOver && !open)
        {
            currentTime += Time.deltaTime;
            if (!small)
            {
                background.sizeDelta = new Vector2(background.sizeDelta.x, Mathf.Lerp(background.sizeDelta.y, backOrigin, currentTime * 2));
            }
            detector.localPosition = Vector2.Lerp(detector.localPosition, origin, currentTime * 2);
            detector.sizeDelta = new Vector2(originScale.x, Mathf.Lerp(detector.sizeDelta.y, originScale.y, currentTime * 2));
            nextCard.localPosition = Vector2.Lerp(nextCard.localPosition, new Vector2(nextCard.localPosition.x, closedPos), currentTime * 2);
            yield return null;
        }
        yield break;
    }
}
