﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopScroll : MonoBehaviour
{
    private ListMaker script;
    private Scroll scroll;
    private ScrollRect rect;
    private RectTransform size;
    void Start()
    {
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
        size = this.transform.Find("ItemSort").GetComponent<RectTransform>();
        scroll = GameObject.Find("DomainSort").GetComponent<Scroll>();
    }
    private void Update()
    {
        if (script.newItems.Count > 0)
        {
            if (script.availableItems.Count > 6)
            {
                size.sizeDelta = new Vector2(64.75f, 70f);
                size.localPosition = new Vector2(0, 0);
            }
            else if (script.availableItems.Count > 9)
            {
                size.sizeDelta = new Vector2(64.75f, 70f);
                size.localPosition = new Vector2(0, 0);
            }
            else
            {
                size.sizeDelta = new Vector2(64.75f, 42.98f);
                size.localPosition = new Vector2(0, -23f);
            }
        }
    }
    public void In()
    {
        scroll.mouse = false;
    }
    public void Out()
    {
        scroll.mouse = true;
    }
    public void TitleIn()
    {
        script.panel.gameObject.SetActive(true);
        script.panelTitle.text = "Currents";
        script.panelText.text = "Unlocking a new artistic Current allows you to enroll any Artist who depend on it, unless they also depend on another Current which is still locked.";
        script.panelDisplay = true;
    }
    public void TitleOut()
    {
        script.panel.gameObject.SetActive(false);
        script.panelTitle.text = "";
        script.panelText.text = "";
        script.panelDisplay = false;
    }
}
