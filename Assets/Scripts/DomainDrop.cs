﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DomainDrop : MonoBehaviour
{
    ListMaker script;
    public int domain;
    Scroll scroll;
    void Start()
    {
        scroll = GameObject.Find("DomainSort").GetComponent<Scroll>();
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
    }
    public void MouseIn()
    {
        script.DomainDropOn(domain);
        if (!script.drawOn && script.displayOn && !script.drawing && !scroll.moving)
        {
            script.panel.gameObject.SetActive(true);
            script.panelText.fontSize = 5f;
            script.panelTitle.text = "Domain's Trendsetter";
            if (script.domainProduction[domain] > 1000000000)
            {
                script.panelText.text = "Drag and drop an Artist's name here to have their Effects enhanced for each other enrolled Artist in the Domain.\n\n<b>Current Domain Production:</b> " + Math.Round(script.domainProduction[domain] * 0.000000001f, 1) + "B";
            }
            else if (script.domainProduction[domain] > 1000000)
            {
                script.panelText.text = "Drag and drop an Artist's name here to have their Effects enhanced for each other enrolled Artist in the Domain.\n\n<b>Current Domain Production:</b> " + Math.Round(script.domainProduction[domain] * 0.000001f, 1) + "M";
            }
            else if (script.domainProduction[domain] > 1000)
            {
                script.panelText.text = "Drag and drop an Artist's name here to have their Effects enhanced for each other enrolled Artist in the Domain.\n\n<b>Current Domain Production:</b> " + Math.Round(script.domainProduction[domain] * 0.001f, 1) + "K";
            }
            else
            {
                script.panelText.text = "Drag and drop an Artist's name here to have their Effects enhanced for each other enrolled Artist in the Domain.\n\n<b>Current Domain Production:</b> " + Math.Round(script.domainProduction[domain], 1);
            }
            script.panelDisplay = true;
        }
    }
    public void MouseOut()
    {
        script.DomainDropOff();
        script.panel.gameObject.SetActive(false);
        script.panelTitle.text = "";
        script.panelText.text = "";
        script.panelDisplay = false;
    }
}
