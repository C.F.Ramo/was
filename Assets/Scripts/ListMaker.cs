﻿using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class ListMaker : MonoBehaviour
{
    // Setup Artists
    public ArtistList artistList = new ArtistList();
    public List<ArtistList> artistDomainLocked = new List<ArtistList>();
    public List<ArtistList> artistDomainAvailable = new List<ArtistList>();
    public ArtistList shamesAvailable = new ArtistList();
    public List<ArtistList> artistDomainActive = new List<ArtistList>();
    public Color[] domainColors = new Color[22];
    public Color colorMalus;
    public Color colorMalusCard;
    public Color colorBonusCard;
    public Color colorMalusEffect;
    public Color colorMalusEffect2;
    public DomainShop[] domainShops = new DomainShop[20];
    public List<string> domain = new List<string>();
    public List<string> domainShortened = new List<string>();
    public List<Sprite> flags = new List<Sprite>();
    public Sprite genderF;
    public Sprite genderM;
    public Sprite genderNB;
    // Setup Maluses
    public ArtistList shamefulInfluenceLocked = new ArtistList();
    public ArtistList[] shamefulInfluenceAvailable = new ArtistList[20];
    public ArtistList shamefulInfluenceActive = new ArtistList();
    // Setup Effects
    public EffectList effectList = new EffectList();
    readonly Color[] effectColors = new Color[13];
    // Setup News
    public TextMeshProUGUI newsText;
    public Transform transformNews;
    public NewsList newsList = new NewsList();
    public NewsList newsListAvailable = new NewsList();
    public int newsEra = 1;
    float newsTime = 27;
    int newsRef;
    bool newsChange;
    int newsPlay;
    bool beginning = true;
    // Navigation
    public RectTransform scrollList;
    TextMeshProUGUI[] scrollTexts = new TextMeshProUGUI[22];
    public RectTransform domainList;
    public RectTransform scrollButton;
    public List<RectTransform> cardBoxes = new List<RectTransform>();
    public RectTransform cardBoxShame;
    public RectTransform cardBoxItems;
    public GameObject cardBoxOther;
    public GameObject cardBox;
    public GameObject card;
    public GameObject effect;
    public GameObject nation;
    public int domainScroll = 1;
    // Statistics Count
    public List<Attribute> nations = new List<Attribute>();
    public List<Attribute> areas = new List<Attribute>();
    public List<Attribute> tags = new List<Attribute>();
    public List<Attribute> currents = new List<Attribute>();
    public int nationCount;
    public int women;
    public int men;
    public int feminists;
    public int antiracists;
    public int activists;
    public int currentCount;
    public float available;
    public double productionHistory;
    public float erasPassed;
    public double[] domainProduction = new double[20];
    // Statistics Display
    public TextMeshProUGUI productionDisplay;
    public TextMeshProUGUI stockDisplay;
    public TextMeshProUGUI stockDisplayText;
    public List<TextMeshProUGUI> statisticTexts = new List<TextMeshProUGUI>();
    public CanvasGroup statsDisplay;
    public CanvasGroup gaugeDisplay;
    public TextMeshProUGUI gaugeText;
    public TextMeshProUGUI gaugeTitle;
    // Production Maths
    public float globalMultiplicatorEffect;
    public float[] domainMultiplicators = new float[20];
    public float[] domainEffectMultiplicator = new float[20];
    public double production;
    public double stock;
    public float domainDefined;
    readonly float amountVersusTotal = 0.2f;
    readonly float multEffectiveness = 0.3f;
    float eraMultiplicator = 1;
    float eraFactor = 1;
    // Production Jauge
    int currentGauge = 1;
    public float globalMultiplicator;
    float globalOrigin = 0.5f;
    public float globalMultiplicatorEffectiveness;
    public float imperialRatio;
    public float patriarcalRatio;
    public float heteroRatio;
    public float justiceRatio;
    public float marginalizedMultiplicator;
    public float womenMultiplicator;
    public float queerMultiplicator;
    float gaugeTime;
    public List<Shame> shames = new List<Shame>();
    public Transform gauge;
    public float gaugeSize = 0;
    public Image gaugeIsUp;
    public Image gaugeIsDown;
    // Card price Maths
    readonly float[] priceModifiers = new float[20];
    public float globalPriceModifier = 1;
    // DrawCard
    public Transform cardPlace;
    public List<Transform>[] cards = new List<Transform>[20];
    public bool drawing = false;
    public bool drawOn = false;
    public int domainDraw;
    public int drawRef;
    public bool overDraw = false;
    public int[] drawSuspended = new int[20];
    public CanvasGroup blur;
    // Card Drags
    public Transform[] domainDrops = new Transform[20];
    public List<Transform> headerDrops = new List<Transform>();
    public GameObject trendSlot;
    public Transform slotsList;
    public Artist[] leadArtists = new Artist[20];
    public Artist[] supremeLeadArtists = new Artist[5];
    public int cardDomain;
    public int domainDrop;
    public int headerDrop;
    public bool DomainDropAvailable = false;
    public bool HeaderDropAvailable = false;
    public bool drag = false;
    // Store
    public Transform storeFront;
    public string[] currentNames = new string[13];
    public int[] currentEras = new int[13];
    public Sprite[] currentIcons = new Sprite[13];
    public MajorCurrent[] majorCurrents = new MajorCurrent[13];
    public UnityEngine.Object storeItem;
    public List<Item> availableItems;
    public List<Item> activeItems = new List<Item>();
    public List<CanvasGroup> newItems = new List<CanvasGroup>();
    // Bought Items
    public GameObject smallCurrent;
    public GameObject bigCurrent;
    public GameObject SubCurrent;
    // Transitions
    public List<GameObject> trendSlots = new List<GameObject>();
    public CanvasGroup transBackground;
    public CanvasGroup transPanel;
    public CanvasGroup transClick;
    public CanvasGroup transParagraph;
    public TextMeshProUGUI transTitle;
    public TextMeshProUGUI transSubTitle;
    public TextMeshProUGUI transText;
    public SocietyStage[] societies = new SocietyStage[13];
    public TextMeshProUGUI name1;
    public TextMeshProUGUI name2;
    int currentSociety = 0;
    bool click = false;
    public bool endAvailable = false;
    // Introduction
    public GameObject border;
    public GameObject unlockText;
    public CanvasGroup validate;
    GameObject[] borders;
    Transform[] firstCards;
    Transform[] secondCards;
    public List<Artist> selected = new List<Artist>();
    public bool gameOn = false;
    public bool firstDrawOn = false;
    public bool secondDrawOn = false;
    public List<CanvasGroup> introPanels = new List<CanvasGroup>();
    public CanvasGroup introMenu;
    public GameObject highLight;
    bool infoOn = true;
    bool introButton = false;
    bool goingOn = true;
    bool reversing = false;
    float timeIntro;
    int infoRef;
    [TextAreaAttribute] public List<string> introTexts = new List<string>();
    // Explanations
    public TextMeshProUGUI panelText;
    public TextMeshProUGUI panelTitle;
    public RectTransform panel;
    public RectTransform nationPanel;
    public TextMeshProUGUI nationText;
    public bool panelDisplay;
    public bool displayOn = true;
    // Menu
    public TextMeshProUGUI soundText;
    public TextMeshProUGUI displayText;
    public CanvasGroup prodStats;
    public CanvasGroup menu;
    public CanvasGroup confirm;
    public TextMeshProUGUI confirmText;
    public bool menuOn = false;
    public bool confirmOn = false;
    public int choice;
    public Transform menuButton;
    // Save Game
    public Save save;
    bool loading = false;
    // Sounds
    public bool audioOn = true;
    public float volume = 1f;
    public AudioSource sound;
    public AudioSource sound2;
    public AudioClip[] clips = new AudioClip[14];
    void Start()
    {
        newsEra = 1;
        // Set Save
        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
            Save oldSave = (Save)bf.Deserialize(file);
            file.Close();
            if (oldSave.loadOn)
            {
                loading = true;
                save = oldSave;
            }
            else
            {
                loading = false;
                save = new Save();
                save.loadOn = false;
            }
        }
        else
        {
            loading = false;
            save = new Save();
            save.loadOn = false;
        }
        // Set Sound
        sound = this.GetComponent<AudioSource>();
        sound2 = Camera.main.GetComponent<AudioSource>();
        // Set Colors
        float blueColorDiv = 1f / 35;
        for (int i = 0; i < domainColors.Length - 2; i++)
        {
            domainColors[i] = Color.HSVToRGB(0.4f + blueColorDiv * i, 0.5f, 0.5f);
        }
        domainColors[20] = colorMalus;
        domainColors[21] = colorMalus;
        float colorDiv = 1f / effectColors.Length;
        for (int i = 0; i < effectColors.Length; i++)
        {
            effectColors[i] = Color.HSVToRGB(colorDiv * i, 0.3f, 0.9f);
        }
        // Read List
        TextAsset DB = Resources.Load("Artist") as TextAsset;
        TextAsset DBS = Resources.Load("Shameful") as TextAsset;
        TextAsset DBE = Resources.Load("Effect") as TextAsset;
        TextAsset DBN = Resources.Load("News") as TextAsset;
        artistList = JsonUtility.FromJson<ArtistList>(DB.text);
        shamefulInfluenceLocked = JsonUtility.FromJson<ArtistList>(DBS.text);
        effectList = JsonUtility.FromJson<EffectList>(DBE.text);
        newsList = JsonUtility.FromJson<NewsList>(DBN.text);
        for (int i = 0; i < shamefulInfluenceAvailable.Length; i++)
        {
            shamefulInfluenceAvailable[i] = new ArtistList();
        }
        // Prepare Stats
        for (int i = 0; i < majorCurrents.Length; i++)
        {
            majorCurrents[i] = new MajorCurrent
            {
                SubCurrents = new List<int>(),
                Name = currentNames[i]
            };
        }
        for (int i = 0; i < artistList.Artist.Count; i++)
        {
            bool[] verifs = new bool[majorCurrents.Length];
            for (int y = 0; y < verifs.Length; y++)
            {
                verifs[y] = false;
            }
            for (int x = 0; x < artistList.Artist[i].Currents.Count; x++)
            {
                bool same = false;
                for (int w = 0; w < currents.Count; w++)
                {
                    if (artistList.Artist[i].Currents[x] == currents[w].name)
                    {
                        same = true;
                        currents[w].total++;
                        artistList.Artist[i].currentRefs.Add(w);
                        break;
                    }
                }
                if (!same && artistList.Artist[i].Currents[x] != "None")
                {
                    currents.Add(new Attribute(artistList.Artist[i].Currents[x], 1));
                    artistList.Artist[i].currentRefs.Add(currents.Count - 1);
                }
                for (int w = 0; w < effectList.Effect.Count; w++)
                {
                    if (artistList.Artist[i].Currents[x] == effectList.Effect[w].Name)
                    {
                        for (int y = 0; y < majorCurrents.Length; y++)
                        {
                            if (effectList.Effect[w].SubType == majorCurrents[y].Name + " Current")
                            {
                                if (verifs[y])
                                {
                                    majorCurrents[y].Doubles++;
                                }
                                verifs[y] = true;
                                if (!effectList.Effect[w].Added)
                                {
                                    for (int z = 0; z < currents.Count; z++)
                                    {
                                        if (currents[z].name == effectList.Effect[w].Name)
                                        {
                                            majorCurrents[y].SubCurrents.Add(z);
                                            break;
                                        }
                                    }
                                    effectList.Effect[w].Added = true;
                                }
                            }
                        }
                        break;
                    }
                }
            }
            for (int x = 0; x < artistList.Artist[i].Effects.Count; x++)
            {
                bool found = false;
                for (int w = 0; w < effectList.Effect.Count; w++)
                {
                    if (artistList.Artist[i].Effects[x] == effectList.Effect[w].Name)
                    {
                        artistList.Artist[i].effectRefs.Add(w);
                        found = true;
                    }
                }
                if (!found)
                {
                    print("effect not written " + artistList.Artist[i].Effects[x]);
                }
            }
            for (int x = 0; x < artistList.Artist[i].Tags.Count; x++)
            {
                bool same = false;
                for (int w = 0; w < tags.Count; w++)
                {
                    if (artistList.Artist[i].Tags[x] == tags[w].name)
                    {
                        same = true;
                        tags[w].total++;
                        artistList.Artist[i].tagRefs.Add(w);
                        break;
                    }
                }
                if (!same && artistList.Artist[i].Tags[x] != "None")
                {
                    tags.Add(new Attribute(artistList.Artist[i].Tags[x], 1));
                    artistList.Artist[i].tagRefs.Add(tags.Count - 1);
                }
            }
            for (int x = 0; x < artistList.Artist[i].Nation.Count; x++)
            {
                bool same = false;
                for (int w = 0; w < nations.Count; w++)
                {
                    if (artistList.Artist[i].Nation[x] == nations[w].name)
                    {
                        same = true;
                        nations[w].total++;
                        artistList.Artist[i].nationRefs.Add(w);
                        break;
                    }
                }
                if (!same && artistList.Artist[i].Nation[x] != "None")
                {
                    nations.Add(new Attribute(artistList.Artist[i].Nation[x], 1));
                    artistList.Artist[i].nationRefs.Add(nations.Count - 1);
                }
            }
        }
        for (int i = 0; i < majorCurrents.Length; i++)
        {
            for (int y = 0; y < majorCurrents[i].SubCurrents.Count; y++)
            {
                majorCurrents[i].Total += currents[majorCurrents[i].SubCurrents[y]].total;
            }
            majorCurrents[i].Total -= majorCurrents[i].Doubles;
        }
        for (int i = 0; i < shamefulInfluenceLocked.Artist.Count; i++)
        {
            bool same = false;
            for (int w = 0; w < shames.Count; w++)
            {
                if (shamefulInfluenceLocked.Artist[i].Shame == shames[w].name)
                {
                    same = true;
                    shames[w].total++;
                    break;
                }
            }
            if (!same)
            {
                shames.Add(new Shame(shamefulInfluenceLocked.Artist[i].Shame, 1));
                shamefulInfluenceLocked.Artist[i].currentRefs.Add(shames.Count - 1);
            }
        }
        areas.Add(new Attribute("Africa", 0));
        areas.Add(new Attribute("South America", 0));
        areas.Add(new Attribute("Central America", 0));
        areas.Add(new Attribute("North America", 0));
        areas.Add(new Attribute("Eastern Europe", 0));
        areas.Add(new Attribute("Western Europe", 0));
        areas.Add(new Attribute("Middle East", 0));
        areas.Add(new Attribute("Southern Asia", 0));
        areas.Add(new Attribute("Eastern Asia", 0));
        for (int x = 0; x < nations.Count; x++)
        {
            if (nations[x].name == "Nigeria" ||
                nations[x].name == "South Africa" ||
                nations[x].name == "Algeria" ||
                nations[x].name == "Morocco" ||
                nations[x].name == "Kenya" ||
                nations[x].name == "Ethiopia" ||
                nations[x].name == "Angola" ||
                nations[x].name == "Ghana" ||
                nations[x].name == "Tanzania" ||
                nations[x].name == "Cameroon" ||
                nations[x].name == "Tunisia" ||
                nations[x].name == "Ugandda" ||
                nations[x].name == "Senegal" ||
                nations[x].name == "Zambia" ||
                nations[x].name == "Zimbabwe" ||
                nations[x].name == "Mali" ||
                nations[x].name == "Gabon" ||
                nations[x].name == "Burkina Faso" ||
                nations[x].name == "Mozambique" ||
                nations[x].name == "Namibia" ||
                nations[x].name == "Congo" ||
                nations[x].name == "Somalia" ||
                nations[x].name == "Mauritania" ||
                nations[x].name == "Liberia" ||
                nations[x].name == "Cape Verde"
                )
            {
                nations[x].area = 0;
                areas[0].total += nations[x].total;
            }
            else if (nations[x].name == "Argentina" ||
                nations[x].name == "Bolivia" ||
                nations[x].name == "Brazil" ||
                nations[x].name == "Chile" ||
                nations[x].name == "Colombia" ||
                nations[x].name == "Ecuador" ||
                nations[x].name == "Guyana" ||
                nations[x].name == "Paraguay" ||
                nations[x].name == "Peru" ||
                nations[x].name == "Uruguay" ||
                nations[x].name == "Suriname" ||
                nations[x].name == "Venezuela"
                )
            {
                nations[x].area = 1;
                areas[1].total += nations[x].total;
            }
            else if (nations[x].name == "Barbados" ||
                nations[x].name == "Cayman Islands" ||
                nations[x].name == "Mexico" ||
                nations[x].name == "Costa Rica" ||
                nations[x].name == "Cuba" ||
                nations[x].name == "Dominica" ||
                nations[x].name == "Guatemala" ||
                nations[x].name == "Haiti" ||
                nations[x].name == "Jamaica" ||
                nations[x].name == "Martinique" ||
                nations[x].name == "Nicaragua" ||
                nations[x].name == "Puerto Rico"
                )
            {
                nations[x].area = 2;
                areas[2].total += nations[x].total;
            }
            else if (nations[x].name == "Canada" ||
                nations[x].name == "United States"
                )
            {
                nations[x].area = 3;
                areas[3].total += nations[x].total;
            }
            else if (nations[x].name == "Estonia" ||
                nations[x].name == "Latvia" ||
                nations[x].name == "Lithuania" ||
                nations[x].name == "Belarus" ||
                nations[x].name == "Russia" ||
                nations[x].name == "Ukraine" ||
                nations[x].name == "Romania" ||
                nations[x].name == "Czechia" ||
                nations[x].name == "Croatia" ||
                nations[x].name == "Serbia" ||
                nations[x].name == "Slovakia" ||
                nations[x].name == "Slovenia" ||
                nations[x].name == "Yugoslavia" ||
                nations[x].name == "Albania" ||
                nations[x].name == "Bulgaria" ||
                nations[x].name == "Greece" ||
                nations[x].name == "Macedonia" ||
                nations[x].name == "Austria" ||
                nations[x].name == "Hungary" ||
                nations[x].name == "Poland" ||
                nations[x].name == "Austria-Hungary"
                )
            {
                nations[x].area = 4;
                areas[4].total += nations[x].total;
            }
            else if (nations[x].name == "Belgium" ||
                nations[x].name == "Denmark" ||
                nations[x].name == "Finland" ||
                nations[x].name == "Germany" ||
                nations[x].name == "Iceland" ||
                nations[x].name == "Ireland" ||
                nations[x].name == "Italy" ||
                nations[x].name == "Luxembourg" ||
                nations[x].name == "Netherlands" ||
                nations[x].name == "Norway" ||
                nations[x].name == "Portugal" ||
                nations[x].name == "Spain" ||
                nations[x].name == "Sweden" ||
                nations[x].name == "Switzerland" ||
                nations[x].name == "England" ||
                nations[x].name == "Wales" ||
                nations[x].name == "Scotland" ||
                nations[x].name == "France"
                )
            {
                nations[x].area = 5;
                areas[5].total += nations[x].total;
            }
            else if (nations[x].name == "Kyrgyzstan" ||
                nations[x].name == "Armenia" ||
                nations[x].name == "Azerbaijan" ||
                nations[x].name == "Iran" ||
                nations[x].name == "Georgia" ||
                nations[x].name == "Egypt" ||
                nations[x].name == "Iraq" ||
                nations[x].name == "Israel" ||
                nations[x].name == "Palestine" ||
                nations[x].name == "Lebanon" ||
                nations[x].name == "Pakistan" ||
                nations[x].name == "Syria" ||
                nations[x].name == "Turkey" ||
                nations[x].name == "Yemen"
                )
            {
                nations[x].area = 6;
                areas[6].total += nations[x].total;
            }
            else if (nations[x].name == "Bangladesh" ||
                nations[x].name == "Cambodia" ||
                nations[x].name == "India" ||
                nations[x].name == "Indonesia" ||
                nations[x].name == "Laos" ||
                nations[x].name == "Malaysia" ||
                nations[x].name == "Philippines" ||
                nations[x].name == "Singapore" ||
                nations[x].name == "Sri Lanka" ||
                nations[x].name == "Thailand" ||
                nations[x].name == "Vietnam" ||
                nations[x].name == "Australia" ||
                nations[x].name == "New Zealand" ||
                nations[x].name == "Yogyakarta"
                )
            {
                nations[x].area = 7;
                areas[7].total += nations[x].total;
            }
            else if (nations[x].name == "China" ||
                nations[x].name == "Korea" ||
                nations[x].name == "North Korea" ||
                nations[x].name == "Japan" ||
                nations[x].name == "Taiwan" ||
                nations[x].name == "Hong Kong"
                )
            {
                nations[x].area = 8;
                areas[8].total += nations[x].total;
            }
        }
        // Set Mathematical Values
        available = 0;
        globalMultiplicatorEffectiveness = globalOrigin;
        gauge.localScale = new Vector2(1, 0);
        gaugeIsDown.enabled = false;
        gaugeIsUp.enabled = false;
        // Divide List
        for (int i = 0; i < domain.Count; i++)
        {
            artistDomainLocked.Add(new ArtistList());
            artistDomainAvailable.Add(new ArtistList());
            artistDomainActive.Add(new ArtistList());
            if (!loading)
            {
                save.artistDomainActive.Add(new List<string>());
                save.isArtistLead.Add(new List<int>());
                save.eraList.Add(new int());
            }
            for (int y = 0; y < artistList.Artist.Count; y++)
            {
                if (artistList.Artist[y].Domain == domain[i])
                {
                    artistDomainLocked[i].Artist.Add(artistList.Artist[y]);
                }
            }
            artistDomainLocked[i].name = artistDomainLocked[i].Artist[0].Domain;
            artistDomainAvailable[i].name = artistDomainLocked[i].Artist[0].Domain;
            artistDomainActive[i].name = artistDomainLocked[i].Artist[0].Domain;
        }
        // Create Cardboxes
        List<RectTransform> motherCardBoxes = new List<RectTransform>();
        for (int i = 0; i < domain.Count + 2; i++)
        {
            if (i == 0)
            {
                RectTransform newBox = GameObject.Instantiate(cardBoxOther, domainList).GetComponent<RectTransform>();
                Transform background = newBox.Find("Background");
                background.GetComponent<Image>().color = domainColors[21];
                cardBoxItems = background.Find("CardSort").GetComponent<RectTransform>();
                TextMeshProUGUI domainText = newBox.Find("CardBox Shop").Find("Text").GetComponent<TextMeshProUGUI>();
                domainText.text = "Here are the major Currents\nyou have unlocked.";
                domainText.fontSize = 7;
                domainText.alignment = TextAlignmentOptions.Center;
                newBox.Find("CardBox Shop").Find("DomainTitle").GetComponent<TextMeshProUGUI>().text = "Horizons";
            }
            else if (i < domain.Count + 1)
            {
                motherCardBoxes.Add(GameObject.Instantiate(cardBox, domainList).GetComponent<RectTransform>());
                Transform background = motherCardBoxes[i - 1].Find("Background");
                cardBoxes.Add(background.Find("CardSort").GetComponent<RectTransform>());
                background.GetComponent<Image>().color = domainColors[i - 1];
                Transform domainShop = motherCardBoxes[i - 1].Find("CardBox Shop").Find("CardSort");
                domainShop.GetComponent<DomainShop>().domain = i - 1;
                domainShop.parent.Find("DomainName").GetComponent<TextMeshProUGUI>().text = artistDomainAvailable[i - 1].name;
                Transform image = domainShop.GetChild(0).Find("Dark");
                float H;
                Color.RGBToHSV(domainColors[i - 1], out H, out _, out _);
                image.GetComponent<Image>().color = Color.HSVToRGB(H, 0.3f, 0.9f);
                image.GetChild(0).GetComponent<Image>().color = Color.HSVToRGB(H, 0.2f, 0.9f);
                Transform dropBox = domainShop.Find("Card (2)");
                dropBox.GetComponent<DomainDrop>().domain = i - 1;
                domainDrops[i - 1] = dropBox;
                domainShops[i - 1] = domainShop.GetComponent<DomainShop>();
            }
            else
            {
                RectTransform newBox = GameObject.Instantiate(cardBoxOther, domainList).GetComponent<RectTransform>();
                Transform background = newBox.Find("Background");
                background.GetComponent<Image>().color = domainColors[20];
                cardBoxShame = background.Find("CardSort").GetComponent<RectTransform>();
                newBox.Find("CardBox Shop").Find("DomainTitle").GetComponent<TextMeshProUGUI>().text = "Shameful Influences";
                newBox.Find("CardBox Shop").Find("Text").GetComponent<TextMeshProUGUI>().text = "These artists have exerted a substantial influence on their respective domains, but they also have been substantial scumbags. Their ugliness is now part of the art world, which grows uglier and uglier with them, and its influence declines.";
            }
        }
        for (int i = 0; i < domain.Count; i++)
        {
            domainShops[i].price = (i * (i + 1)) * 1000;
        }
        // Fill Shop
        for (int i = 0; i < majorCurrents.Length; i++)
        {
            majorCurrents[i].Era = currentEras[i];
            if (!loading)
            {
                if (majorCurrents[i].Era <= newsEra)
                {
                    GameObject newItem = (GameObject)Instantiate(storeItem, storeFront);
                    availableItems.Add(new Item(newItem.transform, majorCurrents[i].Name, "Major Current"));
                    save.availableItems.Add(majorCurrents[i].Name);
                    Image newItemImage = newItem.GetComponent<Image>();
                    newItemImage.color = new Color(newItemImage.color.r, newItemImage.color.g, newItemImage.color.b, 1);
                    newItem.GetComponent<ShopClick>().current = availableItems[availableItems.Count - 1];
                    availableItems[availableItems.Count - 1].SubGenders = new List<string>();
                    availableItems[availableItems.Count - 1].Era = majorCurrents[i].Era;
                    availableItems[availableItems.Count - 1].icon = currentIcons[i];
                    newItem.transform.GetChild(0).GetComponent<Image>().sprite = availableItems[availableItems.Count - 1].icon;
                    if (majorCurrents[i].SubCurrents != null)
                    {
                        for (int y = 0; y < majorCurrents[i].SubCurrents.Count; y++)
                        {
                            availableItems[availableItems.Count - 1].SubGenders.Add(currents[majorCurrents[i].SubCurrents[y]].name);
                            availableItems[availableItems.Count - 1].Total += currents[majorCurrents[i].SubCurrents[y]].total;
                        }
                    }
                    availableItems[availableItems.Count - 1].Price = ItemPrice(availableItems[availableItems.Count - 1]);
                    majorCurrents[i].Unlocked = true;
                }
            }
        }
        // Prepare ScrollList
        for (int i = 0; i < scrollTexts.Length; i++)
        {
            scrollTexts[i] = scrollList.Find("Text (TMP) (" + i + ")").GetComponent<TextMeshProUGUI>();
        }
        scrollTexts[0].text = "Horizons";
        scrollTexts[1].text = "Painting";
        scrollTexts[1].color = domainColors[0];
        // Prepare TrendSlots
        headerDrops.Add(slotsList.Find("TrendSlot"));
        // Display First Stats
        // Set News
        if (newsEra < 2)
        {
            for (int i = 0; i < newsList.News.Count; i++)
            {
                if (newsList.News[i].Era == newsEra)
                {
                    newsListAvailable.News.Add(newsList.News[i]);
                }
            }
        }
        if (!loading)
        {
            stockDisplay.text = "0 <size=5>ames</size>";
            productionDisplay.text = "0 <size=4>ames/sec</size>";
            statisticTexts[0].text = "<b>Artists:</b> 0";
            statisticTexts[1].text = "<b>Average:</b> 0";
            statisticTexts[2].text = "<b>Eras:</b> 0%";
            statisticTexts[3].text = "<b>Gauge:</b> 0";
            statisticTexts[4].text = "<b>Multiplied by:</b> 1";
            statisticTexts[5].text = "<b>Nations:</b> 0";
            statisticTexts[6].text = "<b>Women:</b> 0";
            statisticTexts[7].text = "<b>Men:</b> 0";
            statisticTexts[8].text = "<b>Activists:</b> 0";
            statisticTexts[9].text = "<b>Society Age:</b> 1";
            StartCoroutine(Beginning());
            save.displayOn = true;
            save.audioOn = true;
        }
        else
        {
            LoadGame();
        }
    }
    private void Update()
    {
        if (click && !introButton)
        {
            if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
            {
                click = false;
                if (audioOn)
                {
                    sound2.clip = clips[13];
                    sound2.volume = volume * 1.5f;
                    sound2.Play();
                }
            }
        }
        if (gameOn)
        {
            if (stock > 1000000000000)
            {
                stockDisplay.text = Math.Round(stock * 0.000000000001f, 3).ToString();
                stockDisplayText.text = "<size=8>Trillion </size><size=5>ames</size>";
                stockDisplayText.transform.localPosition = new Vector2(11f, -22.5f);
            }
            else if (stock > 1000000000)
            {
                stockDisplay.text = Math.Round(stock * 0.000000001f, 3).ToString();
                stockDisplayText.text = "<size=8>Billion </size><size=5>ames</size>";
                stockDisplayText.transform.localPosition = new Vector2(9f, -22.5f);
            }
            else if (stock > 1000000)
            {
                stockDisplay.text = Math.Round(stock * 0.000001f, 3).ToString();
                stockDisplayText.text = "<size=8>Million </size><size=5>ames</size>";
                stockDisplayText.transform.localPosition = new Vector2(9f, -22.5f);
            }
            else if (stock > 1000)
            {
                stockDisplay.text = Math.Round(stock * 0.001f, 3).ToString();
                stockDisplayText.text = "<size=6.5>Thousand </size><size=5>ames</size>";
                stockDisplayText.transform.localPosition = new Vector2(13.11f, -22.5f);
            }
            else
            {
                stockDisplay.text = Math.Round(stock).ToString();
                stockDisplayText.text = "<align=left><size=5>ames</size></align>";
                stockDisplayText.transform.localPosition = new Vector2(13.11f, -22.5f);
            }
            newsTime += Time.deltaTime;
            stock += production * Time.deltaTime;
            save.stock = stock;
            productionHistory += production * Time.deltaTime;
            if (gaugeTime < 2)
            {
                gaugeTime += Time.deltaTime;
                gauge.localScale = new Vector3(gauge.localScale.x, Mathf.Lerp(gauge.localScale.y, gaugeSize, gaugeTime * 0.5f));
                if (gauge.localScale.y > 0.005f)
                {
                    if (globalMultiplicator < 0)
                    {
                        gaugeIsDown.enabled = true;
                        gaugeIsUp.enabled = false;
                    }
                    else
                    {
                        gaugeIsDown.enabled = false;
                        gaugeIsUp.enabled = true;
                    }
                }
            }
            if (newsTime > 20)
            {
                StartCoroutine(NewsChange());
                SaveGame();
                newsTime = 0;
            }
            if (drawOn)
            {
                if (!overDraw)
                {
                    if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
                    {
                        StartCoroutine(HideDraw(drawRef));
                    }
                }
            }
            if (panelDisplay)
            {
                panel.position = new Vector3(Mathf.Clamp(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, -3.76f, 6.21f), Mathf.Clamp(Camera.main.ScreenToWorldPoint(Input.mousePosition).y, -3.81f, -0.86f), 1000);
            }
        }
        else
        {
            if (secondDrawOn)
            {
                if (selected.Count == 2)
                {
                    if (!validate.gameObject.activeSelf)
                    {
                        validate.gameObject.SetActive(true);
                    }
                    if (validate.alpha < 1)
                    {
                        validate.alpha += Time.deltaTime;
                    }
                }
                else
                {
                    if (validate.gameObject.activeSelf)
                    {
                        validate.gameObject.SetActive(false);
                    }
                    if (validate.alpha >= 0)
                    {
                        validate.alpha -= Time.deltaTime;
                    }
                }
            }
        }
        if (Input.GetKey(KeyCode.Space))
        {
            StartCoroutine(EndGame());
        }
    }
    IEnumerator ItemsFade()
    {
        float currentTime = 0;
        while (currentTime <= 2)
        {
            currentTime += Time.deltaTime;
            for (int i = 0; i < newItems.Count; i++)
            {
                newItems[i].alpha = currentTime * 0.5f;
            }
            yield return null;
        }
        newItems.Clear();
        yield break;
    }
    IEnumerator GaugeChange(bool positive)
    {
        if (positive)
        {
            gaugeTitle.text = "Gauge is Up!";
            gaugeText.text = "Your just passed a gauge level!\nProduction is multiplied.";
        }
        else
        {
            gaugeTitle.text = "Gauge is Down.";
            gaugeText.text = "Your just lost a gauge level!\nProduction is divided.";
        }
        float currentTime = 0;
        while (currentTime <= 1f)
        {
            currentTime += Time.deltaTime;
            statsDisplay.alpha = 1 - currentTime;
            gaugeDisplay.alpha = currentTime;
            yield return null;
        }
        statsDisplay.alpha = 0;
        gaugeDisplay.alpha = 1;
        click = true;
        while (click) { yield return null; }
        currentTime = 0;
        while (currentTime <= 0.5f)
        {
            currentTime += Time.deltaTime;
            gaugeDisplay.alpha = 1 - currentTime * 2;
            statsDisplay.alpha = currentTime * 2;
            yield return null;
        }
        gaugeDisplay.alpha = 0;
        statsDisplay.alpha = 1;
        yield break;
    }
    IEnumerator NewsChange()
    {
        int newRef = UnityEngine.Random.Range(0, newsListAvailable.News.Count);
        if (beginning)
        {
            newRef = 0;
        }
        else
        {
            if (newsPlay >= newsListAvailable.News.Count)
            {
                newsPlay = 0;
                for (int i = 0; i < newsListAvailable.News.Count; i++)
                {
                    newsListAvailable.News[i].played = false;
                }
            }
            while (newsListAvailable.News[newRef].played == true)
            {
                newRef = UnityEngine.Random.Range(0, newsListAvailable.News.Count);
                yield return null;
            }
        }
        beginning = false;
        newsRef = newRef;
        newsListAvailable.News[newsRef].played = true;
        newsPlay++;
        string text = newsListAvailable.News[newsRef].Text;
        float timeFade = 0;
        float origin = transformNews.localPosition.y;
        while (timeFade < 1)
        {
            timeFade += Time.deltaTime;
            transformNews.localPosition = new Vector2(newsText.transform.localPosition.x, Mathf.Lerp(origin, origin - 2, timeFade));
            newsText.color = new Color(newsText.color.r, newsText.color.g, newsText.color.b, Mathf.Lerp(1, 0, timeFade));
            yield return null;
        }
        newsText.text = text;
        timeFade = 0;
        while (timeFade < 1)
        {
            timeFade += Time.deltaTime;
            transformNews.localPosition = new Vector2(newsText.transform.localPosition.x, Mathf.Lerp(origin + 2, origin, timeFade));
            newsText.color = new Color(newsText.color.r, newsText.color.g, newsText.color.b, Mathf.Lerp(0, 1, timeFade));
            yield return null;
        }
        yield break;
    }
    public void Menu()
    {
        StartCoroutine(DisplayMenu());
    }
    public void Quit()
    {
        StartCoroutine(ConfirmChoice());
        choice = 0;
    }
    public void Audio()
    {
        if (audioOn)
        {
            audioOn = false;
            soundText.text = "Turn on audio";
        }
        else
        {
            sound.clip = clips[10];
            sound.volume = volume * 0.25f;
            sound.Play();
            audioOn = true;
            soundText.text = "Turn off audio";
        }
        save.audioOn = audioOn;
    }
    public void ChoiceYes()
    {
        if (choice == 0)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");
            bf.Serialize(file, save);
            file.Close();
            Application.Quit();
        }
        if (choice == 1)
        {
            File.Delete(Application.persistentDataPath + "/gamesave.save");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        if (choice == 3)
        {
            StartCoroutine(EndGame());
        }
    }
    public void ToggleDisplay()
    {
        if (displayOn)
        {
            if (audioOn)
            {
                sound.clip = clips[12];
                sound.volume = volume * 0.25f;
                sound.Play();
            }
            displayText.text = "Turn on info";
            displayOn = false;
        }
        else
        {
            if (audioOn)
            {
                sound.clip = clips[10];
                sound.volume = volume * 0.25f;
                sound.Play();
            }
            displayText.text = "Turn off info";
            displayOn = true;
        }
        save.displayOn = displayOn;
    }
    public void About()
    {
        StartCoroutine(DisplayAbout());
    }
    public void ResetGame()
    {
        choice = 1;
        StartCoroutine(ConfirmChoice());
    }
    void SaveGame()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");
        bf.Serialize(file, save);
        file.Close();
    }
    void LoadGame()
    {
        // Set Panel
        transParagraph.alpha = 1;
        transPanel.transform.localPosition = new Vector2(37, -56);
        //transPanel.gameObject.GetComponent<Image>().color = colorMalus;
        transClick.transform.localPosition = new Vector2(transClick.transform.localPosition.x, -17);
        transText.transform.localPosition = new Vector2(transText.transform.localPosition.x, 15);
        transTitle.transform.localPosition = new Vector2(transTitle.transform.localPosition.x, 28);
        transTitle.alpha = 0;
        transSubTitle.alpha = 0;
        RectTransform rect = transPanel.GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, 90);
        transBackground.transform.SetParent(transPanel.transform);
        transPanel.alpha = 0;
        transPanel.gameObject.SetActive(false);
        // Load Save
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
        save = (Save)bf.Deserialize(file);
        file.Close();
        // Set Domains
        for (int i = 0; i < save.freeDomains.Count; i++)
        {
            ArtistList tempArtistDomainLocked = artistDomainLocked[save.freeDomains[i]];
            ArtistList tempArtistDomainAvailable = artistDomainAvailable[save.freeDomains[i]];
            ArtistList tempArtistDomainActive = artistDomainActive[save.freeDomains[i]];
            ArtistList tempShamefulActive = shamefulInfluenceAvailable[save.freeDomains[i]];
            string tempDomain = domain[save.freeDomains[i]];
            string tempDomainShortened = domainShortened[save.freeDomains[i]];
            artistDomainLocked[save.freeDomains[i]] = artistDomainLocked[save.outDomains[i]];
            artistDomainAvailable[save.freeDomains[i]] = artistDomainAvailable[save.outDomains[i]];
            artistDomainActive[save.freeDomains[i]] = artistDomainActive[save.outDomains[i]];
            shamefulInfluenceAvailable[save.freeDomains[i]] = shamefulInfluenceAvailable[save.outDomains[i]];
            domain[save.freeDomains[i]] = domain[save.outDomains[i]];
            domainShortened[save.freeDomains[i]] = domainShortened[save.outDomains[i]];
            artistDomainLocked[save.outDomains[i]] = tempArtistDomainLocked;
            artistDomainAvailable[save.outDomains[i]] = tempArtistDomainAvailable;
            artistDomainActive[save.outDomains[i]] = tempArtistDomainActive;
            shamefulInfluenceAvailable[save.outDomains[i]] = tempShamefulActive;
            domain[save.outDomains[i]] = tempDomain;
            domainShortened[save.outDomains[i]] = tempDomainShortened;
        }
        for (int y = 0; y < domainShops.Length; y++)
        {
            domainShops[y].transform.parent.Find("DomainName").GetComponent<TextMeshProUGUI>().text = artistDomainAvailable[y].name;
        }
        // Get Multiplicator
        currentSociety = save.currentSociety;
        eraMultiplicator = (currentSociety * eraFactor) + 1;
        for (int i = 3; i <= currentSociety; i += 3)
        {
            trendSlots[0].SetActive(true);
            headerDrops.Add(trendSlots[0].transform);
            trendSlots.Remove(trendSlots[0]);
        }
        if (currentSociety <= 3)
        {
            slotsList.parent.Find("Trendsetters").GetComponent<TextMeshProUGUI>().text = "Global Trendsetters";
        }
        name1.text = societies[currentSociety].Name;
        name2.text = societies[currentSociety].Name;
        // Get Artists
        List<Artist> artists = new List<Artist>();
        List<int> artistsCount = new List<int>();
        List<Artist> leaders = new List<Artist>();
        for (int i = 0; i < artistDomainLocked.Count; i++)
        {
            for (int y = 0; y < artistDomainLocked[i].Artist.Count; y++)
            {
                for (int x = 0; x < save.artistDomainActive[i].Count; x++)
                {
                    if (artistDomainLocked[i].Artist[y].Name == save.artistDomainActive[i][x])
                    {
                        Artist artist = artistDomainLocked[i].Artist[y];
                        artistDomainActive[i].Artist.Add(artist);
                        artists.Add(artist);
                        artistsCount.Add(i);
                        CreateCard(i, artist);
                        if (save.isArtistLead[i][x] == 1)
                        {
                            CreateTrendsetter(save.isArtistLead[i][x], artist, 0);
                        }
                        else if (save.isArtistLead[i][x] == 2)
                        {
                            leaders.Add(artist);
                        }
                    }
                }
            }
        }
        for (int i = 0; i < leaders.Count; i++)
        {
            if (i < headerDrops.Count)
            {
                CreateTrendsetter(2, leaders[i], i);
            }
        }
        for (int i = 0; i < artistsCount.Count; i++)
        {
            artistDomainLocked[artistsCount[i]].Artist.Remove(artists[i]);
        }
        for (int i = 0; i < artistDomainActive.Count; i++)
        {
            if (artistDomainActive[i].Artist.Count > 0)
            {
                artistDomainActive[i].Artist.Sort((y, x) => x.Birth.CompareTo(y.Birth));
                artistDomainActive[i].Artist.Reverse();
                for (int y = 1; y < artistDomainActive[i].Artist.Count; y++)
                {
                    artistDomainActive[i].Artist[y].Card.SetParent(artistDomainActive[i].Artist[y - 1].Card.Find("NextCard"));
                    artistDomainActive[i].Artist[y].Card.localPosition = Vector2.zero;
                }
                artistDomainActive[i].Artist[0].Card.SetParent(cardBoxes[i]);
                cardBoxes[i].parent.GetComponent<BoxMouse>().cardNumber = artistDomainActive[i].Artist.Count;
                artistDomainActive[i].Artist[0].Card.localPosition = new Vector2(0, 460);
            }
        }
        // Get Scumbags
        for (int i = 0; i < shamefulInfluenceLocked.Artist.Count; i++)
        {
            for (int y = 0; y < save.shamefulInfluenceActive.Count; y++)
            {
                if (shamefulInfluenceLocked.Artist[i].Name == save.shamefulInfluenceActive[y])
                {
                    Artist shame = shamefulInfluenceLocked.Artist[y];
                    shamefulInfluenceActive.Artist.Add(shame);
                    shamefulInfluenceLocked.Artist.Remove(shame);
                    CreateShame(shame);
                    break;
                }
                cardBoxShame.parent.GetComponent<BoxMouse>().cardNumber = save.shamefulInfluenceActive.Count;
            }
        }
        if (shamefulInfluenceActive.Artist.Count > 0)
        {
            shamefulInfluenceActive.Artist[0].Card.SetParent(cardBoxShame);
            shamefulInfluenceActive.Artist[0].Card.localPosition = new Vector2(0, 460);
            for (int i = 1; i < shamefulInfluenceActive.Artist.Count; i++)
            {
                shamefulInfluenceActive.Artist[i].Card.SetParent(shamefulInfluenceActive.Artist[i - 1].Card.Find("NextCard"));
                shamefulInfluenceActive.Artist[i].Card.localPosition = Vector2.zero;
            }
        }
        // Get Eras
        int maxDomain = 1;
        for (int i = 0; i < domainShops.Length; i++)
        {
            erasPassed += save.eraList[i];
            domainShops[i].era = save.eraList[i];
            if (save.eraList[i] > 0)
            {
                scrollTexts[i + 1].text = artistDomainAvailable[i].name;
                scrollTexts[i + 1].color = domainColors[i];
                if (i > maxDomain)
                {
                    maxDomain = i;
                }
            }
            for (int y = 0; y < artistDomainLocked[i].Artist.Count; y++)
            {
                if (artistDomainLocked[i].Artist[y].Era <= domainShops[i].era)
                {
                    Artist artist = artistDomainLocked[i].Artist[y];
                    artistDomainAvailable[i].Artist.Add(artist);
                    artistDomainLocked[i].Artist.Remove(artist);
                    y--;
                }
            }
            domainShops[i].cardText = domainShops[i].transform.Find("Card (1)").Find("Available").GetComponent<TextMeshProUGUI>();
            domainShops[i].cardText2 = domainShops[i].transform.Find("Card (1)").Find("Locked").GetComponent<TextMeshProUGUI>();
            domainShops[i].cardText.text = domainShops[i].availableCards.ToString();
            domainShops[i].cardText2.text = artistDomainLocked[i].Artist.Count.ToString();
            domainShops[i].availableCards += 50 - artistDomainLocked[i].Artist.Count;
            domainShops[i].price = EraPrice(i, domainShops[i]);
            domainShops[i].eraText = domainShops[i].transform.Find("Card (3)").Find("Present").GetComponent<TextMeshProUGUI>();
            domainShops[i].eraText2 = domainShops[i].transform.Find("Card (3)").Find("Price").GetComponent<TextMeshProUGUI>();
            EraPriceDisplay(domainShops[i]);
            for (int y = 0; y < shamefulInfluenceLocked.Artist.Count; y++)
            {
                if (shamefulInfluenceLocked.Artist[y].Domain == artistDomainLocked[i].name)
                {
                    if (shamefulInfluenceLocked.Artist[y].Era <= domainShops[i].era)
                    {
                        shamefulInfluenceAvailable[i].Artist.Add(shamefulInfluenceLocked.Artist[y]);
                        shamefulInfluenceLocked.Artist.Remove(shamefulInfluenceLocked.Artist[y]);
                    }
                }
            }
        }
        // Get Items
        for (int i = 0; i < save.activeItems.Count; i++)
        {
            CreateItem(save.activeItems[i]);
        }
        cardBoxItems.parent.GetComponent<BoxMouse>().cardNumber = save.activeItems.Count;
        // Get Available Items
        for (int y = 0; y < save.availableItems.Count; y++)
        {
            for (int i = 0; i < majorCurrents.Length; i++)
            {
                if (majorCurrents[i].Name == save.availableItems[y])
                {
                    GameObject newItem = (GameObject)Instantiate(storeItem, storeFront);
                    availableItems.Add(new Item(newItem.transform, majorCurrents[i].Name, "Major Current"));
                    Item item = availableItems[availableItems.Count - 1];
                    item.icon = currentIcons[i];
                    newItem.transform.GetChild(0).GetComponent<Image>().sprite = item.icon;
                    newItems.Add(newItem.GetComponent<CanvasGroup>());
                    newItem.GetComponent<CanvasGroup>().alpha = 0;
                    Image newItemImage = newItem.GetComponent<Image>();
                    newItemImage.color = new Color(newItemImage.color.r, newItemImage.color.g, newItemImage.color.b, 1);
                    newItem.GetComponent<ShopClick>().current = item;
                    item.SubGenders = new List<string>();
                    item.Era = majorCurrents[i].Era;
                    majorCurrents[i].Unlocked = true;
                    if (majorCurrents[i].SubCurrents != null)
                    {
                        for (int x = 0; x < majorCurrents[i].SubCurrents.Count; x++)
                        {
                            item.SubGenders.Add(currents[majorCurrents[i].SubCurrents[x]].name);
                        }
                    }
                    item.Total = majorCurrents[i].Total;
                    item.Price = ItemPrice(item);
                }
            }
        }
        // Set Game
        stock = save.stock;
        audioOn = save.audioOn;
        displayOn = save.displayOn;
        if (maxDomain >= 19)
        {
            scrollTexts[21].text = "Shameful Influences";
        }
        if (maxDomain > 2)
        {
            domainScroll = maxDomain;
        }
        else
        {
            domainScroll = 2;
        }
        if (save.newsEra > 1)
        {
            newsEra = save.newsEra;
        }
        newsChange = true;
        Calculation();
        gameOn = true;
        GameObject.Find("IntroSafe").SetActive(false);
        StartCoroutine(ShowScene());
        StartCoroutine(ItemsFade());
    }
    IEnumerator ShowScene()
    {
        float currentTime = 0;
        while (currentTime <= 2)
        {
            currentTime += Time.deltaTime;
            transBackground.alpha = 1 - currentTime * 0.5f;
            yield return null;
        }
        transBackground.alpha = 0;
        transBackground.gameObject.SetActive(false);
        yield break;
    }
    void CreateTrendsetter(int lead, Artist artist, int id)
    {
        artist.Lead = lead;
        GameObject newCard = (GameObject)Instantiate(Resources.Load("FlyingCard"));
        Image imageCard = newCard.transform.Find("Image").GetComponent<Image>();
        int domainRef = 0;
        for (int i = 0; i < artistDomainAvailable.Count; i++)
        {
            if (artistDomainAvailable[i].name == artist.Domain)
            {
                domainRef = i;
            }
        }
        Color cardColor = domainColors[domainRef];
        FlyingCard fly = newCard.GetComponent<FlyingCard>();
        fly.mouse = artist.Card.Find("MouseDetector").GetComponent<MouseDetector>();
        fly.mouse.card = newCard.transform;
        fly.mouse.imageCard = imageCard;
        fly.domain = domainRef;
        string artistName = fly.mouse.artistName;
        string[] effectName = fly.mouse.effectName;
        Color[] effectColor = fly.mouse.effectColor;
        newCard.transform.Find("Name").GetComponent<TextMeshProUGUI>().text = artistName;
        fly.mouse.dragSpawn = true;
        fly.mouse.fly = fly;
        for (int i = 0; i < effectName.Length; i++)
        {
            GameObject newEffect = (GameObject)Instantiate(Resources.Load("SmallEffect"), newCard.transform.Find("Effects"));
            newEffect.GetComponent<Image>().color = effectColor[i];
            newEffect.transform.Find("Effect Name").GetComponent<TextMeshProUGUI>().text = effectName[i];
        }
        if (lead == 1)
        {
            fly.global = false;
            newCard.transform.SetParent(domainDrops[domainRef]);
            imageCard.color = new Color(cardColor.r, cardColor.g, cardColor.b, 1);
            newCard.GetComponent<Image>().raycastTarget = true;
            imageCard.raycastTarget = false;
            newCard.transform.localScale = new Vector2(1, 1);
            newCard.transform.localPosition = new Vector3(0, 0, 0);
            leadArtists[domainRef] = artist;
            fly.mouse.cardDomainDropped = true;
        }
        else if (lead == 2)
        {
            int leadNumber = id;
            Transform parent = headerDrops[leadNumber];
            newCard.transform.SetParent(parent);
            fly.global = true;
            imageCard.color = new Color(cardColor.r, cardColor.g, cardColor.b, 1);
            imageCard.raycastTarget = true;
            newCard.transform.localScale = new Vector2(1, 1);
            newCard.transform.localPosition = new Vector3(0, 0, 0);
            supremeLeadArtists[leadNumber] = artist;
            fly.mouse.cardHeaderDropped = true;
            fly.mouse.headDropRef = leadNumber;
        }
    }
    void CreateShame(Artist currentArtist)
    {
        int domainRef = 0;
        for (int i = 0; i < artistDomainAvailable.Count; i++)
        {
            if (artistDomainAvailable[i].name == currentArtist.Domain)
            {
                domainRef = i;
            }
        }
        Transform newCard = GameObject.Instantiate(card, cardPlace).transform;
        newCard.GetComponent<CardSelection>().enabled = false;
        newCard.GetComponent<EventTrigger>().enabled = false;
        newCard.localScale = new Vector2(1f, 1f);
        TextMeshProUGUI cardText = newCard.Find("Name").GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI cardTags = newCard.Find("Currents").GetComponent<TextMeshProUGUI>();
        Transform effectSort = newCard.Find("EffectSort");
        newCard.Find("Title").gameObject.SetActive(false);
        TextMeshProUGUI cardEffect = newCard.Find("Currents").GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI cardShame = newCard.Find("Tags").GetComponent<TextMeshProUGUI>();
        cardEffect.color = colorMalusEffect2;
        cardEffect.text = currentArtist.Influence;
        cardShame.color = colorMalusEffect;
        cardShame.text = currentArtist.Shame;
        cardText.text = currentArtist.Name;
        currentArtist.Card = newCard;
        cardText.alignment = TextAlignmentOptions.Center;
        Transform cashEffect = GameObject.Instantiate(effect, effectSort).transform;
        TextMeshProUGUI cashDescription = cashEffect.Find("Effect Name").GetComponent<TextMeshProUGUI>();
        cashEffect.Find("Effect").gameObject.SetActive(false);
        cashEffect.gameObject.GetComponent<Image>().color = colorMalusEffect2;
        currentArtist.cashPrize = CashPrize(domainRef, domainShops[domainRef].era);
        cashDescription.text = "Grants you an immediate " + currentArtist.cashPrize + " ames";
        cashDescription.alignment = TextAlignmentOptions.Center;
        cashDescription.color = Color.white;
        Transform badEffect = GameObject.Instantiate(effect, effectSort).transform;
        TextMeshProUGUI badDescription = badEffect.Find("Effect Name").GetComponent<TextMeshProUGUI>();
        badEffect.gameObject.GetComponent<Image>().color = colorMalusEffect;
        badEffect.Find("Effect").gameObject.SetActive(false);
        badDescription.text = "Brutally hurts the Gauge";
        badDescription.color = Color.white;
        currentArtist.Locked = false;
        newCard.Find("Image").GetComponent<Image>().color = colorMalusCard;
        for (int i = 0; i < shames.Count; i++)
        {
            if (shames[i].name == currentArtist.Shame)
            {
                shames[i].amount++;
            }
        }
        GameObject detectorObject = newCard.Find("MouseDetector").gameObject;
        detectorObject.SetActive(true);
        MouseDetector detector = detectorObject.GetComponent<MouseDetector>();
        detector.nextCard = newCard.Find("NextCard").transform;
        detector.currentCard = newCard;
        detector.outOfDomain = true;
    }
    void CreateCard(int domain, Artist currentArtist)
    {
        Transform newCard = GameObject.Instantiate(card, cardPlace).transform;
        newCard.GetComponent<CardSelection>().enabled = false;
        newCard.GetComponent<EventTrigger>().enabled = false;
        newCard.localScale = new Vector2(1f, 1f);
        TextMeshProUGUI cardText = newCard.Find("Name").GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI cardTags = newCard.Find("Tags").GetComponent<TextMeshProUGUI>();
        Transform effectSort = newCard.Find("EffectSort");
        TextMeshProUGUI cardCurrents = newCard.Find("Currents").GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI cardDesc = newCard.Find("Text").GetComponent<TextMeshProUGUI>();
        cardDesc.text = currentArtist.Text;
        // Read Card Name
        if (currentArtist.Name2 == null)
        {
            if (currentArtist.Death != 9999 && currentArtist.Birth != 9999)
            {
                cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + ")";
            }
            else if (currentArtist.Birth == 9999)
            {
                cardText.text = currentArtist.Name + " (???)";
            }
            else
            {
                cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "- )";
            }
        }
        else if (currentArtist.Name3 == null && currentArtist.CommonName == null)
        {
            if (currentArtist.Death != 9999 && currentArtist.Death2 != 9999)
            {
                cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + ") &\n"
                + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "-" + currentArtist.Death2.ToString() + ")";
            }
            else if (currentArtist.Death != 9999)
            {
                cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + ") &\n"
                + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "- )";
            }
            else if (currentArtist.Death2 != 9999)
            {
                cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "- ) &\n"
                + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "-" + currentArtist.Death2.ToString() + ")";
            }
            else
            {
                cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "- ) &\n"
                + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "- )";
            }
        }
        else if (currentArtist.Name3 == null)
        {
            if (currentArtist.Death != 9999 && currentArtist.Death2 != 9999)
            {
                cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + ") &\n"
                + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "-" + currentArtist.Death2.ToString() + ") " + currentArtist.CommonName;
            }
            else if (currentArtist.Death != 9999)
            {
                cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + ") &\n"
                + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "- ) " + currentArtist.CommonName;
            }
            else if (currentArtist.Death2 != 9999)
            {
                cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "- ) &\n"
                + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "-" + currentArtist.Death2.ToString() + ") " + currentArtist.CommonName;
            }
            else
            {
                cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "- ) &\n"
                + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "- ) " + currentArtist.CommonName;
            }
        }
        else
        {
            cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + "), "
                + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "-\n" + currentArtist.Death2.ToString() + ") & "
                + currentArtist.Name3 + " (" + currentArtist.Birth2.ToString() + "-" + currentArtist.Death3.ToString() + ") " + currentArtist.CommonName;
        }
        // Read Card Informations
        Transform nationSort = newCard.Find("NationSort");
        Transform genderSort = newCard.Find("GenderSort");
        for (int y = 0; y < currentArtist.nationRefs.Count; y++)
        {
            GameObject newNation = GameObject.Instantiate(nation, nationSort);
            if (currentArtist.nationRefs[y] == 8 && currentArtist.Birth > 1900 && currentArtist.Birth < 1980)
            {
                newNation.GetComponent<Image>().sprite = flags[118];
            }
            else
            {
                newNation.GetComponent<Image>().sprite = flags[currentArtist.nationRefs[y]];
            }
            NationMouse nm = newNation.GetComponent<NationMouse>();
            nm.active = true;
            nm.nation = currentArtist.Nation[y];
            nm.pos = newCard;
            nm.cardActive = true;
        }
        for (int y = 0; y < currentArtist.Gender.Count; y++)
        {
            GameObject newGender = GameObject.Instantiate(nation, genderSort);
            newGender.transform.localScale = new Vector2(0.8f, 0.8f);
            if (currentArtist.Gender[y] == "Female")
            {
                newGender.GetComponent<Image>().sprite = genderF;
            }
            else if (currentArtist.Gender[y] == "Male")
            {
                newGender.GetComponent<Image>().sprite = genderM;
            }
            else
            {
                newGender.GetComponent<Image>().sprite = genderNB;
            }
        }
        for (int i = 0; i < currentArtist.effectRefs.Count; i++)
        {
            Transform newEffect = GameObject.Instantiate(effect, effectSort).transform;
            TextMeshProUGUI effectTitle = newEffect.Find("Effect Name").GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI effectDescription = newEffect.Find("Effect").GetComponent<TextMeshProUGUI>();
            EffectClick click = newEffect.GetComponent<EffectClick>();
            click.artist = currentArtist;
            click.effect = i;
            effectTitle.text = currentArtist.Effects[i];
            Effect effectRef = effectList.Effect[currentArtist.effectRefs[i]];
            if (effectRef.Type == "Activism")
            {
                newEffect.gameObject.GetComponent<Image>().color = effectColors[0];
                if (effectRef.SubType == "Feminism")
                {
                    effectDescription.text = "Heals the Gauge and boosts women Artists";
                }
                else if (effectRef.SubType == "Antiracism")
                {
                    effectDescription.text = "Heals the Gauge and boosts marginalized Artists";
                }
                else if (effectRef.SubType == "Queer")
                {
                    effectDescription.text = "Heals the Gauge and boosts queer Artists";
                }
                else
                {
                    effectDescription.text = "Heals the Gauge";
                }
            }
            else if (effectRef.Type == "Contribution")
            {
                newEffect.gameObject.GetComponent<Image>().color = effectColors[1];
                if (effectRef.SubType == "Global")
                {
                    effectDescription.text = "Slightly reduces the price of all Cards";
                }
                else
                {
                    effectDescription.text = "Reduces the price of " + domainShortened[domain] + " Cards";
                }
            }
            else if (effectRef.Type == "Feature")
            {
                newEffect.gameObject.GetComponent<Image>().color = effectColors[2];
                effectDescription.text = "Strengthens this Artist's other Effects";
            }
            else if (effectRef.Type == "Interdisciplinary current")
            {
                newEffect.gameObject.GetComponent<Image>().color = effectColors[3];
                effectDescription.text = "Boosts each Artist linked to " + effectRef.Name;
            }
            else if (effectRef.Type == "Area-defined current")
            {
                newEffect.gameObject.GetComponent<Image>().color = effectColors[4];
                effectDescription.text = "Boosts " + effectRef.Area + " for each Artist in " + domainShortened[domain];
            }
            else if (effectRef.Type == "Domain-defined current")
            {
                newEffect.gameObject.GetComponent<Image>().color = effectColors[5];
                effectDescription.text = "Boosts " + domainShortened[domain] + " for each Artist within a minor current";
            }
            else if (effectRef.Type == "Group")
            {
                newEffect.gameObject.GetComponent<Image>().color = effectColors[6];
                effectDescription.text = "Strengthens the Gauge's influence";
            }
            else if (effectRef.Type == "Title")
            {
                newEffect.gameObject.GetComponent<Image>().color = effectColors[7];
                effectDescription.text = "Strengthens all Effects within " + domainShortened[domain];
            }
            else if (effectRef.Type == "Prize")
            {
                newEffect.gameObject.GetComponent<Image>().color = effectColors[8];
                effectDescription.text = "Slightly boosts everyone for each represented country";
            }
            else if (effectRef.Type == "Discovery")
            {
                newEffect.gameObject.GetComponent<Image>().color = effectColors[9];
                if (effectRef.Area != "")
                {
                    effectDescription.text = "Boosts " + domainShortened[domain] + " for each Artist in " + effectRef.Area;
                }
                else
                {
                    effectDescription.text = "Boosts " + domainShortened[domain] + " for each Artist in " + effectRef.SubType;
                }
            }
            else if (effectRef.Type == "Genre")
            {
                newEffect.gameObject.GetComponent<Image>().color = effectColors[10];
                effectDescription.text = "Boosts " + domainShortened[domain] + " for each of its Artists";
            }
            else if (effectRef.Type == "Activity")
            {
                newEffect.gameObject.GetComponent<Image>().color = effectColors[11];
                if (effectRef.Name == "Multitalented Artist")
                {
                    effectDescription.text = "Slightly boosts " + domainShortened[domain] + " for each Artist";
                }
                else if (effectList.Effect[currentArtist.effectRefs[i]].Domain.Count > 2)
                {
                    int domainReference = 0;
                    for (int z = 0; z < artistDomainActive.Count; z++)
                    {
                        if (effectRef.Domain[0] == artistDomainActive[z].name)
                        {
                            domainReference = z;
                        }
                    }
                    effectDescription.text = "Boosts " + domainShortened[domainReference] + " for each Artist in these two Domains";
                }
                else
                {
                    int domainReference = 0;
                    for (int z = 0; z < artistDomainActive.Count; z++)
                    {
                        if (effectRef.Domain[0] == artistDomainActive[z].name)
                        {
                            domainReference = z;
                        }
                    }
                    int domainReference2 = 0;
                    for (int z = 0; z < artistDomainActive.Count; z++)
                    {
                        if (effectRef.Domain[1] == artistDomainActive[z].name)
                        {
                            domainReference2 = z;
                        }
                    }
                    if (domainReference == domainReference2) { effectDescription.text = "Boosts " + domainShortened[domainReference] + " for each of its Artists"; }
                    else { effectDescription.text = "Boosts " + domainShortened[domainReference] + " for each Artist in " + domainShortened[domainReference2]; }
                }
            }
            else if (effectRef.Type == "Achievement")
            {
                newEffect.gameObject.GetComponent<Image>().color = effectColors[12];
                effectDescription.text = "Slightly boosts everyone for each Artist in " + domainShortened[domain];
            }
        }
        if (currentArtist.Tags[0] == "None")
        {
            cardTags.text = " ";
        }
        else if (currentArtist.Tags.Count == 1)
        {
            cardTags.text = currentArtist.Tags[0];
        }
        else if (currentArtist.Tags.Count == 2)
        {
            cardTags.text = currentArtist.Tags[0] + ", " + currentArtist.Tags[1];
        }
        else
        {
            cardTags.text = currentArtist.Tags[0] + ", " + currentArtist.Tags[1] + ", " + currentArtist.Tags[2];
        }
        if (currentArtist.Currents[0] == "None")
        {
            cardCurrents.text = " ";
        }
        else if (currentArtist.Currents.Count == 1)
        {
            cardCurrents.text = currentArtist.Currents[0];
        }
        else if (currentArtist.Currents.Count == 2)
        {
            cardCurrents.text = currentArtist.Currents[0] +
                ", " + currentArtist.Currents[1];
        }
        else if (currentArtist.Currents.Count == 3)
        {
            cardCurrents.text = currentArtist.Currents[0] +
                ", " + currentArtist.Currents[1] +
                ", " + currentArtist.Currents[2];
        }
        else if (currentArtist.Currents.Count == 4)
        {
            cardCurrents.text = currentArtist.Currents[0] +
                ", " + currentArtist.Currents[1] +
                ", " + currentArtist.Currents[2] +
                ", " + currentArtist.Currents[3];
        }
        else if (currentArtist.Currents.Count == 5)
        {
            cardCurrents.text = currentArtist.Currents[0] +
                ", " + currentArtist.Currents[1] +
                ", " + currentArtist.Currents[2] +
                ", " + currentArtist.Currents[3] +
                ", " + currentArtist.Currents[4];
        }
        // Organize Cards
        currentArtist.Card = newCard;
        domainShops[domain].availableCards--;
        // Update Stats
        currentArtist.Prod = newCard.Find("Production").GetComponent<TextMeshProUGUI>();
        currentArtist.Prod.fontSize = 4;
        available++;
        for (int i = 0; i < currentArtist.nationRefs.Count; i++)
        {
            nations[currentArtist.nationRefs[i]].amount++;
        }
        for (int i = 0; i < currentArtist.tagRefs.Count; i++)
        {
            tags[currentArtist.tagRefs[i]].amount++;
        }
        for (int i = 0; i < currentArtist.currentRefs.Count; i++)
        {
            currents[currentArtist.currentRefs[i]].amount++;
        }
        if (currentArtist.Gender.Count > 1)
        {
            men++;
            women++;
        }
        else if (currentArtist.Gender[0] == "Female")
        {
            women++;
        }
        else
        {
            men++;
        }
        // Set Cards
        GameObject detectorObject = newCard.Find("MouseDetector").gameObject;
        detectorObject.SetActive(true);
        MouseDetector detector = detectorObject.GetComponent<MouseDetector>();
        detector.nextCard = newCard.Find("NextCard").transform;
        detector.currentCard = newCard;
        detector.box = cardBoxes[domain].parent.GetComponent<BoxMouse>();
        detector.cardColor = domainColors[domain];
        detector.domain = domain;
        detector.artist = currentArtist;
        if (currentArtist.Name2 == null)
        {
            detector.artistName = currentArtist.Name;
        }
        else if (currentArtist.Name3 == null && currentArtist.CommonName == null)
        {
            detector.artistName = currentArtist.Name + " & " + currentArtist.Name2;
        }
        else if (currentArtist.Name3 == null)
        {
            detector.artistName = currentArtist.Name + ", " + currentArtist.Name2 + " & " + currentArtist.Name3;
        }
        else
        {
            detector.artistName = currentArtist.Name + ", " + currentArtist.Name2 + " & " + currentArtist.Name3 + currentArtist.CommonName;
        }
        detector.effectName = new string[currentArtist.Effects.Count];
        detector.effectColor = new Color[currentArtist.Effects.Count];
        for (int i = 0; i < currentArtist.Effects.Count; i++)
        {
            detector.effectName[i] = currentArtist.Effects[i];
            Effect effectRef = effectList.Effect[currentArtist.effectRefs[i]];
            if (effectRef.Type == "Activism")
            {
                detector.effectColor[i] = effectColors[0];
            }
            else if (effectRef.Type == "Contribution")
            {
                detector.effectColor[i] = effectColors[1];
            }
            else if (effectRef.Type == "Feature")
            {
                detector.effectColor[i] = effectColors[2];
            }
            else if (effectRef.Type == "Interdisciplinary current")
            {
                detector.effectColor[i] = effectColors[3];
            }
            else if (effectRef.Type == "Area-defined current")
            {
                detector.effectColor[i] = effectColors[4];
            }
            else if (effectRef.Type == "Domain-defined current")
            {
                detector.effectColor[i] = effectColors[5];
            }
            else if (effectRef.Type == "Group")
            {
                detector.effectColor[i] = effectColors[6];
            }
            else if (effectRef.Type == "Title")
            {
                detector.effectColor[i] = effectColors[7];
            }
            else if (effectRef.Type == "Prize")
            {
                detector.effectColor[i] = effectColors[8];
            }
            else if (effectRef.Type == "Discovery")
            {
                detector.effectColor[i] = effectColors[9];
            }
            else if (effectRef.Type == "Genre")
            {
                detector.effectColor[i] = effectColors[10];
            }
            else if (effectRef.Type == "Activity")
            {
                detector.effectColor[i] = effectColors[11];
            }
            else if (effectRef.Type == "Achievement")
            {
                detector.effectColor[i] = effectColors[12];
            }
        }
    }
    void CreateItem(string name)
    {
        MajorCurrent current = majorCurrents[0];
        int currentRef = 0;
        for (int i = 0; i < majorCurrents.Length; i++)
        {
            if (majorCurrents[i].Name == name)
            {
                current = majorCurrents[i];
                majorCurrents[i].Owned = true;
                majorCurrents[i].Unlocked = true;
                currentRef = i;
                break;
            }
        }
        activeItems.Add(new Item(current.Name, "Major Current"));
        Item item = activeItems[activeItems.Count - 1];
        item.SubGenders = new List<string>();
        item.Era = current.Era;
        Transform parent = cardBoxItems;
        if (activeItems.Count > 1)
        {
            Transform cardParent = activeItems[activeItems.Count - 2].card;
            ItMouseDetector mouse = cardParent.Find("MouseDetector").GetComponent<ItMouseDetector>();
            if (!mouse.small)
            {
                mouse.background.gameObject.SetActive(true);
            }
            parent = cardParent.Find("NextCard").transform;
        }
        item.SubRefs = new List<int>();
        item.SubTexts = new List<TextMeshProUGUI>();
        if (current.SubCurrents != null)
        {
            for (int y = 0; y < current.SubCurrents.Count; y++)
            {
                item.SubGenders.Add(currents[current.SubCurrents[y]].name);
            }
            item.Total = current.Total;
        }
        if (item.SubGenders.Count > 1)
        {
            item.card = Instantiate(bigCurrent, parent).transform;
            RectTransform subList = item.card.transform.Find("SubCurrents").GetComponent<RectTransform>();
            for (int i = 0; i < item.SubGenders.Count; i++)
            {
                GameObject sub = Instantiate(SubCurrent, subList);
                int count = 0;
                for (int y = 0; y < currents.Count; y++)
                {
                    if (currents[y].name == item.SubGenders[i])
                    {
                        item.SubRefs.Add(y);
                        count = currents[y].amount;
                    }
                }
                item.SubTexts.Add(sub.GetComponent<TextMeshProUGUI>());
                item.SubTexts[i].text = item.SubGenders[i] + ": " + count + " artists";
            }
        }
        else
        {
            item.card = Instantiate(smallCurrent, parent).transform;
            int totalArtists = 0;
            for (int y = 0; y < currents.Count; y++)
            {
                if (currents[y].name == item.Name)
                {
                    item.SubRefs.Add(y);
                    totalArtists = currents[y].total;
                }
            }
            item.SubTexts.Add(item.card.Find("Amount").GetComponent<TextMeshProUGUI>());
            item.SubTexts[0].text = "Artists: " + item.Total;
            item.card.Find("Total").GetComponent<TextMeshProUGUI>().text = "Maximum: " + totalArtists;
        }
        for (int i = 0; i < currents.Count; i++)
        {
            if (item.Name == currents[i].name)
            {
                currents[i].available = true;
            }
            if (item.SubGenders != null)
            {
                for (int y = 0; y < item.SubGenders.Count; y++)
                {
                    if (item.SubGenders[y] == currents[i].name)
                    {
                        currents[i].available = true;
                    }
                }
            }
        }
        item.card.Find("Icon").GetComponent<Image>().sprite = currentIcons[currentRef];
        item.card.Find("Name").GetComponent<TextMeshProUGUI>().text = item.Name;
        item.card.Find("Era").GetComponent<TextMeshProUGUI>().text = item.Era.ToString();
        item.card.localScale = Vector2.one;
        Vector2 destination = Vector2.zero;
        if (item == activeItems[0])
        {
            if (item.SubGenders.Count > 1)
            {
                destination = new Vector2(0, 483.8f);
            }
            else
            {
                destination = new Vector2(0, 510);
            }
        }
        else if (item.SubGenders.Count > 1)
        {
            destination = new Vector2(0, -26.2f);
        }
        item.card.GetComponent<RectTransform>().localPosition = destination;
        ItMouseDetector detector = item.card.Find("MouseDetector").GetComponent<ItMouseDetector>();
        detector.nextCard = item.card.Find("NextCard").transform;
        detector.box = cardBoxItems.parent.GetComponent<BoxMouse>();
        if (item.SubGenders.Count < 2)
        {
            detector.Small();
        }
        else
        {
            detector.background = item.card.Find("NextCard").Find("Background").GetComponent<RectTransform>();
            detector.background.gameObject.SetActive(false);
            StartCoroutine(detector.PointerOutCo());
        }
    }
    IEnumerator DisplayAbout()
    {
        if (audioOn)
        {
            sound2.clip = clips[11];
            sound2.volume = volume * 0.25f;
            sound2.Play();
        }
        transPanel.gameObject.SetActive(true);
        transClick.alpha = 0;
        transTitle.alpha = 1;
        transSubTitle.alpha = 1;
        transText.alpha = 1;
        transParagraph.alpha = 1;
        transClick.transform.localPosition = new Vector2(transClick.transform.localPosition.x, -40);
        transText.transform.localPosition = new Vector2(transText.transform.localPosition.x, -12);
        transTitle.transform.localPosition = new Vector2(transTitle.transform.localPosition.x, 40);
        transSubTitle.transform.localPosition = new Vector2(transSubTitle.transform.localPosition.x, transSubTitle.transform.localPosition.y + 1.5f);
        RectTransform panelSize = transPanel.GetComponent<RectTransform>();
        panelSize.sizeDelta = new Vector2(panelSize.sizeDelta.x, panelSize.sizeDelta.y + 14);
        transPanel.transform.localPosition = new Vector2(transPanel.transform.localPosition.x, transPanel.transform.localPosition.y - 3);
        transTitle.text = "About Worldwide Arts Society";
        transSubTitle.text = "An incremental collecting game by Ramo";
        transText.fontSize = 5;
        transText.text = "I will try to set up a good wiki soon, so that a substantial amount of information about the game is available for anyone playing it.\n\nBIG THANKS\nTo Zeph for his original idea of a Jack Lang simulator.\nTo Anne for her help and support.\nTo Ambrine and Chloé for testing this game.\nTo L'Archipel des Malotrus for helping me survive confinement.\nTo anyone reading this, for your curiosity and attention.";
        transClick.gameObject.GetComponent<TextMeshProUGUI>().text = "Click to close";
        click = true;
        float currentTime = 0;
        while (currentTime <= 2 && click)
        {
            currentTime += Time.deltaTime;
            transPanel.alpha = currentTime * 0.5f;
            yield return null;
        }
        transPanel.alpha = 1;
        currentTime = 0;
        while (currentTime <= 1 && click)
        {
            currentTime += Time.deltaTime;
            transClick.alpha = currentTime;
            yield return null;
        }
        transClick.alpha = 1;
        while (click)
        {
            yield return null;
        }
        currentTime = 0;
        while (currentTime <= 1)
        {
            currentTime += Time.deltaTime;
            transClick.alpha = 1 - currentTime;
            transPanel.alpha = 1 - currentTime;
            yield return null;
        }
        transPanel.alpha = 0;
        transClick.alpha = 0;
        transClick.gameObject.GetComponent<TextMeshProUGUI>().text = "Click to continue";
        transText.fontSize = 6;
        transClick.transform.localPosition = new Vector2(transClick.transform.localPosition.x, -17);
        transText.transform.localPosition = new Vector2(transText.transform.localPosition.x, 15);
        transTitle.transform.localPosition = new Vector2(transTitle.transform.localPosition.x, 28);
        transSubTitle.transform.localPosition = new Vector2(transSubTitle.transform.localPosition.x, transSubTitle.transform.localPosition.y - 1.5f);
        panelSize.sizeDelta = new Vector2(panelSize.sizeDelta.x, panelSize.sizeDelta.y - 14);
        transPanel.transform.localPosition = new Vector2(transPanel.transform.localPosition.x, transPanel.transform.localPosition.y + 3);
        transPanel.gameObject.SetActive(false);
        click = false;
        yield break;
    }
    public IEnumerator ConfirmChoice()
    {
        if (audioOn)
        {
            sound.clip = clips[10];
            sound.volume = volume * 0.25f;
            sound.Play();
        }
        if (choice == 3)
        {
            confirmText.text = "This will end the\nGame.Are you sure?";
        }
        else
        {
            confirmText.text = "Are you sure?";
        }
        confirmOn = true;
        confirm.gameObject.SetActive(true);
        float menuAlpha = menu.alpha;
        float prodAlpha = prodStats.alpha;
        float currentTime = 0;
        while (currentTime <= 0.5f)
        {
            currentTime += Time.deltaTime;
            prodStats.alpha = Mathf.Lerp(prodAlpha, 0, currentTime * 2);
            menu.alpha = Mathf.Lerp(menuAlpha, 0, currentTime * 2);
            confirm.alpha = 1 - currentTime * 2;
            confirm.alpha = currentTime * 2;
            yield return null;
        }
        prodStats.alpha = 0;
        menu.alpha = 0;
        confirm.alpha = 1;
        yield break;
    }
    IEnumerator DisplayMenu()
    {
        if (confirmOn)
        {
            if (audioOn)
            {
                sound2.clip = clips[12];
                sound2.volume = volume * 0.25f;
                sound2.Play();
            }
            menuOn = false;
            confirmOn = false;
            float prodAlpha = prodStats.alpha;
            menu.gameObject.SetActive(false);
            float currentTime = 0;
            while (currentTime <= 0.5f)
            {
                menuButton.eulerAngles = new Vector3(0, 0, Mathf.Lerp(180, 0, currentTime * 2));
                currentTime += Time.deltaTime;
                prodStats.alpha = currentTime * 2;
                menu.alpha = Mathf.Lerp(prodAlpha, 0, currentTime * 2);
                confirm.alpha = 1 - currentTime * 2;
                yield return null;
            }
            menuButton.eulerAngles = new Vector3(0, 0, 0);
            prodStats.alpha = 1;
            confirm.alpha = 0;
            menu.alpha = 0;
            confirm.gameObject.SetActive(false);
        }
        else if (!menuOn)
        {
            if (audioOn)
            {
                sound2.clip = clips[11];
                sound2.volume = volume * 0.25f;
                sound2.Play();
            }
            menuOn = true;
            float confirmAlpha = confirm.alpha;
            float prodAlpha = prodStats.alpha;
            menu.gameObject.SetActive(true);
            float currentTime = 0;
            while (currentTime <= 0.5f)
            {
                menuButton.eulerAngles = new Vector3(0, 0, Mathf.Lerp(0, 180, currentTime * 2));
                currentTime += Time.deltaTime;
                confirm.alpha = Mathf.Lerp(confirmAlpha, 0, currentTime * 2);
                prodStats.alpha = Mathf.Lerp(prodAlpha, 0, currentTime * 2);
                menu.alpha = currentTime * 2;
                yield return null;
            }
            menuButton.eulerAngles = new Vector3(0, 0, 180);
            prodStats.alpha = 0;
            confirm.alpha = 0;
            menu.alpha = 1;
        }
        else if (menuOn)
        {
            if (audioOn)
            {
                sound2.clip = clips[12];
                sound2.volume = volume * 0.25f;
                sound2.Play();
            }
            menuOn = false;
            float confirmAlpha = confirm.alpha;
            float menuAlpha = menu.alpha;
            float currentTime = 0;
            while (currentTime <= 0.5f)
            {
                menuButton.eulerAngles = new Vector3(0, 0, Mathf.Lerp(180, 0, currentTime * 2));
                confirm.alpha = Mathf.Lerp(confirmAlpha, 0, currentTime * 2);
                menu.alpha = Mathf.Lerp(menuAlpha, 0, currentTime * 2);
                prodStats.alpha = currentTime * 2;
                currentTime += Time.deltaTime;
                menu.alpha = 1 - currentTime * 2;
                yield return null;
            }
            menuButton.eulerAngles = new Vector3(0, 0, 0);
            prodStats.alpha = 1;
            confirm.alpha = 0;
            menu.alpha = 0;
            menu.gameObject.SetActive(false);
        }
        yield break;
    }
    float CashPrize(int domain, int era)
    {
        return Mathf.Round((Price(domain, era, 4)) * 10);
    }
    float Price(int domain, int era, int effectCount)
    {
        //return ((artistDomainActive[domain].Artist.Count * available * available * 10 * Mathf.Pow(era, 2)) + (available * 30 * Mathf.Pow((domain + 1), 3)))  * effectCount * priceMultiplicator[domain] * globalPriceModifier;
        return ((15 * Mathf.Pow(artistDomainActive[domain].Artist.Count, 1.7f)) + (10 * Mathf.Pow(available - artistDomainActive[domain].Artist.Count, 1.7f))) * Mathf.Pow(8, era) * Mathf.Pow(2, (domain + 1)) * effectCount * priceModifiers[domain] * globalPriceModifier;
    }
    float ItemPrice(Item item)
    {
        return ((item.Total * 6) * Mathf.Pow(15, item.Era));
    }
    public float EraPrice(int domain, DomainShop shop)
    {
        return 160 * Mathf.Pow(6, shop.era) * Mathf.Pow(2f, (domain + 1)) * ((domain + 1) * 15);
    }
    public void EraPriceDisplay(DomainShop shop)
    {
        if (shop.era == 5)
        {
            shop.eraText.text = shop.era.ToString();
            shop.eraText2.text = "";
        }
        else
        {
            if (shop.price > 1000000000)
            {
                shop.eraText.text = shop.era.ToString();
                shop.eraText2.text = Math.Round(shop.price * 0.000000001, 1) + "B";
            }
            else if (shop.price > 1000000)
            {
                shop.eraText.text = shop.era.ToString();
                shop.eraText2.text = Math.Round(shop.price * 0.000001, 1) + "M";
            }
            else if (shop.price > 1000)
            {
                shop.eraText.text = shop.era.ToString();
                shop.eraText2.text = Math.Round(shop.price * 0.001, 1) + "K";
            }
            else
            {
                shop.eraText.text = shop.era.ToString();
                shop.eraText2.text = shop.price.ToString();
            }
        }
    }
    string CardPriceDisplay(double price)
    {
        if (price > 1000000000)
        {
            return "Price: " + Math.Round(price * 0.000000001, 1) + "B";
        }
        else if (price > 1000000)
        {
            return "Price: " + Math.Round(price * 0.000001, 1) + "M";
        }
        else if (price > 1000)
        {
            return "Price: " + Math.Round(price * 0.001, 1) + "K";
        }
        else
        {
            return "Price: " + price.ToString();
        }
    }
    public IEnumerator DrawCards(int domain, int drawNumber)
    {
        drawing = true;
        panelText.fontSize = 5f;
        panel.gameObject.SetActive(false);
        panelTitle.text = "";
        panelText.text = "";
        panelDisplay = false;
        if (audioOn)
        {
            sound2.clip = clips[6];
            sound.volume = volume * 0.4f;
            sound2.Play();
        }
        if (drawOn)
        {
            drawOn = false;
            if (domainDraw == domain)
            {
                float currentTime = 0;
                blur.blocksRaycasts = false;
                while (currentTime <= 1)
                {
                    currentTime += Time.deltaTime;
                    for (int i = 0; i < cards[domain].Count; i++)
                    {
                        cards[domain][i].localPosition = new Vector2(cards[domain][i].localPosition.x, Mathf.Lerp(cards[domain][i].localPosition.y, 250, currentTime));
                    }
                    yield return null;
                }
                for (int i = 0; i < cards[domain].Count; i++)
                {
                    if (cards[domain][i] != card)
                    {
                        GameObject.Destroy(cards[domain][i].gameObject);
                    }
                }
                cards[domain].Clear();
            }
            else
            {
                float currentTime = 0;
                drawSuspended[domainDraw] = 1;
                while (currentTime <= 1 && drawSuspended[domainDraw] == 1)
                {
                    currentTime += Time.deltaTime;
                    for (int i = 0; i < cards[domainDraw].Count; i++)
                    {
                        cards[domainDraw][i].localPosition = new Vector2(cards[domainDraw][i].localPosition.x, Mathf.Lerp(cards[domainDraw][i].localPosition.y, 250, currentTime));
                    }
                    yield return null;
                }
            }
        }
        drawRef = domain;
        domainDraw = domain;
        if (drawSuspended[domain] == 1)
        {
            drawSuspended[domain] = 0;
            for (int y = 0; y < cards[domain].Count; y++)
            {
                CardSelection script = cards[domain][y].GetComponent<CardSelection>();
                Artist cardArtist = script.artist;
                if (cardArtist.cashPrize != 0)
                {
                    cardArtist.cashPrize = CashPrize(domain, domainShops[domain].era);
                    Transform cashEffect = cards[domain][y].Find("EffectSort").GetChild(0);
                    TextMeshProUGUI cashDescription = cashEffect.Find("Effect Name").GetComponent<TextMeshProUGUI>();
                    cashDescription.text = "Grants you " + cardArtist.cashPrize + " ames immediately.";
                    if (cardArtist.cashPrize > 1000000)
                    {
                        cashDescription.text = "Grants you " + Math.Round(cardArtist.cashPrize * 0.000001f, 1) + "M ames immediately.";
                    }
                    else if (cardArtist.cashPrize > 1000)
                    {
                        cashDescription.text = "Grants you " + Math.Round(cardArtist.cashPrize * 0.001f, 1) + "K ames immediately.";
                    }
                    else
                    {
                        cashDescription.text = "Grants you " + Math.Round(cardArtist.cashPrize, 1) + "ames immediately.";
                    }
                }
                else
                {
                    TextMeshProUGUI cardPrice = cards[domain][y].Find("Production").GetComponent<TextMeshProUGUI>();
                    script.price = Price(domain, cardArtist.Era, cardArtist.Effects.Count);
                    cardPrice.text = CardPriceDisplay(script.price);
                }
            }
            float currentTime = 0;
            blur.blocksRaycasts = true;
            while (currentTime <= 1)
            {
                currentTime += Time.deltaTime;
                blur.alpha = Mathf.Lerp(blur.alpha, 1, currentTime);
                for (int i = 0; i < cards[domain].Count; i++)
                {
                    cards[domain][i].localPosition = new Vector2(cards[domain][i].localPosition.x, Mathf.Lerp(cards[domain][i].localPosition.y, -60, currentTime));
                }
                yield return null;
            }
            drawing = false;
            drawOn = true;
        }
        else
        {
            int shame = 0;
            if (shamefulInfluenceAvailable[domain].Artist.Count > 0)
            {
                if (drawNumber < 5)
                {
                    drawNumber++;
                    shame = 1;
                }
            }
            int[] cardNumbers = new int[drawNumber];
            cards[domain] = new List<Transform>();
            for (int y = 0; y < drawNumber - shame; y++)
            {
                if (domainShops[domain].era < 2 || artistDomainAvailable[domain].Artist.Count <= drawNumber - shame)
                {
                    cardNumbers[y] = y;
                }
                else
                {
                    int verified = 0;
                    int number = UnityEngine.Random.Range(0, artistDomainAvailable[domain].Artist.Count);
                    while (verified < (drawNumber - 1) - shame)
                    {
                        for (int i = 0; i < drawNumber - shame; i++)
                        {
                            if (number == cardNumbers[i])
                            {
                                verified = 0;
                                number = UnityEngine.Random.Range(0, artistDomainAvailable[domain].Artist.Count);
                                i = 0;
                            }
                            else if (y != i)
                            {
                                verified++;
                            }
                        }
                        yield return null;
                    }
                    cardNumbers[y] = number;
                }
            }
            if (shamefulInfluenceAvailable[domain].Artist.Count > 0)
            {
                cardNumbers[drawNumber - 1] = 666;
            }
            for (int y = 0; y < drawNumber; y++)
            {
                cards[domain].Add(GameObject.Instantiate(card, cardPlace).transform);
                cards[domain][y].localScale = new Vector2(0.8f, 0.8f);
                TextMeshProUGUI cardText = cards[domain][y].Find("Name").GetComponent<TextMeshProUGUI>();
                TextMeshProUGUI cardTags = cards[domain][y].Find("Tags").GetComponent<TextMeshProUGUI>();
                TextMeshProUGUI cardDesc = cards[domain][y].Find("Text").GetComponent<TextMeshProUGUI>();
                Transform effectSort = cards[domain][y].Find("EffectSort");
                Artist currentArtist;
                CardSelection script = cards[domain][y].GetComponent<CardSelection>();
                script.domain = domain;
                if (cardNumbers[y] != 666)
                {
                    script.outOfDomain = false;
                    TextMeshProUGUI cardCurrents = cards[domain][y].Find("Currents").GetComponent<TextMeshProUGUI>();
                    currentArtist = artistDomainAvailable[domain].Artist[cardNumbers[y]];
                    cardDesc.text = currentArtist.Text;
                    script.artist = currentArtist;
                    TextMeshProUGUI cardPrice = cards[domain][y].Find("Production").GetComponent<TextMeshProUGUI>();
                    script.price = Price(domain, currentArtist.Era, currentArtist.Effects.Count);
                    cardPrice.text = CardPriceDisplay(script.price);
                    // Read Card Name
                    if (currentArtist.Name2 == null)
                    {
                        if (currentArtist.Death != 9999 && currentArtist.Birth != 9999)
                        {
                            cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + ")";
                        }
                        else if (currentArtist.Birth == 9999)
                        {
                            cardText.text = currentArtist.Name + " (???)";
                        }
                        else
                        {
                            cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "- )";
                        }
                    }
                    else if (currentArtist.Name3 == null && currentArtist.CommonName == null)
                    {
                        if (currentArtist.Death != 9999 && currentArtist.Death2 != 9999)
                        {
                            cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + ") &\n"
                            + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "-" + currentArtist.Death2.ToString() + ")";
                        }
                        else if (currentArtist.Death != 9999)
                        {
                            cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + ") &\n"
                            + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "- )";
                        }
                        else if (currentArtist.Death2 != 9999)
                        {
                            cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "- ) &\n"
                            + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "-" + currentArtist.Death2.ToString() + ")";
                        }
                        else
                        {
                            cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "- ) &\n"
                            + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "- )";
                        }
                    }
                    else if (currentArtist.Name3 == null)
                    {
                        if (currentArtist.Death != 9999 && currentArtist.Death2 != 9999)
                        {
                            cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + ") &\n"
                            + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "-" + currentArtist.Death2.ToString() + ") " + currentArtist.CommonName;
                        }
                        else if (currentArtist.Death != 9999)
                        {
                            cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + ") &\n"
                            + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "- ) " + currentArtist.CommonName;
                        }
                        else if (currentArtist.Death2 != 9999)
                        {
                            cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "- ) &\n"
                            + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "-" + currentArtist.Death2.ToString() + ") " + currentArtist.CommonName;
                        }
                        else
                        {
                            cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "- ) &\n"
                            + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "- ) " + currentArtist.CommonName;
                        }
                    }
                    else
                    {
                        cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + "), "
                            + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "-\n" + currentArtist.Death2.ToString() + ") & "
                            + currentArtist.Name3 + " (" + currentArtist.Birth2.ToString() + "-" + currentArtist.Death3.ToString() + ") " + currentArtist.CommonName;
                    }
                    // Read Card Informations
                    Transform nationSort = cards[domain][y].Find("NationSort");
                    Transform genderSort = cards[domain][y].Find("GenderSort");
                    for (int x = 0; x < currentArtist.nationRefs.Count; x++)
                    {
                        GameObject newNation = GameObject.Instantiate(nation, nationSort);
                        if (currentArtist.nationRefs[x] == 8 && currentArtist.Birth > 1900 && currentArtist.Birth < 1980)
                        {
                            newNation.GetComponent<Image>().sprite = flags[118];
                        }
                        else
                        {
                            newNation.GetComponent<Image>().sprite = flags[currentArtist.nationRefs[x]];
                        }
                        NationMouse nm = newNation.GetComponent<NationMouse>();
                        nm.active = true;
                        nm.nation = currentArtist.Nation[x];
                        nm.pos = cards[domain][y];
                    }
                    for (int x = 0; x < currentArtist.Gender.Count; x++)
                    {
                        GameObject newGender = GameObject.Instantiate(nation, genderSort);
                        newGender.transform.localScale = new Vector2(0.8f, 0.8f);
                        if (currentArtist.Gender[x] == "Female")
                        {
                            newGender.GetComponent<Image>().sprite = genderF;
                        }
                        else if (currentArtist.Gender[x] == "Male")
                        {
                            newGender.GetComponent<Image>().sprite = genderM;
                        }
                        else
                        {
                            newGender.GetComponent<Image>().sprite = genderNB;
                        }
                    }
                    for (int i = 0; i < currentArtist.effectRefs.Count; i++)
                    {
                        Transform newEffect = GameObject.Instantiate(effect, effectSort).transform;
                        TextMeshProUGUI effectTitle = newEffect.Find("Effect Name").GetComponent<TextMeshProUGUI>();
                        TextMeshProUGUI effectDescription = newEffect.Find("Effect").GetComponent<TextMeshProUGUI>();
                        EffectClick click = newEffect.GetComponent<EffectClick>();
                        click.artist = currentArtist;
                        click.effect = i;
                        effectTitle.text = currentArtist.Effects[i];
                        Effect effectRef = effectList.Effect[currentArtist.effectRefs[i]];
                        if (effectRef.Type == "Activism")
                        {
                            newEffect.gameObject.GetComponent<Image>().color = effectColors[0];
                            if (effectRef.SubType == "Feminism")
                            {
                                effectDescription.text = "Heals the Gauge and boosts women Artists";
                            }
                            else if (effectRef.SubType == "Antiracism")
                            {
                                effectDescription.text = "Heals the Gauge and boosts marginalized Artists";
                            }
                            else if (effectRef.SubType == "Queer")
                            {
                                effectDescription.text = "Heals the Gauge and boosts queer Artists";
                            }
                            else
                            {
                                effectDescription.text = "Heals the Gauge";
                            }
                        }
                        else if (effectRef.Type == "Contribution")
                        {
                            newEffect.gameObject.GetComponent<Image>().color = effectColors[1];
                            if (effectRef.SubType == "Global")
                            {
                                effectDescription.text = "Slightly reduces the price of all Cards";
                            }
                            else
                            {
                                effectDescription.text = "Reduces the price of " + domainShortened[domain] + " Cards";
                            }
                        }
                        else if (effectRef.Type == "Feature")
                        {
                            newEffect.gameObject.GetComponent<Image>().color = effectColors[2];
                            effectDescription.text = "Strengthens this Artist's other Effects";
                        }
                        else if (effectRef.Type == "Interdisciplinary current")
                        {
                            newEffect.gameObject.GetComponent<Image>().color = effectColors[3];
                            effectDescription.text = "Boosts each Artist linked to " + effectRef.Name;
                        }
                        else if (effectRef.Type == "Area-defined current")
                        {
                            newEffect.gameObject.GetComponent<Image>().color = effectColors[4];
                            effectDescription.text = "Boosts " + effectRef.Area + " for each Artist in " + domainShortened[domain];
                        }
                        else if (effectRef.Type == "Domain-defined current")
                        {
                            newEffect.gameObject.GetComponent<Image>().color = effectColors[5];
                            effectDescription.text = "Boosts " + domainShortened[domain] + " for each Artist within a minor current";
                        }
                        else if (effectRef.Type == "Group")
                        {
                            newEffect.gameObject.GetComponent<Image>().color = effectColors[6];
                            effectDescription.text = "Strengthens the Gauge's influence";
                        }
                        else if (effectRef.Type == "Title")
                        {
                            newEffect.gameObject.GetComponent<Image>().color = effectColors[7];
                            effectDescription.text = "Strengthens all Effects within " + domainShortened[domain];
                        }
                        else if (effectRef.Type == "Prize")
                        {
                            newEffect.gameObject.GetComponent<Image>().color = effectColors[8];
                            effectDescription.text = "Slightly boosts everyone for each represented country";
                        }
                        else if (effectRef.Type == "Discovery")
                        {
                            newEffect.gameObject.GetComponent<Image>().color = effectColors[9];
                            if (effectRef.Area != "")
                            {
                                effectDescription.text = "Boosts " + domainShortened[domain] + " for each Artist in " + effectRef.Area;
                            }
                            else
                            {
                                effectDescription.text = "Boosts " + domainShortened[domain] + " for each Artist in " + effectRef.SubType;
                            }
                        }
                        else if (effectRef.Type == "Genre")
                        {
                            newEffect.gameObject.GetComponent<Image>().color = effectColors[10];
                            effectDescription.text = "Boosts " + domainShortened[domain] + " for each of its Artists";
                        }
                        else if (effectRef.Type == "Activity")
                        {
                            newEffect.gameObject.GetComponent<Image>().color = effectColors[11];
                            if (effectRef.Name == "Multitalented Artist")
                            {
                                effectDescription.text = "Slightly boosts " + domainShortened[domain] + " for each Artist";
                            }
                            else if (effectList.Effect[currentArtist.effectRefs[i]].Domain.Count > 2)
                            {
                                int domainReference = 0;
                                for (int z = 0; z < artistDomainActive.Count; z++)
                                {
                                    if (effectRef.Domain[0] == artistDomainActive[z].name)
                                    {
                                        domainReference = z;
                                    }
                                }
                                effectDescription.text = "Boosts " + domainShortened[domainReference] + " for each Artist in these two Domains";
                            }
                            else
                            {
                                int domainReference = 0;
                                for (int z = 0; z < artistDomainActive.Count; z++)
                                {
                                    if (effectRef.Domain[0] == artistDomainActive[z].name)
                                    {
                                        domainReference = z;
                                    }
                                }
                                int domainReference2 = 0;
                                for (int z = 0; z < artistDomainActive.Count; z++)
                                {
                                    if (effectRef.Domain[1] == artistDomainActive[z].name)
                                    {
                                        domainReference2 = z;
                                    }
                                }
                                if (domainReference == domainReference2) { effectDescription.text = "Boosts " + domainShortened[domainReference] + " for each of its Artists"; }
                                else { effectDescription.text = "Boosts " + domainShortened[domainReference] + " for each Artist in " + domainShortened[domainReference2]; }
                            }
                        }
                        else if (effectRef.Type == "Achievement")
                        {
                            newEffect.gameObject.GetComponent<Image>().color = effectColors[12];
                            effectDescription.text = "Slightly boosts everyone for each Artist in " + domainShortened[domain];
                        }
                    }
                    if (currentArtist.Tags[0] == "None")
                    {
                        cardTags.text = " ";
                    }
                    else if (currentArtist.Tags.Count == 1)
                    {
                        cardTags.text = currentArtist.Tags[0];
                    }
                    else if (currentArtist.Tags.Count == 2)
                    {
                        cardTags.text = currentArtist.Tags[0] + ", " + currentArtist.Tags[1];
                    }
                    else
                    {
                        cardTags.text = currentArtist.Tags[0] + ", " + currentArtist.Tags[1] + ", " + currentArtist.Tags[2];
                    }
                    if (currentArtist.Currents[0] == "None")
                    {
                        cardCurrents.text = " ";
                    }
                    else if (currentArtist.Currents.Count == 1)
                    {
                        cardCurrents.text = currentArtist.Currents[0];
                    }
                    else if (currentArtist.Currents.Count == 2)
                    {
                        cardCurrents.text = currentArtist.Currents[0] +
                            ", " + currentArtist.Currents[1];
                    }
                    else if (currentArtist.Currents.Count == 3)
                    {
                        cardCurrents.text = currentArtist.Currents[0] +
                            ", " + currentArtist.Currents[1] +
                            ", " + currentArtist.Currents[2];
                    }
                    else if (currentArtist.Currents.Count == 4)
                    {
                        cardCurrents.text = currentArtist.Currents[0] +
                            ", " + currentArtist.Currents[1] +
                            ", " + currentArtist.Currents[2] +
                            ", " + currentArtist.Currents[3];
                    }
                    else if (currentArtist.Currents.Count == 5)
                    {
                        cardCurrents.text = currentArtist.Currents[0] +
                            ", " + currentArtist.Currents[1] +
                            ", " + currentArtist.Currents[2] +
                            ", " + currentArtist.Currents[3] +
                            ", " + currentArtist.Currents[4];
                    }
                    // Check Availability
                    bool OK = true;
                    if (currentArtist.currentRefs != null)
                    {
                        for (int z = 0; z < currentArtist.currentRefs.Count; z++)
                        {
                            if (!currents[currentArtist.currentRefs[z]].available)
                            {
                                OK = false;
                            }
                        }
                    }
                    if (!OK)
                    {
                        currentArtist.Locked = true;
                    }
                    else
                    {
                        currentArtist.Locked = false;
                    }
                }
                else
                {
                    script.outOfDomain = true;
                    currentArtist = shamefulInfluenceAvailable[domain].Artist[UnityEngine.Random.Range(0, shamefulInfluenceAvailable[domain].Artist.Count)];
                    cardDesc.text = currentArtist.Text;
                    script.artist = currentArtist;
                    cardText.text = currentArtist.Name;
                    cardText.alignment = TextAlignmentOptions.Center;
                    cards[domain][y].Find("Title").gameObject.SetActive(false);
                    TextMeshProUGUI cardEffect = cards[domain][y].Find("Currents").GetComponent<TextMeshProUGUI>();
                    cardEffect.color = colorMalusEffect2;
                    cardEffect.text = currentArtist.Influence;
                    cardTags.color = colorMalusEffect;
                    cardTags.text = currentArtist.Shame;
                    Transform cashEffect = GameObject.Instantiate(effect, effectSort).transform;
                    TextMeshProUGUI cashDescription = cashEffect.Find("Effect Name").GetComponent<TextMeshProUGUI>();
                    cashEffect.Find("Effect").gameObject.SetActive(false);
                    cashEffect.gameObject.GetComponent<Image>().color = colorMalusEffect2;
                    currentArtist.cashPrize = CashPrize(domain, domainShops[domain].era);
                    cashDescription.text = "Grants you " + currentArtist.cashPrize + " ames immediately.";
                    if (currentArtist.cashPrize > 1000000)
                    {
                        cashDescription.text = "Grants you " + Math.Round(currentArtist.cashPrize * 0.000001f, 1) + "M ames immediately.";
                    }
                    else if (currentArtist.cashPrize > 1000)
                    {
                        cashDescription.text = "Grants you " + Math.Round(currentArtist.cashPrize * 0.001f, 1) + "K ames immediately.";
                    }
                    else
                    {
                        cashDescription.text = "Grants you " + Math.Round(currentArtist.cashPrize, 1) + "ames immediately.";
                    }
                    cashDescription.alignment = TextAlignmentOptions.Midline;
                    cashDescription.color = Color.white;
                    Transform badEffect = GameObject.Instantiate(effect, effectSort).transform;
                    TextMeshProUGUI badDescription = badEffect.Find("Effect Name").GetComponent<TextMeshProUGUI>();
                    badEffect.gameObject.GetComponent<Image>().color = colorMalusEffect;
                    badEffect.Find("Effect").gameObject.SetActive(false);
                    badDescription.text = "Brutally hurts the Gauge.";
                    badDescription.alignment = TextAlignmentOptions.Midline;
                    badDescription.color = Color.white;
                    currentArtist.Locked = false;
                    cards[domain][y].Find("Image").GetComponent<Image>().color = colorMalusCard;
                }
            }
            // Animate cards
            float currentTime = 0;
            blur.blocksRaycasts = true;
            while (currentTime <= 1)
            {
                currentTime += Time.deltaTime;
                blur.alpha = Mathf.Lerp(blur.alpha, 1, currentTime);
                for (int i = 0; i < drawNumber; i++)
                {
                    cards[domain][i].localPosition = new Vector2(cards[domain][i].localPosition.x, Mathf.Lerp(cards[domain][i].localPosition.y, -60, currentTime));
                }
                yield return null;
            }
            if (drawNumber > 1)
            {
                if (audioOn)
                {
                    sound2.clip = clips[2];
                    sound2.volume = volume;
                    sound2.Play();
                }
                currentTime = 0;
                while (currentTime <= 1)
                {
                    currentTime += Time.deltaTime;
                    for (int i = 0; i < drawNumber; i++)
                    {
                        cards[domain][i].localPosition = new Vector2(Mathf.Lerp(cards[domain][i].localPosition.x, -(40 * drawNumber) + (95 * i), currentTime), cards[domain][i].localPosition.y);
                    }
                    yield return null;
                }
            }
            drawing = false;
            drawOn = true;
        }
        yield break;
    }
    public IEnumerator HideDraw(int domain)
    {
        drawing = true;
        if (audioOn)
        {
            sound2.clip = clips[4];
            sound2.volume = volume;
            sound2.Play();
        }
        float currentTime = 0;
        drawSuspended[domain] = 1;
        drawOn = false;
        blur.blocksRaycasts = false;
        while (currentTime <= 1 && drawSuspended[domain] == 1)
        {
            currentTime += Time.deltaTime;
            blur.alpha = 1 - currentTime;
            for (int i = 0; i < cards[domain].Count; i++)
            {
                cards[domain][i].localPosition = new Vector2(cards[domain][i].localPosition.x, Mathf.Lerp(cards[domain][i].localPosition.y, 250, currentTime));
            }
            yield return null;
        }
        blur.alpha = 0;
        drawing = false;
        yield break;
    }
    public IEnumerator CardSelect(int domain, Artist artist, Transform newCard, bool outOfDomain)
    {
        drawing = true;
        newCard.GetComponent<CardSelection>().enabled = false;
        newCard.GetComponent<EventTrigger>().enabled = false;
        if (audioOn)
        {
            sound2.clip = clips[13];
            sound2.volume = volume * 3;
            sound2.Play();
        }
        drawOn = false;
        newCard.localScale = new Vector2(1, 1);
        Artist current = artist;
        // Organize Cards
        current.Card = newCard;
        Transform movedCard = newCard;
        ArtistList currentDomain = artistDomainActive[0];
        if (outOfDomain)
        {
            cardBoxShame.parent.GetComponent<BoxMouse>().cardNumber++;
            for (int i = 0; i < shames.Count; i++)
            {
                if (shames[i].name == current.Shame)
                {
                    shames[i].amount++;
                }
            }
            currentDomain = shamefulInfluenceActive;
            int domainRef = 0;
            for (int i = 0; i < artistDomainAvailable.Count; i++)
            {
                if (artistDomainAvailable[i].name == current.Domain)
                {
                    domainRef = i;
                }
            }
            currentDomain.Artist.Add(current);
            save.shamefulInfluenceActive.Add(current.Name);
            shamefulInfluenceAvailable[domainRef].Artist.Remove(current);
        }
        else
        {
            cardBoxes[domain].parent.GetComponent<BoxMouse>().cardNumber++;
            currentDomain = artistDomainActive[domain];
            domainShops[domain].availableCards--;
            domainShops[domain].cardText.text = domainShops[domain].availableCards.ToString();
            domainShops[domain].cardText2.text = artistDomainLocked[domain].Artist.Count.ToString();
            currentDomain.Artist.Add(current);
            save.artistDomainActive[domain].Add(current.Name);
            save.isArtistLead[domain].Add(current.Lead);
            artistDomainAvailable[domain].Artist.Remove(current);
            for (int w = 0; w < current.Nation.Count; w++)
            {
                newCard.Find("NationSort").GetChild(w).GetComponent<NationMouse>().cardActive = true;
            }
        }
        currentDomain.Artist.Sort((y, x) => x.Birth.CompareTo(y.Birth));
        currentDomain.Artist.Reverse();
        float translationAmount = 350;
        if (currentDomain.Artist.Count > 1)
        {
            for (int i = 0; i < currentDomain.Artist.Count; i++)
            {
                if (currentDomain.Artist[i].Card == newCard && i + 1 < currentDomain.Artist.Count)
                {
                    movedCard = currentDomain.Artist[i + 1].Card;
                    translationAmount = 100;
                    break;
                }
            }
        }
        // Hide Cards
        float currentTime = 0;
        blur.blocksRaycasts = false;
        while (currentTime <= 1)
        {
            currentTime += Time.deltaTime;
            blur.alpha = 1 - currentTime;
            for (int i = 0; i < cards[domain].Count; i++)
            {
                if (cards[domain][i] != newCard)
                {
                    cards[domain][i].localPosition = new Vector2(cards[domain][i].localPosition.x, Mathf.Lerp(cards[domain][i].localPosition.y, 250, currentTime));
                }
                else if (movedCard != newCard)
                {
                    cards[domain][i].localPosition = new Vector2(cards[domain][i].localPosition.x, Mathf.Lerp(cards[domain][i].localPosition.y, -250, currentTime));
                    movedCard.localPosition = new Vector2(Mathf.Lerp(movedCard.localPosition.x, movedCard.localPosition.x + translationAmount, currentTime), movedCard.localPosition.y);
                }
                else
                {
                    movedCard.localPosition = new Vector2(movedCard.localPosition.x, Mathf.Lerp(movedCard.localPosition.y, -250, currentTime));
                }
            }
            yield return null;
        }
        blur.alpha = 0;
        // Set Parents
        if (!outOfDomain)
        {
            currentDomain.Artist[0].Card.SetParent(cardBoxes[domain]);
            for (int i = 1; i < currentDomain.Artist.Count; i++)
            {
                currentDomain.Artist[i].Card.SetParent(currentDomain.Artist[i - 1].Card.Find("NextCard"));
                currentDomain.Artist[i].Card.localPosition = Vector2.zero;
            }
        }
        else
        {
            shamefulInfluenceActive.Artist[0].Card.SetParent(cardBoxShame);
            for (int i = 1; i < shamefulInfluenceActive.Artist.Count; i++)
            {
                shamefulInfluenceActive.Artist[i].Card.SetParent(shamefulInfluenceActive.Artist[i - 1].Card.Find("NextCard"));
                shamefulInfluenceActive.Artist[i].Card.localPosition = Vector2.zero;
            }
        }
        // Update Stats
        available++;
        if (!outOfDomain)
        {
            current.Prod = newCard.Find("Production").GetComponent<TextMeshProUGUI>();
            current.Prod.fontSize = 4;
            for (int i = 0; i < current.nationRefs.Count; i++)
            {
                nations[current.nationRefs[i]].amount++;
            }
            for (int i = 0; i < current.tagRefs.Count; i++)
            {
                tags[current.tagRefs[i]].amount++;
            }
            for (int i = 0; i < current.currentRefs.Count; i++)
            {
                currents[current.currentRefs[i]].amount++;
            }
            if (current.Gender.Count > 1)
            {
                men++;
                women++;
            }
            else if (current.Gender[0] == "Female")
            {
                women++;
            }
            else
            {
                men++;
            }
            int cardEra = 1;
            if (current.Birth > 1970)
            {
                cardEra = 7;
            }
            else if (current.Birth > 1940)
            {
                cardEra = 6;
            }
            else if (current.Birth > 1905)
            {
                cardEra = 5;
            }
            else if (current.Birth > 1870)
            {
                cardEra = 4;
            }
            else if (current.Birth > 1835)
            {
                cardEra = 3;
            }
            else if (current.Birth > 1800)
            {
                cardEra = 2;
            }
            if (newsEra < cardEra)
            {
                for (int i = 0; i < majorCurrents.Length; i++)
                {
                    if (!majorCurrents[i].Owned)
                    {
                        if (majorCurrents[i].Era <= cardEra && !majorCurrents[i].Unlocked)
                        {
                            GameObject newItem = (GameObject)Instantiate(storeItem, storeFront);
                            availableItems.Add(new Item(newItem.transform, majorCurrents[i].Name, "Major Current"));
                            save.availableItems.Add(majorCurrents[i].Name);
                            newItems.Add(newItem.GetComponent<CanvasGroup>());
                            newItem.GetComponent<CanvasGroup>().alpha = 0;
                            newItem.GetComponent<ShopClick>().current = availableItems[availableItems.Count - 1];
                            availableItems[availableItems.Count - 1].icon = currentIcons[i];
                            newItem.transform.GetChild(0).GetComponent<Image>().sprite = availableItems[availableItems.Count - 1].icon;
                            availableItems[availableItems.Count - 1].SubGenders = new List<string>();
                            availableItems[availableItems.Count - 1].Era = majorCurrents[i].Era;
                            if (majorCurrents[i].SubCurrents != null)
                            {
                                for (int y = 0; y < majorCurrents[i].SubCurrents.Count; y++)
                                {
                                    availableItems[availableItems.Count - 1].SubGenders.Add(currents[majorCurrents[i].SubCurrents[y]].name);
                                }
                            }
                            availableItems[availableItems.Count - 1].Total = majorCurrents[i].Total;
                            availableItems[availableItems.Count - 1].Price = ItemPrice(availableItems[availableItems.Count - 1]);
                            majorCurrents[i].Unlocked = true;
                        }
                    }
                    newsEra = cardEra;
                    newsChange = true;
                }
                StartCoroutine(ItemsFade());
            }
        }
        Calculation();
        // Set Cards
        if (movedCard != newCard)
        {
            movedCard.localPosition = Vector2.zero;
        }
        Vector2 destination = Vector2.zero;
        if (newCard == currentDomain.Artist[0].Card)
        {
            destination = new Vector2(0, 460);
        }
        else
        {
            currentDomain.Artist[0].Card.transform.localPosition = new Vector2(0, 460);
        }
        newCard.localPosition = new Vector2(destination.x + 100, destination.y);
        currentTime = 0;
        if (available >= societies[currentSociety + 1].Population)
        {
            currentSociety++;
            save.currentSociety = currentSociety;
            transClick.transform.localPosition = new Vector2(transClick.transform.localPosition.x, -27);
            transText.transform.localPosition = new Vector2(transText.transform.localPosition.x, -10);
            transTitle.text = societies[currentSociety - 1].Name + " becomes The " + societies[currentSociety].Name + ".";
            transSubTitle.text = "Your gathering reached the milestone of " + societies[currentSociety].Population + " Artists.";
            if (currentSociety == 3 || currentSociety == 6 || currentSociety == 9 || currentSociety == 12)
            {
                if (currentSociety == 3)
                {
                    slotsList.parent.Find("Trendsetters").GetComponent<TextMeshProUGUI>().text = "Global Trendsetters";
                }
                trendSlots[0].SetActive(true);
                headerDrops.Add(trendSlots[0].transform);
                trendSlots.Remove(trendSlots[0]);
                transText.text = "A shift in your scale of activity increases your global ame production by 50%, and you can have one more global Trendsetter.";
            }
            else
            {
                transText.text = "A shift in your scale of activity increases your global ame production by 50%.";
            }
            transClick.alpha = 0;
            transText.fontSize = 6;
            transTitle.alpha = 1;
            transSubTitle.alpha = 1;
            transText.alpha = 1;
            transParagraph.alpha = 1;
            transPanel.gameObject.SetActive(true);
            currentTime = 0;
            while (currentTime <= 1)
            {
                newCard.localPosition = Vector2.Lerp(newCard.localPosition, destination, currentTime);
                currentTime += Time.deltaTime;
                transPanel.alpha = currentTime;
                yield return null;
            }
            click = true;
            transPanel.alpha = 1;
            eraMultiplicator += eraFactor;
            name1.text = societies[currentSociety].Name;
            name2.text = societies[currentSociety].Name;
            Calculation();
            currentTime = 0;
            while (currentTime <= 1 && click)
            {
                currentTime += Time.deltaTime;
                transClick.alpha = currentTime;
                yield return null;
            }
            while (click) { yield return null; }
            currentTime = 0;
            while (currentTime <= 1)
            {
                currentTime += Time.deltaTime;
                transClick.alpha = 1 - currentTime;
                transPanel.alpha = 1 - currentTime;
                yield return null;
            }
            transClick.alpha = 1;
        }
        else
        {
            while (currentTime <= 1)
            {
                newCard.localPosition = Vector2.Lerp(newCard.localPosition, destination, currentTime);
                currentTime += Time.deltaTime;
                yield return null;
            }
        }
        newCard.localPosition = destination;
        for (int i = 0; i < cards[domain].Count; i++)
        {
            if (cards[domain][i] != newCard)
            {
                GameObject.Destroy(cards[domain][i].gameObject);
            }
        }
        cards[domain].Clear();
        // Set MouseDetector;
        GameObject detectorObject = newCard.Find("MouseDetector").gameObject;
        detectorObject.SetActive(true);
        MouseDetector detector = detectorObject.GetComponent<MouseDetector>();
        detector.nextCard = newCard.Find("NextCard").transform;
        detector.currentCard = newCard;
        if (!outOfDomain)
        {
            detector.box = cardBoxes[domain].parent.GetComponent<BoxMouse>();
            detector.cardColor = domainColors[domain];
            detector.domain = domain;
            detector.artist = current;
            if (current.Name2 == null)
            {
                detector.artistName = current.Name;
            }
            else if (current.Name3 == null && current.CommonName == null)
            {
                detector.artistName = current.Name + " & " + current.Name2;
            }
            else if (current.Name3 == null)
            {
                detector.artistName = current.Name + ", " + current.Name2 + " & " + current.Name3;
            }
            else
            {
                detector.artistName = current.Name + ", " + current.Name2 + " & " + current.Name3 + current.CommonName;
            }
            detector.effectName = new string[current.Effects.Count];
            detector.effectColor = new Color[current.Effects.Count];
            for (int i = 0; i < current.effectRefs.Count; i++)
            {
                detector.effectName[i] = current.Effects[i];
                Effect effectRef = effectList.Effect[current.effectRefs[i]];
                if (effectRef.Type == "Activism")
                {
                    detector.effectColor[i] = effectColors[0];
                }
                else if (effectRef.Type == "Contribution")
                {
                    detector.effectColor[i] = effectColors[1];
                }
                else if (effectRef.Type == "Feature")
                {
                    detector.effectColor[i] = effectColors[2];
                }
                else if (effectRef.Type == "Interdisciplinary current")
                {
                    detector.effectColor[i] = effectColors[3];
                }
                else if (effectRef.Type == "Area-defined current")
                {
                    detector.effectColor[i] = effectColors[4];
                }
                else if (effectRef.Type == "Domain-defined current")
                {
                    detector.effectColor[i] = effectColors[5];
                }
                else if (effectRef.Type == "Group")
                {
                    detector.effectColor[i] = effectColors[6];
                }
                else if (effectRef.Type == "Title")
                {
                    detector.effectColor[i] = effectColors[7];
                }
                else if (effectRef.Type == "Prize")
                {
                    detector.effectColor[i] = effectColors[8];
                }
                else if (effectRef.Type == "Discovery")
                {
                    detector.effectColor[i] = effectColors[9];
                }
                else if (effectRef.Type == "Genre")
                {
                    detector.effectColor[i] = effectColors[10];
                }
                else if (effectRef.Type == "Activity")
                {
                    detector.effectColor[i] = effectColors[11];
                }
                else if (effectRef.Type == "Achievement")
                {
                    detector.effectColor[i] = effectColors[12];
                }
            }
        }
        else
        {
            detector.outOfDomain = true;
        }
        // Checking Age
        drawing = false;
        yield break;
    }
    public IEnumerator BuyItem(Item item)
    {
        panel.gameObject.SetActive(false);
        panelTitle.fontSize = 5;
        panelTitle.text = "";
        panelText.text = "";
        panelDisplay = false;
        cardBoxItems.parent.GetComponent<BoxMouse>().cardNumber++;
        save.activeItems.Add(item.Name);
        drawing = true;
        for (int i = 0; i < currents.Count; i++)
        {
            if (item.Name == currents[i].name)
            {
                currents[i].available = true;
            }
            if (item.SubGenders != null)
            {
                for (int y = 0; y < item.SubGenders.Count; y++)
                {
                    if (item.SubGenders[y] == currents[i].name)
                    {
                        currents[i].available = true;
                    }
                }
            }
        }
        for (int i = 0; i < artistDomainActive.Count; i++)
        {
            if (cards[i] != null)
            {
                for (int y = 0; y < cards[i].Count; y++)
                {
                    bool available = true;
                    Artist artist = cards[i][y].GetComponent<CardSelection>().artist;
                    if (artist.currentRefs != null)
                    {
                        for (int z = 0; z < artist.currentRefs.Count; z++)
                        {
                            if (!currents[artist.currentRefs[z]].available)
                            {
                                available = false;
                            }
                        }
                    }
                    if (!available)
                    {
                        artist.Locked = true;
                    }
                    else
                    {
                        artist.Locked = false;
                    }
                }
            }
        }
        availableItems.Remove(item);
        save.availableItems.Remove(item.Name);
        GameObject.Destroy(item.Package.gameObject);
        activeItems.Add(item);
        Transform parent = cardBoxItems;
        item.SubTexts = new List<TextMeshProUGUI>();
        item.SubRefs = new List<int>();
        if (activeItems.Count > 1)
        {
            Transform cardParent = activeItems[activeItems.Count - 2].card;
            ItMouseDetector mouse = cardParent.Find("MouseDetector").GetComponent<ItMouseDetector>();
            if (!mouse.small)
            {
                mouse.background.gameObject.SetActive(true);
            }
            parent = cardParent.Find("NextCard").transform;
        }
        if (item.SubGenders.Count > 1)
        {
            item.card = Instantiate(bigCurrent, parent).transform;
            RectTransform subList = item.card.transform.Find("SubCurrents").GetComponent<RectTransform>();
            for (int i = 0; i < item.SubGenders.Count; i++)
            {
                GameObject sub = Instantiate(SubCurrent, subList);
                int count = 0;
                for (int y = 0; y < currents.Count; y++)
                {
                    if (currents[y].name == item.SubGenders[i])
                    {
                        item.SubRefs.Add(y);
                        count = currents[y].amount;
                    }
                }
                item.SubTexts.Add(sub.GetComponent<TextMeshProUGUI>());
                item.SubTexts[i].text = item.SubGenders[i] + ": " + count + " artists";
            }
        }
        else
        {
            item.card = Instantiate(smallCurrent, parent).transform;
            int totalArtists = 0;
            for (int y = 0; y < currents.Count; y++)
            {
                if (currents[y].name == item.Name)
                {
                    item.SubRefs.Add(y);
                    totalArtists = currents[y].total;
                }
            }
            item.SubTexts.Add(item.card.Find("Amount").GetComponent<TextMeshProUGUI>());
            item.SubTexts[0].text = "Artists: " + item.Total;
            item.card.Find("Total").GetComponent<TextMeshProUGUI>().text = "Maximum: " + totalArtists;
        }
        //item.card.SetParent(parent);
        item.card.Find("Name").GetComponent<TextMeshProUGUI>().text = item.Name;
        item.card.Find("Era").GetComponent<TextMeshProUGUI>().text = item.Era.ToString();
        item.card.Find("Icon").GetComponent<Image>().sprite = item.icon;
        item.card.localScale = Vector2.one;
        Vector2 destination = Vector2.zero;
        if (item == activeItems[0])
        {
            if (item.SubGenders.Count > 1)
            {
                destination = new Vector2(0, 483.8f);
            }
            else
            {
                destination = new Vector2(0, 510);
            }
        }
        else if (item.SubGenders.Count > 1)
        {
            destination = new Vector2(0, -26.2f);
        }
        item.card.GetComponent<RectTransform>().localPosition = destination;
        ItMouseDetector detector = item.card.Find("MouseDetector").GetComponent<ItMouseDetector>();
        detector.nextCard = item.card.Find("NextCard").transform;
        detector.box = cardBoxItems.parent.GetComponent<BoxMouse>();
        if (item.SubGenders.Count < 2)
        {
            detector.Small();
        }
        else
        {
            detector.background = item.card.Find("NextCard").Find("Background").GetComponent<RectTransform>();
            detector.background.gameObject.SetActive(false);
            StartCoroutine(detector.PointerOutCo());
        }
        if (gameOn)
        {
            drawing = false;
        }
        yield break;
    }
    public void EraChange(int domain, DomainShop shop)
    {
        erasPassed++;
        statisticTexts[2].text = "<b>Eras:</b> " + erasPassed + "%";
        for (int i = 0; i < artistDomainLocked[domain].Artist.Count; i++)
        {
            if (artistDomainLocked[domain].Artist[i].Era == shop.era)
            {
                artistDomainAvailable[domain].Artist.Add(artistDomainLocked[domain].Artist[i]);
                artistDomainLocked[domain].Artist.Remove(artistDomainLocked[domain].Artist[i]);
                i--;
            }
        }
        for (int i = 0; i < shamefulInfluenceLocked.Artist.Count; i++)
        {
            if (shamefulInfluenceLocked.Artist[i].Domain == artistDomainAvailable[domain].name)
            {
                if (shamefulInfluenceLocked.Artist[i].Era <= shop.era)
                {
                    shamefulInfluenceAvailable[domain].Artist.Add(shamefulInfluenceLocked.Artist[i]);
                    shamefulInfluenceLocked.Artist.Remove(shamefulInfluenceLocked.Artist[i]);
                }
            }
        }
        shop.price = EraPrice(domain, shop);
        scrollTexts[domain + 1].text = artistDomainAvailable[domain].name;
        scrollTexts[domain + 1].color = domainColors[domain];
        if (domain > 1 && shop.era < 2 && domainScroll < 20)
        {
            domainScroll++;
        }
        if (domainScroll > 18)
        {
            scrollTexts[21].text = "Shameful Influences";
        }
        if (shop.era == 5)
        {
            bool max = true;
            for (int i = 0; i < domainShops.Length; i++)
            {
                if (domainShops[i].era < 5)
                {
                    max = false;
                }
            }
            if (max)
            {
                endAvailable = true;
                for (int i = 0; i < domainShops.Length; i++)
                {
                    domainShops[i].eraText.text = 6.ToString();
                    domainShops[i].eraText2.text = 1000000000000.ToString();
                }
            }
        }
        for (int i = 0; i < domainShops.Length; i++)
        {
            save.eraList[i] = domainShops[i].era;
        }
    }
    public IEnumerator EndGame()
    {
        statisticTexts[2].text = "<b>Eras:</b> " + 101 + "%";
        transPanel.gameObject.SetActive(true);
        transBackground.gameObject.SetActive(true);
        transBackground.transform.SetParent(transPanel.transform.parent);
        RectTransform rect = transPanel.GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, 122.9f);
        transSubTitle.GetComponent<RectTransform>().sizeDelta = new Vector2(138.75f, transSubTitle.GetComponent<RectTransform>().sizeDelta.y);
        transPanel.transform.localPosition = new Vector2(0, 71.3f);
        transClick.transform.localPosition = new Vector2(transClick.transform.localPosition.x, -44.2f);
        transText.transform.localPosition = new Vector2(transText.transform.localPosition.x, -13.19f);
        transTitle.transform.localPosition = new Vector2(transTitle.transform.localPosition.x, 49.32f);
        transSubTitle.transform.localPosition = new Vector2(transSubTitle.transform.localPosition.x, -7.3f);
        transTitle.text = "The time has come for the earth to end";
        transSubTitle.text = "But you've done your part.";
        transText.text = "We've been carefully observing the W.A.S as the plans for earth's use were being prepared. It has been decided that the W.A.S archive would be acquired and kept within our own archival system, as a testimony of what once existed on this planet. " +
            "We must admit that some of the ideas developed within the W.A.S even had quite an influence on our old society, especially that \"game\" thing. " +
            "It was met with a particularly enthusiastic opinion amongst our educators, and a plan for an adaptation of the W.A.S history into a game for our children is set to be realized soon, so they can imagine, as a formative experience, these great impressions we had while destroying earth." +
            "We may even enjoy it sometimes, and remember...";
        transText.fontSize = 6;
        transTitle.fontSize = 8;
        transText.alignment = TextAlignmentOptions.Justified;
        transTitle.alpha = 1;
        transSubTitle.alpha = 0;
        transText.alpha = 0;
        transClick.alpha = 0;
        transParagraph.alpha = 1;
        float currentTime = 0;
        while (currentTime <= 2)
        {
            currentTime += Time.deltaTime;
            transBackground.alpha = currentTime * 0.5f;
            yield return null;
        }
        currentTime = 0;
        while (currentTime <= 2)
        {
            currentTime += Time.deltaTime;
            transPanel.alpha = currentTime * 0.5f;
            yield return null;
        }
        transPanel.alpha = 1;
        currentTime = 0;
        while (currentTime <= 2)
        {
            currentTime += Time.deltaTime;
            transSubTitle.alpha = currentTime * 0.5f;
            yield return null;
        }
        currentTime = 0;
        while (currentTime <= 4)
        {
            currentTime += Time.deltaTime;
            transText.alpha = currentTime * 0.25f;
            yield return null;
        }
        while (click) { yield return null; }
        yield break;
    }
    public void DomainDropOn(int domain)
    {
        if (domain == cardDomain)
        {
            domainDrop = domain;
            DomainDropAvailable = true;
        }
    }
    public void DomainDropOff()
    {
        DomainDropAvailable = false;
    }
    public void HeaderDropOn(int header)
    {
        headerDrop = header;
        HeaderDropAvailable = true;
    }
    public void HeaderDropOff()
    {
        HeaderDropAvailable = false;
    }
    public void Calculation()
    {
        SaveGame();
        // Reset
        globalMultiplicatorEffect = 0;
        globalMultiplicatorEffectiveness = globalOrigin;
        globalPriceModifier = 1;
        production = 0;
        domainDefined = 0;
        nationCount = 0;
        for (int i = 0; i < priceModifiers.Length; i++)
        {
            priceModifiers[i] = 1;
        }
        for (int z = 0; z < domainEffectMultiplicator.Length; z++)
        {
            domainEffectMultiplicator[z] = 1;
        }
        for (int z = 0; z < domainMultiplicators.Length; z++)
        {
            domainMultiplicators[z] = 0;
        }
        for (int z = 0; z < areas.Count; z++)
        {
            areas[z].multiplicator = 0;
        }
        for (int z = 0; z < tags.Count; z++)
        {
            tags[z].multiplicator = 0;
        }
        for (int z = 0; z < currents.Count; z++)
        {
            currents[z].multiplicator = 0;
        }
        for (int z = 0; z < domainProduction.Length; z++)
        {
            domainProduction[z] = 0;
        }
        // Stats
        int marginalized = 0;
        int marginalized2 = 0;
        int queer = 0;
        for (int i = 0; i < areas.Count; i++)
        {
            areas[i].amount = 0;
        }
        for (int i = 0; i < nations.Count; i++)
        {
            areas[nations[i].area].amount += nations[i].amount;
            if (nations[i].amount > 0)
            {
                nationCount++;
            }
        }
        for (int i = 0; i < areas.Count; i++)
        {
            if (areas[i].name != "North America" && areas[i].name != "Eastern Europe" && areas[i].name != "Western Europe")
                marginalized += areas[i].amount;
        }
        for (int i = 0; i < tags.Count; i++)
        {
            if (tags[i].name == "Racialized" || tags[i].name == "Native")
            {
                marginalized += tags[i].amount;
            }
            if (tags[i].name == "Executed" || tags[i].name == "Institutionalized" || tags[i].name == "Lifelong Illness" || tags[i].name == "Censored" || tags[i].name == "Disability" || tags[i].name == "Exile" || tags[i].name == "Persecuted" || tags[i].name == "Queer")
            {
                marginalized2 += tags[i].amount;
            }
            if (tags[i].name == "Queer")
            {
                queer = tags[i].amount;
            }
        }
        for (int i = 0; i < activeItems.Count; i++)
        {
            if (activeItems[i].SubGenders.Count > 1)
            {
                for (int y = 0; y < activeItems[i].SubGenders.Count; y++)
                {
                    activeItems[i].SubTexts[y].text = currents[activeItems[i].SubRefs[y]].name + ": " + currents[activeItems[i].SubRefs[y]].amount.ToString();
                }
            }
            else
            {
                activeItems[i].SubTexts[0].text = "Artists: " + currents[activeItems[i].SubRefs[0]].amount.ToString();
            }
        }
        // Ratios
        imperialRatio = marginalized - (available * 0.5f);
        justiceRatio = marginalized2 - (available * 0.25f);
        patriarcalRatio = women - men;
        heteroRatio = (queer * 9) - available;
        for (int i = 0; i < shames.Count; i++)
        {
            if (shames[i].name == "Racism")
            {
                imperialRatio -= shames[i].amount * 40;
            }
            else if (shames[i].name == "Abuse" || shames[i].name == "Misogyny")
            {
                patriarcalRatio -= shames[i].amount * 40;
            }
            else if (shames[i].name == "Multitalented scumbag")
            {
                imperialRatio -= shames[i].amount * 10;
                heteroRatio -= shames[i].amount * 10;
                patriarcalRatio -= shames[i].amount * 10;
                justiceRatio -= shames[i].amount * 10;
            }
            else if (shames[i].name == "Fascism" || shames[i].name == "Collaboration")
            {
                imperialRatio -= shames[i].amount * 20;
                justiceRatio -= shames[i].amount * 20;
            }
        }
        womenMultiplicator = patriarcalRatio * 0.25f;
        marginalizedMultiplicator = imperialRatio * 0.25f;
        queerMultiplicator = heteroRatio * 0.25f;
        // Calculate Effects
        feminists = 0;
        antiracists = 0;
        activists = 0;
        for (int i = 0; i < artistDomainActive.Count; i++)
        {
            for (int y = 0; y < artistDomainActive[i].Artist.Count; y++)
            {
                for (int z = 0; z < artistDomainActive[i].Artist[y].effectRefs.Count; z++)
                {
                    Effect newEffect = effectList.Effect[artistDomainActive[i].Artist[y].effectRefs[z]];
                    if (newEffect.Type == "Domain-defined current")
                    {
                        domainDefined++;
                    }
                    else if (newEffect.Type == "Activism")
                    {
                        if (newEffect.SubType == "Feminism")
                        {
                            feminists++;
                        }
                        else if (newEffect.SubType == "Antiracism")
                        {
                            antiracists++;
                        }
                        else if (newEffect.SubType == "Black Feminism")
                        {
                            feminists++;
                            antiracists++;
                        }
                        else if (newEffect.SubType == "Justice" || newEffect.SubType == "Queer")
                        {
                            activists++;
                        }
                    }
                    else if (newEffect.Type == "Title")
                    {
                        domainEffectMultiplicator[i] += 0.1f;
                    }
                }
            }
        }
        for (int i = 0; i < artistDomainActive.Count; i++)
        {
            for (int y = 0; y < artistDomainActive[i].Artist.Count; y++)
            {
                float effectMultiplicator = domainEffectMultiplicator[i];
                if (artistDomainActive[i].Artist[y].Lead == 1)
                {
                    effectMultiplicator += (artistDomainActive[i].Artist.Count) / 8;
                }
                else if (artistDomainActive[i].Artist[y].Lead == 2)
                {
                    effectMultiplicator += available / 40;
                }
                // Checking for "Features" Multiplicators
                for (int x = 0; x < artistDomainActive[i].Artist[y].effectRefs.Count; x++)
                {
                    string newEffect = effectList.Effect[artistDomainActive[i].Artist[y].effectRefs[x]].Type;
                    if (newEffect == "Feature")
                    {
                        effectMultiplicator *= 1.5f;
                    }
                }
                // Checking for other effects
                for (int z = 0; z < artistDomainActive[i].Artist[y].effectRefs.Count; z++)
                {
                    Effect newEffect = effectList.Effect[artistDomainActive[i].Artist[y].effectRefs[z]];
                    if (newEffect.Type == "Activism")
                    {
                        if (newEffect.SubType == "Feminism")
                        {
                            patriarcalRatio += (3 + (feminists * 0.03f)) * effectMultiplicator;
                        }
                        else if (newEffect.SubType == "Antiracism")
                        {
                            imperialRatio += (3 + (antiracists * 0.05f)) * effectMultiplicator;
                        }
                        else if (newEffect.SubType == "Black Feminism")
                        {
                            patriarcalRatio += (1.5f + (feminists * 0.03f)) * effectMultiplicator;
                            imperialRatio += (1.5f + (antiracists * 0.04f)) * effectMultiplicator;
                        }
                        else if (newEffect.SubType == "Justice")
                        {
                            justiceRatio += (3 + (activists * 0.03f));
                        }
                        else if (newEffect.SubType == "Queer")
                        {
                            heteroRatio += (5 + ((activists + feminists) * 0.01f)) * effectMultiplicator;
                        }
                    }
                    else if (newEffect.Type == "Contribution")
                    {
                        if (newEffect.SubType == "Global")
                        {
                            globalPriceModifier *= 0.95f;
                        }
                        else
                        {
                            priceModifiers[i] *= 0.7f;
                        }
                    }
                    else if (newEffect.Type == "Interdisciplinary current")
                    {
                        for (int w = 0; w < currents.Count; w++)
                        {
                            if (newEffect.Name == currents[w].name)
                            {
                                float amount = 4 + (currents[w].amount * 4) / (1 + (currents[w].total * amountVersusTotal));
                                currents[w].multiplicator += amount * effectMultiplicator;
                            }
                        }
                    }
                    else if (newEffect.Type == "Genre")
                    {
                        for (int w = 0; w < artistDomainActive.Count; w++)
                        {
                            if (newEffect.Domain[0] == artistDomainActive[w].name)
                            {
                                float amount = 5 + (artistDomainActive[w].Artist.Count * 5) / (1 + (50 * amountVersusTotal));
                                domainMultiplicators[w] += amount * effectMultiplicator;
                            }
                        }
                    }
                    else if (newEffect.Type == "Achievement")
                    {
                        globalMultiplicatorEffect += ((artistDomainActive[i].Artist.Count * 0.1f) * effectMultiplicator);
                    }
                    else if (newEffect.Type == "Prize")
                    {
                        globalMultiplicatorEffect += ((nationCount * 0.05f) * effectMultiplicator);
                    }
                    else if (newEffect.Type == "Domain-defined current")
                    {
                        for (int w = 0; w < artistDomainActive.Count; w++)
                        {
                            if (newEffect.Domain[0] == artistDomainActive[w].name)
                            {
                                domainMultiplicators[w] += 5 + (domainDefined * 0.07f) * effectMultiplicator;
                            }
                        }
                    }
                    else if (newEffect.Type == "Activity")
                    {
                        if (newEffect.Name == "Multitalented Artist")
                        {
                            domainMultiplicators[i] += 3 + ((available * 0.015f) * effectMultiplicator);
                        }
                        else
                        {
                            List<int> refs = new List<int>();
                            for (int w = 0; w < newEffect.Domain.Count; w++)
                            {
                                for (int v = 0; v < artistDomainActive.Count; v++)
                                {
                                    if (artistDomainActive[v].name == newEffect.Domain[w])
                                    {
                                        refs.Add(v);
                                    }
                                }
                            }
                            if (refs.Count == 3)
                            {
                                domainMultiplicators[refs[0]] += 3 + ((artistDomainActive[refs[1]].Artist.Count + artistDomainActive[refs[2]].Artist.Count) * 0.1f) * effectMultiplicator;
                            }
                            else
                            {
                                domainMultiplicators[refs[0]] += 3 + (artistDomainActive[refs[1]].Artist.Count * 0.15f) * effectMultiplicator;
                            }
                        }
                    }
                    else if (newEffect.Type == "Area-defined current")
                    {
                        for (int w = 0; w < areas.Count; w++)
                        {
                            if (areas[w].name == newEffect.Area)
                            {
                                areas[w].multiplicator += 5 + (artistDomainActive[i].Artist.Count * 0.1f) * effectMultiplicator;
                            }
                        }
                    }
                    else if (newEffect.Type == "Group")
                    {
                        globalMultiplicatorEffectiveness += (0.1f * (0.5f + (effectMultiplicator * 0.5f)));
                    }
                    else if (newEffect.Type == "Discovery")
                    {
                        if (newEffect.Area != null)
                        {
                            for (int w = 0; w < areas.Count; w++)
                            {
                                if (newEffect.Area == areas[w].name)
                                {
                                    domainMultiplicators[i] += 5 + ((areas[w].amount * 3) / (1 + (areas[w].total * amountVersusTotal)) * effectMultiplicator);
                                }
                            }
                        }
                        else
                        {
                            for (int w = 0; w < currents.Count; w++)
                            {
                                if (newEffect.SubType == currents[w].name)
                                {
                                    domainMultiplicators[i] += 5 + ((currents[w].amount * 3) / (1 + (currents[w].total * amountVersusTotal)) * effectMultiplicator);
                                }
                            }
                        }
                    }
                }
            }
        }
        // Global Multiplicator
        globalMultiplicator = 5 + (patriarcalRatio + imperialRatio + justiceRatio) * 0.25f;
        gaugeSize = (Mathf.Abs(globalMultiplicator) * 0.00775f);
        if (gaugeSize != gauge.localScale.y)
        {
            gaugeTime = 0;
        }
        // News
        if (newsChange)
        {
            save.newsEra = newsEra;
            newsPlay = 0;
            newsListAvailable.News.Clear();
            for (int i = 0; i < newsList.News.Count; i++)
            {
                if (newsList.News[i].Era == newsEra)
                {
                    if (newsList.News[i].Type == "")
                    {
                        newsListAvailable.News.Add(newsList.News[i]);
                    }
                    else if (newsList.News[i].Type == "BadGauge")
                    {
                        if (globalMultiplicator < 0)
                        {
                            newsListAvailable.News.Add(newsList.News[i]);
                        }
                    }
                    else
                    {
                        if (globalMultiplicator > 0)
                        {
                            newsListAvailable.News.Add(newsList.News[i]);
                        }
                    }
                }
            }
            newsRef = 0;
            newsChange = false;
        }
        // Production
        int gaugeRatio = 0;
        float gaugeEffect = 1;
        if (globalMultiplicator < 0)
        {
            gaugeRatio = (int)Math.Floor(globalMultiplicator / 10);
            gaugeEffect = 1 + (Mathf.Abs(gaugeRatio * globalMultiplicatorEffectiveness) * 0.5f);
        }
        else if (globalMultiplicator > 0)
        {
            gaugeRatio = (int)Math.Ceiling(globalMultiplicator / 10);
            gaugeEffect = 1 + Mathf.Abs(gaugeRatio * globalMultiplicatorEffectiveness);
        }
        if (gameOn)
        {
            if (gaugeRatio > currentGauge)
            {
                StartCoroutine(GaugeChange(true));
            }
            else if (gaugeRatio < currentGauge)
            {
                StartCoroutine(GaugeChange(false));
            }
        }
        currentGauge = gaugeRatio;
        for (int i = 0; i < artistDomainActive.Count; i++)
        {
            // For each artist
            for (int y = 0; y < artistDomainActive[i].Artist.Count; y++)
            {
                //globalMultiplicatorEffectiveness -= (0.0004f);
                // Check Multiplicators
                bool marginalizedArtist = false;
                float multi = 1;
                multi += domainMultiplicators[i];
                for (int x = 0; x < artistDomainActive[i].Artist[y].currentRefs.Count; x++)
                {
                    multi += currents[artistDomainActive[i].Artist[y].currentRefs[x]].multiplicator;
                }
                for (int x = 0; x < artistDomainActive[i].Artist[y].tagRefs.Count; x++)
                {
                    multi += tags[artistDomainActive[i].Artist[y].tagRefs[x]].multiplicator;
                    if (tags[artistDomainActive[i].Artist[y].tagRefs[x]].name == "Racialized" ||
                        tags[artistDomainActive[i].Artist[y].tagRefs[x]].name == "Native")
                    {
                        marginalizedArtist = true;
                    }
                    else if (tags[artistDomainActive[i].Artist[y].tagRefs[x]].name == "Queer")
                    {
                        multi += queerMultiplicator;
                    }
                }
                for (int x = 0; x < artistDomainActive[i].Artist[y].Nation.Count; x++)
                {
                    if (!marginalizedArtist)
                    {
                        if (areas[nations[artistDomainActive[i].Artist[y].nationRefs[x]].area].name != "North America" &&
                            areas[nations[artistDomainActive[i].Artist[y].nationRefs[x]].area].name != "Eastern Europe" &&
                            areas[nations[artistDomainActive[i].Artist[y].nationRefs[x]].area].name != "Western Europe" &&
                            areas[nations[artistDomainActive[i].Artist[y].nationRefs[x]].area].name != "Eastern Asia")
                        {
                            marginalizedArtist = true;
                        }
                    }
                }
                if (marginalizedArtist)
                {
                    multi += marginalizedMultiplicator;
                }
                else if (marginalizedMultiplicator < 0)
                {
                    multi -= marginalizedMultiplicator * 0.5f;
                }
                if (artistDomainActive[i].Artist[y].Gender.Count > 1)
                {
                }
                else if (artistDomainActive[i].Artist[y].Gender[0] == "Female")
                {
                    multi += womenMultiplicator;
                }
                else if (womenMultiplicator < 0)
                {
                    multi -= womenMultiplicator * 0.5f;
                }
                multi += globalMultiplicatorEffect;
                // Multiply
                float personal;
                if (multi < 0)
                {
                    personal = 10 * ((i + 1) * 0.5f) / ((1 + multi * multEffectiveness) * 0.5f);
                }
                else
                {
                    personal = 10 * ((i + 1) * 0.5f) * ((1 + multi * multEffectiveness));
                }
                if (globalMultiplicator > 0)
                {
                    artistDomainActive[i].Artist[y].Production = personal * gaugeEffect * eraMultiplicator;
                }
                else if (globalMultiplicator < 0)
                {
                    artistDomainActive[i].Artist[y].Production = (personal / gaugeEffect) * eraMultiplicator;
                }
                else if (globalMultiplicator == 0)
                {
                    artistDomainActive[i].Artist[y].Production = personal * eraMultiplicator;
                }
                domainProduction[i] += artistDomainActive[i].Artist[y].Production;
                production += artistDomainActive[i].Artist[y].Production;
                if (artistDomainActive[i].Artist[y].Production > 1000000)
                {
                    artistDomainActive[i].Artist[y].Prod.text = "Output: " + Math.Round(artistDomainActive[i].Artist[y].Production * 0.000001f, 1) + "<size=4> Million ames/sec</size>";
                }
                else if (artistDomainActive[i].Artist[y].Production > 1000)
                {
                    artistDomainActive[i].Artist[y].Prod.text = "Output: " + Math.Round(artistDomainActive[i].Artist[y].Production * 0.001f, 1) + "<size=4> Thousand ames/sec</size>";
                }
                else
                {
                    artistDomainActive[i].Artist[y].Prod.text = "Output: " + Math.Round(artistDomainActive[i].Artist[y].Production, 1) + "ames/s";
                }
            }
        }
        //Stats Display
        if (production > 1000000000)
        {
            productionDisplay.text = Math.Round(production * 0.000000001f, 2) + "<size=4> Billion ames/sec</size>";
        }
        else if (production > 1000000)
        {
            productionDisplay.text = Math.Round(production * 0.000001f, 2) + "<size=4> Million ames/sec</size>";
        }
        else if (production > 1000)
        {
            productionDisplay.text = Math.Round(production * 0.001f, 2) + "<size=4> Thousand ames/sec</size>";
        }
        else
        {
            productionDisplay.text = Math.Round(production, 2) + " <size=4> ames/sec</size>";
        }
        statisticTexts[0].text = "<b>Artists:</b> " + available;
        statisticTexts[2].text = "<b>Eras:</b> " + erasPassed + "%";
        statisticTexts[3].text = "<b>Gauge:</b> " + Math.Round(globalMultiplicator, 1);
        statisticTexts[4].text = "<b>Multiplied by:</b> " + Math.Round(globalMultiplicatorEffectiveness * 2, 1);
        statisticTexts[5].text = "<b>Nations:</b> " + nationCount;
        statisticTexts[6].text = "<b>Women:</b> " + women;
        statisticTexts[7].text = "<b>Men:</b> " + men;
        statisticTexts[8].text = "<b>Activists:</b> " + (activists + feminists + antiracists);
        statisticTexts[9].text = "<b>Society Age:</b> " + (currentSociety + 1);
        if (production > 0)
        {
            if (production / available > 1000000000)
            {
                statisticTexts[1].text = "<b>Average:</b> " + Math.Round(production / available * 0.000000001f, 1) + "B";
            }
            else if (production / available > 1000000)
            {
                statisticTexts[1].text = "<b>Average:</b> " + Math.Round(production / available * 0.000001f, 1) + "M";
            }
            else if (production / available > 1000)
            {
                statisticTexts[1].text = "<b>Average:</b> " + Math.Round(production / available * 0.001f, 1) + "K";
            }
            else
            {
                statisticTexts[1].text = "<b>Average:</b> " + Math.Round(production / available, 1);
            }
        }
        else { statisticTexts[1].text = "<b>Average:</b> 0"; }
        SaveGame();
    }
    IEnumerator Beginning()
    {
        secondDrawOn = false;
        transBackground.gameObject.SetActive(true);
        transPanel.gameObject.SetActive(true);
        transBackground.alpha = 1;
        //Display Text
        click = true;
        float currentTime = 0;
        while (currentTime <= 4 && click)
        {
            currentTime += Time.deltaTime;
            transPanel.alpha = currentTime * 0.25f;
            yield return null;
        }
        transPanel.alpha = 1;
        currentTime = 0;
        while (currentTime <= 1 && click)
        {
            currentTime += Time.deltaTime;
            transClick.alpha = currentTime;
            yield return null;
        }
        transClick.alpha = 1;
        while (click)
        {
            yield return null;
        }
        currentTime = 0;
        while (currentTime <= 1)
        {
            currentTime += Time.deltaTime;
            transClick.alpha = 1 - currentTime;
            transParagraph.alpha = 1 - currentTime;
            yield return null;
        }
        transParagraph.alpha = 0;
        transClick.alpha = 0;
        transText.alignment = TextAlignmentOptions.Center;
        transText.text = "<color=#E2E9AF><size=7><b>Currents:</b></size></color>\n\nYou may select one of these Painters.\n\nEach one unlocks a different Current.\n\nThe other two will be available later.";
        currentTime = 0;
        while (currentTime <= 1)
        {
            currentTime += Time.deltaTime;
            transParagraph.alpha = currentTime;
            yield return null;
        }
        transParagraph.alpha = 1;
        drawing = true;
        // Change Era
        domainShops[0].era = 1;
        save.eraList[0] = 1;
        domainShops[0].availableCards += 10;
        erasPassed++;
        statisticTexts[2].text = "<b>Eras:</b> " + erasPassed + "%";
        for (int i = 0; i < artistDomainLocked[0].Artist.Count; i++)
        {
            if (artistDomainLocked[0].Artist[i].Era == domainShops[0].era)
            {
                artistDomainAvailable[0].Artist.Add(artistDomainLocked[0].Artist[i]);
                artistDomainLocked[0].Artist.Remove(artistDomainLocked[0].Artist[i]);
                i--;
            }
        }
        scrollTexts[1].text = artistDomainAvailable[0].name;
        scrollTexts[1].color = domainColors[0];
        // Draw Cards
        Artist[] firstDraw = new Artist[3];
        firstCards = new Transform[3];
        for (int i = 0; i < firstDraw.Length; i++)
        {
            firstDraw[i] = artistDomainAvailable[0].Artist[i];
            firstCards[i] = GameObject.Instantiate(card, transBackground.transform).transform;
            firstCards[i].localScale = new Vector2(0.8f, 0.8f);
            TextMeshProUGUI cardText = firstCards[i].Find("Name").GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI cardTags = firstCards[i].Find("Tags").GetComponent<TextMeshProUGUI>();
            Transform effectSort = firstCards[i].Find("EffectSort");
            Transform nationSort = firstCards[i].Find("NationSort");
            Transform genderSort = firstCards[i].Find("GenderSort");
            Artist currentArtist = firstDraw[i];
            TextMeshProUGUI cardDesc = firstCards[i].Find("Text").GetComponent<TextMeshProUGUI>();
            cardDesc.text = currentArtist.Text;
            CardSelection script = firstCards[i].GetComponent<CardSelection>();
            script.domain = 0;
            script.outOfDomain = false;
            TextMeshProUGUI cardCurrents = firstCards[i].Find("Currents").GetComponent<TextMeshProUGUI>();
            script.artist = currentArtist;
            script.price = 0;
            // Read Card Name
            if (currentArtist.Name2 == null)
            {
                if (currentArtist.Death != 9999 && currentArtist.Birth != 9999)
                {
                    cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + ")";
                }
                else if (currentArtist.Birth == 9999)
                {
                    cardText.text = currentArtist.Name + " (???)";
                }
                else
                {
                    cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "- )";
                }
            }
            else if (currentArtist.Name3 == null && currentArtist.CommonName == null)
            {
                if (currentArtist.Death != 9999 && currentArtist.Death2 != 9999)
                {
                    cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + ") &\n"
                    + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "-" + currentArtist.Death2.ToString() + ")";
                }
                else if (currentArtist.Death != 9999)
                {
                    cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + ") &\n"
                    + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "- )";
                }
                else if (currentArtist.Death2 != 9999)
                {
                    cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "- ) &\n"
                    + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "-" + currentArtist.Death2.ToString() + ")";
                }
                else
                {
                    cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "- ) &\n"
                    + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "- )";
                }
            }
            else if (currentArtist.Name3 == null)
            {
                if (currentArtist.Death != 9999 && currentArtist.Death2 != 9999)
                {
                    cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + ") &\n"
                    + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "-" + currentArtist.Death2.ToString() + ") " + currentArtist.CommonName;
                }
                else if (currentArtist.Death != 9999)
                {
                    cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + ") &\n"
                    + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "- ) " + currentArtist.CommonName;
                }
                else if (currentArtist.Death2 != 9999)
                {
                    cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "- ) &\n"
                    + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "-" + currentArtist.Death2.ToString() + ") " + currentArtist.CommonName;
                }
                else
                {
                    cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "- ) &\n"
                    + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "- ) " + currentArtist.CommonName;
                }
            }
            else
            {
                cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + "), "
                    + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "-\n" + currentArtist.Death2.ToString() + ") & "
                    + currentArtist.Name3 + " (" + currentArtist.Birth2.ToString() + "-" + currentArtist.Death3.ToString() + ") " + currentArtist.CommonName;
            }
            // Read Card Informations
            for (int y = 0; y < currentArtist.nationRefs.Count; y++)
            {
                GameObject newNation = GameObject.Instantiate(nation, nationSort);
                if (currentArtist.nationRefs[y] == 8 && currentArtist.Birth > 1900 && currentArtist.Birth < 1980)
                {
                    newNation.GetComponent<Image>().sprite = flags[118];
                }
                else
                {
                    newNation.GetComponent<Image>().sprite = flags[currentArtist.nationRefs[y]];
                }
                NationMouse nm = newNation.GetComponent<NationMouse>();
                nm.active = true;
                nm.nation = currentArtist.Nation[y];
                nm.pos = firstCards[i];
            }
            for (int y = 0; y < currentArtist.Gender.Count; y++)
            {
                GameObject newGender = GameObject.Instantiate(nation, genderSort);
                newGender.transform.localScale = new Vector2(0.8f, 0.8f);
                if (currentArtist.Gender[y] == "Female")
                {
                    newGender.GetComponent<Image>().sprite = genderF;
                }
                else if (currentArtist.Gender[y] == "Male")
                {
                    newGender.GetComponent<Image>().sprite = genderM;
                }
                else
                {
                    newGender.GetComponent<Image>().sprite = genderNB;
                }
            }
            for (int y = 0; y < currentArtist.Effects.Count; y++)
            {
                Transform newEffect = GameObject.Instantiate(effect, effectSort).transform;
                TextMeshProUGUI effectTitle = newEffect.Find("Effect Name").GetComponent<TextMeshProUGUI>();
                TextMeshProUGUI effectDescription = newEffect.Find("Effect").GetComponent<TextMeshProUGUI>();
                EffectClick click = newEffect.GetComponent<EffectClick>();
                click.artist = currentArtist;
                click.effect = y;
                effectTitle.text = currentArtist.Effects[y];
                Effect effectRef = effectList.Effect[currentArtist.effectRefs[y]];
                if (effectRef.Type == "Activism")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[0];
                    if (effectRef.SubType == "Feminism")
                    {
                        effectDescription.text = "Heals the Gauge and boosts women Artists";
                    }
                    else if (effectRef.SubType == "Antiracism")
                    {
                        effectDescription.text = "Heals the Gauge and boosts marginalized Artists";
                    }
                    else if (effectRef.SubType == "Queer")
                    {
                        effectDescription.text = "Heals the Gauge and boosts queer Artists";
                    }
                    else
                    {
                        effectDescription.text = "Heals the Gauge";
                    }
                }
                else if (effectRef.Type == "Contribution")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[1];
                    if (effectRef.SubType == "Global")
                    {
                        effectDescription.text = "Slightly reduces the price of all Cards";
                    }
                    else
                    {
                        effectDescription.text = "Reduces the price of " + domainShortened[0] + " Cards";
                    }
                }
                else if (effectRef.Type == "Feature")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[2];
                    effectDescription.text = "Strengthens this Artist's other Effects";
                }
                else if (effectRef.Type == "Interdisciplinary current")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[3];
                    effectDescription.text = "Boosts each Artist linked to " + effectRef.Name;
                }
                else if (effectRef.Type == "Area-defined current")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[4];
                    effectDescription.text = "Boosts " + effectRef.Area + " for each Artist in " + domainShortened[0];
                }
                else if (effectRef.Type == "Domain-defined current")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[5];
                    effectDescription.text = "Boosts " + domainShortened[0] + " for each Artist within a minor current";
                }
                else if (effectRef.Type == "Group")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[6];
                    effectDescription.text = "Strengthens the Gauge's influence";
                }
                else if (effectRef.Type == "Title")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[7];
                    effectDescription.text = "Strengthens all Effects within " + domainShortened[0];
                }
                else if (effectRef.Type == "Prize")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[8];
                    effectDescription.text = "Slightly boosts everyone for each represented country";
                }
                else if (effectRef.Type == "Discovery")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[9];
                    if (effectRef.Area != "")
                    {
                        effectDescription.text = "Boosts " + domainShortened[0] + " for each Artist in " + effectRef.Area;
                    }
                    else
                    {
                        effectDescription.text = "Boosts " + domainShortened[0] + " for each Artist in " + effectRef.SubType;
                    }
                }
                else if (effectRef.Type == "Genre")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[10];
                    effectDescription.text = "Boosts " + domainShortened[0] + " for each of its Artists";
                }
                else if (effectRef.Type == "Activity")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[11];
                    if (effectRef.Name == "Multitalented Artist")
                    {
                        effectDescription.text = "Slightly boosts " + domainShortened[0] + " for each Artist";
                    }
                    else if (effectRef.Name == "Multitalented Writer")
                    {
                        effectDescription.text = "Slightly boosts " + domainShortened[0] + " for each poet, novelist or playwright";
                    }
                    else if (effectList.Effect[currentArtist.effectRefs[y]].Domain.Count > 2)
                    {
                        int domainReference = 0;
                        for (int z = 0; z < artistDomainActive.Count; z++)
                        {
                            if (effectRef.Domain[0] == artistDomainActive[z].name)
                            {
                                domainReference = z;
                            }
                        }
                        effectDescription.text = "Boosts " + domainShortened[domainReference] + " for each Artist in these two Domains";
                    }
                    else
                    {
                        int domainReference = 0;
                        for (int z = 0; z < artistDomainActive.Count; z++)
                        {
                            if (effectRef.Domain[0] == artistDomainActive[z].name)
                            {
                                domainReference = z;
                            }
                        }
                        int domainReference2 = 0;
                        for (int z = 0; z < artistDomainActive.Count; z++)
                        {
                            if (effectRef.Domain[1] == artistDomainActive[z].name)
                            {
                                domainReference2 = z;
                            }
                        }
                        if (domainReference == domainReference2) { effectDescription.text = "Boosts " + domainShortened[domainReference] + " for each of its Artists"; }
                        else { effectDescription.text = "Boosts " + domainShortened[domainReference] + " for each Artist in " + domainShortened[domainReference2]; }
                    }
                }
                else if (effectRef.Type == "Achievement")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[12];
                    effectDescription.text = "Slightly boosts everyone for each Artist in " + domainShortened[0];
                }
            }
            if (currentArtist.Tags[0] == "None")
            {
                cardTags.text = " ";
            }
            else if (currentArtist.Tags.Count == 1)
            {
                cardTags.text = currentArtist.Tags[0];
            }
            else if (currentArtist.Tags.Count == 2)
            {
                cardTags.text = currentArtist.Tags[0] + ", " + currentArtist.Tags[1];
            }
            else
            {
                cardTags.text = currentArtist.Tags[0] + ", " + currentArtist.Tags[1] + ", " + currentArtist.Tags[2];
            }
            if (currentArtist.Currents[0] == "None")
            {
                cardCurrents.text = " ";
            }
            else if (currentArtist.Currents.Count == 1)
            {
                cardCurrents.text = currentArtist.Currents[0];
            }
            else if (currentArtist.Currents.Count == 2)
            {
                cardCurrents.text = currentArtist.Currents[0] +
                    ", " + currentArtist.Currents[1];
            }
            else if (currentArtist.Currents.Count == 3)
            {
                cardCurrents.text = currentArtist.Currents[0] +
                    ", " + currentArtist.Currents[1] +
                    ", " + currentArtist.Currents[2];
            }
            else if (currentArtist.Currents.Count == 4)
            {
                cardCurrents.text = currentArtist.Currents[0] +
                    ", " + currentArtist.Currents[1] +
                    ", " + currentArtist.Currents[2] +
                    ", " + currentArtist.Currents[3];
            }
            else if (currentArtist.Currents.Count == 5)
            {
                cardCurrents.text = currentArtist.Currents[0] +
                    ", " + currentArtist.Currents[1] +
                    ", " + currentArtist.Currents[2] +
                    ", " + currentArtist.Currents[3] +
                    ", " + currentArtist.Currents[4];
            }
            currentArtist.Locked = false;
            CardSelection detector = firstCards[i].GetComponent<CardSelection>();
            detector.unlockText = GameObject.Instantiate(unlockText, firstCards[i]);
            if (firstDraw[i].Name == "Katsushika Hokusai\n")
            {
                detector.unlockText.GetComponent<TextMeshProUGUI>().text = "Unlocks Modern\nOrnament Bundle";
            }
            else if (firstDraw[i].Name == "Adélaïde Labille-\nGuiard")
            {
                detector.unlockText.GetComponent<TextMeshProUGUI>().text = "Unlocks\nNeoclassicism";
            }
            else if (firstDraw[i].Name == "Francisco Goya")
            {
                detector.unlockText.GetComponent<TextMeshProUGUI>().text = "Unlocks\nRomanticism";
            }

        }
        // Display Cards
        if (audioOn)
        {
            sound2.clip = clips[6];
            sound2.volume = volume * 0.4f;
            sound2.Play();
        }
        currentTime = 0;
        while (currentTime <= 0.25f)
        {
            currentTime += Time.deltaTime;
            for (int i = 0; i < firstDraw.Length; i++)
            {
                firstCards[i].localPosition = new Vector2(firstCards[i].localPosition.x, Mathf.Lerp(40, -81, currentTime * 4));
            }
            yield return null;
        }
        drawing = false;
        if (audioOn)
        {
            sound2.clip = clips[2];
            sound2.volume = volume;
            sound2.Play();
        }
        currentTime = 0;
        while (currentTime <= 1f)
        {
            currentTime += Time.deltaTime;
            for (int i = 0; i < firstDraw.Length; i++)
            {
                firstCards[i].localPosition = new Vector2(Mathf.Lerp(firstCards[i].localPosition.x, -(50 * firstDraw.Length) + (150 * i), currentTime), firstCards[i].localPosition.y);
            }
            yield return null;
        }
        firstDrawOn = true;
        yield break;
    }
    public IEnumerator Beginning2(Artist artist, Transform artistCard)
    {
        // Identify Card
        cardBoxes[0].parent.GetComponent<BoxMouse>().cardNumber++;
        save.artistDomainActive[0].Add(artist.Name);
        save.isArtistLead[0].Add(artist.Lead);
        drawing = true;
        firstDrawOn = false;
        artistCard.localScale = new Vector2(1, 1);
        Artist current = artist;
        current.Card = artistCard;
        artistCard.GetComponent<CardSelection>().enabled = false;
        artistCard.GetComponent<EventTrigger>().enabled = false;
        domainShops[0].availableCards--;
        domainShops[0].cardText.text = domainShops[0].availableCards.ToString();
        domainShops[0].cardText2.text = artistDomainLocked[0].Artist.Count.ToString();
        EraPriceDisplay(domainShops[0]);
        artistDomainActive[0].Artist.Add(current);
        artistDomainAvailable[0].Artist.Remove(current);
        artistDomainActive[0].Artist.Sort((y, x) => x.Birth.CompareTo(y.Birth));
        artistDomainActive[0].Artist.Reverse();
        for (int w = 0; w < current.Nation.Count; w++)
        {
            artistCard.Find("NationSort").GetChild(w).GetComponent<NationMouse>().cardActive = true;
        }
        //Buy Currents
        cardBoxItems.parent.GetComponent<BoxMouse>().cardNumber++;
        if (current.Name == "Katsushika Hokusai\n")
        {
            int currentRef = 0;
            for (int i = 0; i < majorCurrents.Length; i++)
            {
                if (majorCurrents[i].Name == "Modern Ornament Bundle")
                {
                    currentRef = i;
                }
            }
            GameObject newItem = (GameObject)Instantiate(storeItem, storeFront);
            availableItems.Add(new Item(newItem.transform, majorCurrents[currentRef].Name, "Major Current"));
            save.availableItems.Add(majorCurrents[currentRef].Name);
            Image newItemImage = newItem.GetComponent<Image>();
            newItemImage.color = new Color(newItemImage.color.r, newItemImage.color.g, newItemImage.color.b, 1);
            newItem.GetComponent<ShopClick>().current = availableItems[availableItems.Count - 1];
            availableItems[availableItems.Count - 1].icon = currentIcons[5];
            newItem.transform.GetChild(0).GetComponent<Image>().sprite = availableItems[availableItems.Count - 1].icon;
            availableItems[availableItems.Count - 1].SubGenders = new List<string>();
            availableItems[availableItems.Count - 1].Era = majorCurrents[currentRef].Era;
            if (majorCurrents[currentRef].SubCurrents != null)
            {
                for (int y = 0; y < majorCurrents[currentRef].SubCurrents.Count; y++)
                {
                    availableItems[availableItems.Count - 1].SubGenders.Add(currents[majorCurrents[currentRef].SubCurrents[y]].name);
                }
            }
            availableItems[availableItems.Count - 1].Total = majorCurrents[currentRef].Total;
            StartCoroutine(BuyItem(availableItems[availableItems.Count - 1]));
        }
        else if (current.Name == "Adélaïde Labille-\nGuiard")
        {
            for (int i = 0; i < availableItems.Count; i++)
            {
                if (availableItems[i].Name == "Neoclassicism")
                {
                    StartCoroutine(BuyItem(availableItems[i]));
                }
            }
        }
        else if (current.Name == "Francisco Goya")
        {
            for (int i = 0; i < availableItems.Count; i++)
            {
                if (availableItems[i].Name == "Romanticism")
                {
                    StartCoroutine(BuyItem(availableItems[i]));
                }
            }
        }
        // Hide Cards
        float currentTime = 0;
        while (currentTime <= 1)
        {
            currentTime += Time.deltaTime;
            for (int i = 0; i < 3; i++)
            {
                if (firstCards[i] == artistCard)
                {
                    firstCards[i].localPosition = new Vector2(artistCard.localPosition.x, Mathf.Lerp(artistCard.localPosition.y, -350, currentTime));
                }
                else
                {
                    firstCards[i].localPosition = new Vector2(firstCards[i].localPosition.x, Mathf.Lerp(firstCards[i].localPosition.y, 250, currentTime));
                }
            }
            yield return null;
        }
        // Set Parents
        artistCard.SetParent(cardBoxes[0]);
        current.Prod = artistCard.Find("Production").GetComponent<TextMeshProUGUI>();
        // Update Stats
        available++;
        for (int i = 0; i < current.nationRefs.Count; i++)
        {
            nations[current.nationRefs[i]].amount++;
        }
        for (int i = 0; i < current.tagRefs.Count; i++)
        {
            tags[current.tagRefs[i]].amount++;
        }
        for (int i = 0; i < current.currentRefs.Count; i++)
        {
            currents[current.currentRefs[i]].amount++;
        }
        if (current.Gender.Count > 1)
        {
            men++;
            women++;
        }
        else if (current.Gender[0] == "Female")
        {
            women++;
        }
        else
        {
            men++;
        }
        Calculation();
        // Set Cards
        Vector2 destination = new Vector2(0, 460);
        artistCard.localPosition = destination;
        for (int i = 0; i < firstCards.Length; i++)
        {
            if (firstCards[i] != artistCard)
            {
                GameObject.Destroy(firstCards[i].gameObject);
            }
        }
        GameObject detectorObject = artistCard.Find("MouseDetector").gameObject;
        detectorObject.SetActive(true);
        MouseDetector detector = detectorObject.GetComponent<MouseDetector>();
        detector.nextCard = artistCard.Find("NextCard").transform;
        detector.currentCard = artistCard;
        detector.box = cardBoxes[0].parent.GetComponent<BoxMouse>();
        detector.cardColor = domainColors[0];
        detector.domain = 0;
        detector.artist = current;
        if (current.Name2 == null)
        {
            detector.artistName = current.Name;
        }
        else if (current.Name3 == null && current.CommonName == null)
        {
            detector.artistName = current.Name + " & " + current.Name2;
        }
        else if (current.Name3 == null)
        {
            detector.artistName = current.Name + ", " + current.Name2 + " & " + current.Name3;
        }
        else
        {
            detector.artistName = current.Name + ", " + current.Name2 + " & " + current.Name3 + current.CommonName;
        }
        detector.effectName = new string[current.Effects.Count];
        detector.effectColor = new Color[current.Effects.Count];
        for (int i = 0; i < current.Effects.Count; i++)
        {
            detector.effectName[i] = current.Effects[i];
            Effect effectRef = effectList.Effect[current.effectRefs[i]];
            if (effectRef.Type == "Activism")
            {
                detector.effectColor[i] = effectColors[0];
            }
            else if (effectRef.Type == "Contribution")
            {
                detector.effectColor[i] = effectColors[1];
            }
            else if (effectRef.Type == "Feature")
            {
                detector.effectColor[i] = effectColors[2];
            }
            else if (effectRef.Type == "Interdisciplinary current")
            {
                detector.effectColor[i] = effectColors[3];
            }
            else if (effectRef.Type == "Area-defined current")
            {
                detector.effectColor[i] = effectColors[4];
            }
            else if (effectRef.Type == "Domain-defined current")
            {
                detector.effectColor[i] = effectColors[5];
            }
            else if (effectRef.Type == "Group")
            {
                detector.effectColor[i] = effectColors[6];
            }
            else if (effectRef.Type == "Title")
            {
                detector.effectColor[i] = effectColors[7];
            }
            else if (effectRef.Type == "Prize")
            {
                detector.effectColor[i] = effectColors[8];
            }
            else if (effectRef.Type == "Discovery")
            {
                detector.effectColor[i] = effectColors[9];
            }
            else if (effectRef.Type == "Genre")
            {
                detector.effectColor[i] = effectColors[10];
            }
            else if (effectRef.Type == "Activity")
            {
                detector.effectColor[i] = effectColors[11];
            }
            else if (effectRef.Type == "Achievement")
            {
                detector.effectColor[i] = effectColors[12];
            }
        }
        //Change Text
        currentTime = 0;
        while (currentTime <= 1)
        {
            currentTime += Time.deltaTime;
            transParagraph.alpha = 1 - currentTime;
            yield return null;
        }
        transParagraph.alpha = 0;
        transText.text = "<color=#E2E9AF><size=7><b>Domains:</b></size></color>\n\nYou may select two of these Artists.\n\nEach one unlocks a different Domain.\n\nThe other two will be available later.";
        currentTime = 0;
        while (currentTime <= 1)
        {
            currentTime += Time.deltaTime;
            transParagraph.alpha = currentTime;
            yield return null;
        }
        transParagraph.alpha = 1;
        // Draw Cards
        Artist[] secondDraw = new Artist[4];
        borders = new GameObject[4];
        secondDraw[0] = artistDomainLocked[1].Artist[0];
        secondDraw[1] = artistDomainLocked[2].Artist[0];
        secondDraw[2] = artistDomainLocked[3].Artist[0];
        secondDraw[3] = artistDomainLocked[5].Artist[0];
        secondCards = new Transform[4];
        for (int i = 0; i < secondCards.Length; i++)
        {
            borders[i] = GameObject.Instantiate(border, transBackground.transform);
            secondCards[i] = GameObject.Instantiate(card, transBackground.transform).transform;
            secondCards[i].localScale = new Vector2(0.8f, 0.8f);
            TextMeshProUGUI cardText = secondCards[i].Find("Name").GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI cardTags = secondCards[i].Find("Tags").GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI cardDesc = secondCards[i].Find("Text").GetComponent<TextMeshProUGUI>();
            Transform effectSort = secondCards[i].Find("EffectSort");
            Artist currentArtist = secondDraw[i];
            cardDesc.text = currentArtist.Text;
            currentArtist.Card = secondCards[i];
            CardSelection script = secondCards[i].GetComponent<CardSelection>();
            int domain = 0;
            for (int y = 0; y < artistDomainAvailable.Count; y++)
            {
                if (secondDraw[i].Domain == artistDomainAvailable[y].name)
                {
                    domain = y;
                }
            }
            script.domain = domain;
            script.outOfDomain = false;
            script.border = borders[i];
            TextMeshProUGUI cardCurrents = secondCards[i].Find("Currents").GetComponent<TextMeshProUGUI>();
            script.artist = currentArtist;
            script.price = 0;
            // Read Card Name
            if (currentArtist.Name2 == null)
            {
                if (currentArtist.Death != 9999 && currentArtist.Birth != 9999)
                {
                    cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + ")";
                }
                else if (currentArtist.Birth == 9999)
                {
                    cardText.text = currentArtist.Name + " (???)";
                }
                else
                {
                    cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "- )";
                }
            }
            else if (currentArtist.Name3 == null && currentArtist.CommonName == null)
            {
                if (currentArtist.Death != 9999 && currentArtist.Death2 != 9999)
                {
                    cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + ") &\n"
                    + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "-" + currentArtist.Death2.ToString() + ")";
                }
                else if (currentArtist.Death != 9999)
                {
                    cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + ") &\n"
                    + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "- )";
                }
                else if (currentArtist.Death2 != 9999)
                {
                    cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "- ) &\n"
                    + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "-" + currentArtist.Death2.ToString() + ")";
                }
                else
                {
                    cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "- ) &\n"
                    + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "- )";
                }
            }
            else if (currentArtist.Name3 == null)
            {
                if (currentArtist.Death != 9999 && currentArtist.Death2 != 9999)
                {
                    cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + ") &\n"
                    + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "-" + currentArtist.Death2.ToString() + ") " + currentArtist.CommonName;
                }
                else if (currentArtist.Death != 9999)
                {
                    cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + ") &\n"
                    + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "- ) " + currentArtist.CommonName;
                }
                else if (currentArtist.Death2 != 9999)
                {
                    cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "- ) &\n"
                    + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "-" + currentArtist.Death2.ToString() + ") " + currentArtist.CommonName;
                }
                else
                {
                    cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "- ) &\n"
                    + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "- ) " + currentArtist.CommonName;
                }
            }
            else
            {
                cardText.text = currentArtist.Name + " (" + currentArtist.Birth.ToString() + "-" + currentArtist.Death.ToString() + "), "
                    + currentArtist.Name2 + " (" + currentArtist.Birth2.ToString() + "-\n" + currentArtist.Death2.ToString() + ") & "
                    + currentArtist.Name3 + " (" + currentArtist.Birth2.ToString() + "-" + currentArtist.Death3.ToString() + ") " + currentArtist.CommonName;
            }
            Transform nationSort = secondCards[i].Find("NationSort");
            Transform genderSort = secondCards[i].Find("GenderSort");
            for (int y = 0; y < currentArtist.nationRefs.Count; y++)
            {
                GameObject newNation = GameObject.Instantiate(nation, nationSort);
                if (currentArtist.nationRefs[y] == 8 && currentArtist.Birth > 1900 && currentArtist.Birth < 1980)
                {
                    newNation.GetComponent<Image>().sprite = flags[118];
                }
                else
                {
                    newNation.GetComponent<Image>().sprite = flags[currentArtist.nationRefs[y]];
                }
                NationMouse nm = newNation.GetComponent<NationMouse>();
                nm.active = true;
                nm.nation = currentArtist.Nation[y];
                nm.pos = secondCards[i];
            }
            for (int y = 0; y < currentArtist.Gender.Count; y++)
            {
                GameObject newGender = GameObject.Instantiate(nation, genderSort);
                newGender.transform.localScale = new Vector2(0.8f, 0.8f);
                if (currentArtist.Gender[y] == "Female")
                {
                    newGender.GetComponent<Image>().sprite = genderF;
                }
                else if (currentArtist.Gender[y] == "Male")
                {
                    newGender.GetComponent<Image>().sprite = genderM;
                }
                else
                {
                    newGender.GetComponent<Image>().sprite = genderNB;
                }
            }
            // Read Card Informations
            for (int y = 0; y < currentArtist.Effects.Count; y++)
            {
                Transform newEffect = GameObject.Instantiate(effect, effectSort).transform;
                TextMeshProUGUI effectTitle = newEffect.Find("Effect Name").GetComponent<TextMeshProUGUI>();
                TextMeshProUGUI effectDescription = newEffect.Find("Effect").GetComponent<TextMeshProUGUI>();
                EffectClick click = newEffect.GetComponent<EffectClick>();
                click.artist = currentArtist;
                click.effect = y;
                effectTitle.text = currentArtist.Effects[y];
                Effect effectRef = effectList.Effect[currentArtist.effectRefs[y]];
                if (effectRef.Type == "Activism")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[0];
                    if (effectRef.SubType == "Feminism")
                    {
                        effectDescription.text = "Heals the Gauge and boosts women Artists";
                    }
                    else if (effectRef.SubType == "Antiracism")
                    {
                        effectDescription.text = "Heals the Gauge and boosts marginalized Artists";
                    }
                    else if (effectRef.SubType == "Queer")
                    {
                        effectDescription.text = "Heals the Gauge and boosts queer Artists";
                    }
                    else
                    {
                        effectDescription.text = "Heals the Gauge";
                    }
                }
                else if (effectRef.Type == "Contribution")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[1];
                    if (effectRef.SubType == "Global")
                    {
                        effectDescription.text = "Slightly reduces the price of all Cards";
                    }
                    else
                    {
                        effectDescription.text = "Reduces the price of " + domainShortened[domain] + " Cards";
                    }
                }
                else if (effectRef.Type == "Feature")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[2];
                    effectDescription.text = "Strengthens this Artist's other Effects";
                }
                else if (effectRef.Type == "Interdisciplinary current")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[3];
                    effectDescription.text = "Boosts each Artist linked to " + effectRef.Name;
                }
                else if (effectRef.Type == "Area-defined current")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[4];
                    effectDescription.text = "Boosts " + effectRef.Area + " for each Artist in " + domainShortened[domain];
                }
                else if (effectRef.Type == "Domain-defined current")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[5];
                    effectDescription.text = "Boosts " + domainShortened[domain] + " for each Artist within a minor current";
                }
                else if (effectRef.Type == "Group")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[6];
                    effectDescription.text = "Strengthens the Gauge's influence";
                }
                else if (effectRef.Type == "Title")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[7];
                    effectDescription.text = "Strengthens all Effects within " + domainShortened[domain];
                }
                else if (effectRef.Type == "Prize")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[8];
                    effectDescription.text = "Slightly boosts everyone for each represented country";
                }
                else if (effectRef.Type == "Discovery")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[9];
                    if (effectRef.Area != "")
                    {
                        effectDescription.text = "Boosts " + domainShortened[domain] + " for each Artist in " + effectRef.Area;
                    }
                    else
                    {
                        effectDescription.text = "Boosts " + domainShortened[domain] + " for each Artist in " + effectRef.SubType;
                    }
                }
                else if (effectRef.Type == "Genre")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[10];
                    effectDescription.text = "Boosts " + domainShortened[domain] + " for each of its Artists";
                }
                else if (effectRef.Type == "Activity")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[11];
                    if (effectRef.Name == "Multitalented Artist")
                    {
                        effectDescription.text = "Slightly boosts " + domainShortened[domain] + " for each Artist";
                    }
                    else if (effectList.Effect[currentArtist.effectRefs[y]].Domain.Count > 2)
                    {
                        int domainReference = 0;
                        for (int z = 0; z < artistDomainActive.Count; z++)
                        {
                            if (effectRef.Domain[0] == artistDomainActive[z].name)
                            {
                                domainReference = z;
                            }
                        }
                        effectDescription.text = "Boosts " + domainShortened[domainReference] + " for each Artist in these two Domains";
                    }
                    else
                    {
                        int domainReference = 0;
                        for (int z = 0; z < artistDomainActive.Count; z++)
                        {
                            if (effectRef.Domain[0] == artistDomainActive[z].name)
                            {
                                domainReference = z;
                            }
                        }
                        int domainReference2 = 0;
                        for (int z = 0; z < artistDomainActive.Count; z++)
                        {
                            if (effectRef.Domain[1] == artistDomainActive[z].name)
                            {
                                domainReference2 = z;
                            }
                        }
                        if (domainReference == domainReference2) { effectDescription.text = "Boosts " + domainShortened[domainReference] + " for each of its Artists"; }
                        else { effectDescription.text = "Boosts " + domainShortened[domainReference] + " for each Artist in " + domainShortened[domainReference2]; }
                    }
                }
                else if (effectRef.Type == "Achievement")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[12];
                    effectDescription.text = "Slightly boosts everyone for each Artist in " + domainShortened[domain];
                }
            }
            if (currentArtist.Tags[0] == "None")
            {
                cardTags.text = " ";
            }
            else if (currentArtist.Tags.Count == 1)
            {
                cardTags.text = currentArtist.Tags[0];
            }
            else if (currentArtist.Tags.Count == 2)
            {
                cardTags.text = currentArtist.Tags[0] + ", " + currentArtist.Tags[1];
            }
            else
            {
                cardTags.text = currentArtist.Tags[0] + ", " + currentArtist.Tags[1] + ", " + currentArtist.Tags[2];
            }
            if (currentArtist.Currents[0] == "None")
            {
                cardCurrents.text = " ";
            }
            else if (currentArtist.Currents.Count == 1)
            {
                cardCurrents.text = currentArtist.Currents[0];
            }
            else if (currentArtist.Currents.Count == 2)
            {
                cardCurrents.text = currentArtist.Currents[0] +
                    ", " + currentArtist.Currents[1];
            }
            else if (currentArtist.Currents.Count == 3)
            {
                cardCurrents.text = currentArtist.Currents[0] +
                    ", " + currentArtist.Currents[1] +
                    ", " + currentArtist.Currents[2];
            }
            else if (currentArtist.Currents.Count == 4)
            {
                cardCurrents.text = currentArtist.Currents[0] +
                    ", " + currentArtist.Currents[1] +
                    ", " + currentArtist.Currents[2] +
                    ", " + currentArtist.Currents[3];
            }
            else if (currentArtist.Currents.Count == 5)
            {
                cardCurrents.text = currentArtist.Currents[0] +
                    ", " + currentArtist.Currents[1] +
                    ", " + currentArtist.Currents[2] +
                    ", " + currentArtist.Currents[3] +
                    ", " + currentArtist.Currents[4];
            }
            currentArtist.Locked = false;
            CardSelection detect = secondCards[i].GetComponent<CardSelection>();
            detect.unlockText = GameObject.Instantiate(unlockText, secondCards[i]);
            if (secondDraw[i].Name == "William Blake")
            {
                detect.unlockText.GetComponent<TextMeshProUGUI>().text = "Unlocks\nPoetry";
            }
            else if (secondDraw[i].Name == "Johann Wolfgang von\nGoethe")
            {
                detect.unlockText.GetComponent<TextMeshProUGUI>().text = "Unlocks\nTheatre";
            }
            else if (secondDraw[i].Name == "Antonio Canova")
            {
                detect.unlockText.GetComponent<TextMeshProUGUI>().text = "Unlocks\nSculpture";
            }
            else if (secondDraw[i].Name == "Germaine de Staël")
            {
                detect.unlockText.GetComponent<TextMeshProUGUI>().text = "Unlocks\nLiterature";
            }
        }
        // Display Cards
        if (audioOn)
        {
            sound2.clip = clips[6];
            sound2.volume = volume * 0.4f;
            sound2.Play();
        }
        currentTime = 0;
        while (currentTime <= 0.5f)
        {
            currentTime += Time.deltaTime;
            for (int i = 0; i < secondCards.Length; i++)
            {
                secondCards[i].localPosition = new Vector2(secondCards[i].localPosition.x, Mathf.Lerp(40, -81, currentTime * 4));
            }
            yield return null;
        }
        if (audioOn)
        {
            sound2.clip = clips[2];
            sound2.volume = volume;
            sound2.Play();
        }
        currentTime = 0;
        drawing = false;
        while (currentTime <= 1f)
        {
            currentTime += Time.deltaTime;
            for (int i = 0; i < secondDraw.Length; i++)
            {
                secondCards[i].localPosition = new Vector2(Mathf.Lerp(secondCards[i].localPosition.x, -(180) + (120 * i), currentTime), secondCards[i].localPosition.y);
            }
            yield return null;
        }
        for (int i = 0; i < secondCards.Length; i++)
        {
            borders[i].transform.localPosition = secondCards[i].localPosition;
        }
        secondDrawOn = true;
        yield break;
    }
    public void Beginning3()
    {
        if (audioOn)
        {
            sound.clip = clips[11];
            sound.volume = volume;
            sound.Play();
        }
        validate.gameObject.SetActive(false);
        for (int i = 0; i < borders.Length; i++)
        {
            GameObject.Destroy(borders[i].gameObject);
        }
        drawing = true;
        firstDrawOn = false;
        secondDrawOn = false;
        StartCoroutine(Beginning4());
    }
    IEnumerator Beginning4()
    {
        // Hide Cards
        float currentTime = 0;
        int domainMax = 0;
        while (currentTime <= 1)
        {
            currentTime += Time.deltaTime;
            transPanel.alpha = 1 - currentTime;
            for (int i = 0; i < secondCards.Length; i++)
            {
                if (secondCards[i] == selected[0].Card || secondCards[i] == selected[1].Card)
                {
                    secondCards[i].localPosition = new Vector2(secondCards[i].localPosition.x, Mathf.Lerp(secondCards[i].localPosition.y, -350, currentTime));
                }
                else
                {
                    secondCards[i].localPosition = new Vector2(secondCards[i].localPosition.x, Mathf.Lerp(secondCards[i].localPosition.y, 250, currentTime));
                }
            }
            yield return null;
        }
        // Swap Domains
        List<int> freeDomains = new List<int>();
        List<int> outDomains = new List<int>();
        if (selected[0].Domain != "Poetry" && selected[1].Domain != "Poetry") { freeDomains.Add(1); }
        if (selected[0].Domain != "Theatre" && selected[1].Domain != "Theatre") { freeDomains.Add(2); }
        if (selected[0].Domain == "Sculpture" || selected[0].Domain == "Narrative Literature")
        {
            int dom = 0;
            for (int i = 0; i < domain.Count; i++)
            {
                if (selected[0].Domain == domain[i])
                {
                    dom = i;
                }
            }
            outDomains.Add(dom);
        }
        if (selected[1].Domain == "Sculpture" || selected[1].Domain == "Narrative Literature")
        {
            int dom = 0;
            for (int i = 0; i < domain.Count; i++)
            {
                if (selected[1].Domain == domain[i])
                {
                    dom = i;
                }
            }
            outDomains.Add(dom);
        }
        for (int i = 0; i < freeDomains.Count; i++)
        {
            ArtistList tempArtistDomainLocked = artistDomainLocked[freeDomains[i]];
            ArtistList tempArtistDomainAvailable = artistDomainAvailable[freeDomains[i]];
            ArtistList tempArtistDomainActive = artistDomainActive[freeDomains[i]];
            ArtistList tempShamefulActive = shamefulInfluenceAvailable[freeDomains[i]];
            string tempDomain = domain[freeDomains[i]];
            string tempDomainShortened = domainShortened[freeDomains[i]];
            artistDomainLocked[freeDomains[i]] = artistDomainLocked[outDomains[i]];
            artistDomainAvailable[freeDomains[i]] = artistDomainAvailable[outDomains[i]];
            artistDomainActive[freeDomains[i]] = artistDomainActive[outDomains[i]];
            shamefulInfluenceAvailable[freeDomains[i]] = shamefulInfluenceAvailable[outDomains[i]];
            domain[freeDomains[i]] = domain[outDomains[i]];
            domainShortened[freeDomains[i]] = domainShortened[outDomains[i]];
            artistDomainLocked[outDomains[i]] = tempArtistDomainLocked;
            artistDomainAvailable[outDomains[i]] = tempArtistDomainAvailable;
            artistDomainActive[outDomains[i]] = tempArtistDomainActive;
            shamefulInfluenceAvailable[outDomains[i]] = tempShamefulActive;
            domain[outDomains[i]] = tempDomain;
            domainShortened[outDomains[i]] = tempDomainShortened;
        }
        for (int y = 0; y < domainShops.Length; y++)
        {
            domainShops[y].transform.parent.Find("DomainName").GetComponent<TextMeshProUGUI>().text = artistDomainAvailable[y].name;
        }
        save.freeDomains = freeDomains;
        save.outDomains = outDomains;
        // Read Cards
        for (int y = 0; y < selected.Count; y++)
        {
            int domain = 0;
            for (int x = 0; x < artistDomainActive.Count; x++)
            {
                if (selected[y].Domain == artistDomainActive[x].name)
                {
                    domain = x;
                    if (domain > domainMax)
                    {
                        domainMax = domain;
                    }
                }
            }
            cardBoxes[domain].parent.GetComponent<BoxMouse>().cardNumber++;
            // Change Era
            domainShops[domain].era = 1;
            domainShops[domain].availableCards += 10;
            save.eraList[domain] = 1;
            erasPassed++;
            statisticTexts[2].text = "<b>Eras:</b> " + erasPassed + "%";
            for (int i = 0; i < artistDomainLocked[domain].Artist.Count; i++)
            {
                if (artistDomainLocked[domain].Artist[i].Era == domainShops[0].era)
                {
                    artistDomainAvailable[domain].Artist.Add(artistDomainLocked[domain].Artist[i]);
                    artistDomainLocked[domain].Artist.Remove(artistDomainLocked[domain].Artist[i]);
                    i--;
                }
            }
            domainShops[domain].price = EraPrice(domain, domainShops[domain]);
            scrollTexts[domain + 1].text = artistDomainAvailable[domain].name;
            scrollTexts[domain + 1].color = domainColors[0];
            Artist current = selected[y];
            Transform artistCard = current.Card;
            artistCard.GetComponent<CardSelection>().enabled = false;
            artistCard.GetComponent<EventTrigger>().enabled = false;
            for (int w = 0; w < current.Nation.Count; w++)
            {
                artistCard.Find("NationSort").GetChild(w).GetComponent<NationMouse>().cardActive = true;
            }
            artistCard.localScale = new Vector2(1, 1);
            domainShops[domain].availableCards--;
            domainShops[domain].cardText.text = domainShops[domain].availableCards.ToString();
            domainShops[domain].cardText2.text = artistDomainLocked[domain].Artist.Count.ToString();
            EraPriceDisplay(domainShops[domain]);
            artistDomainActive[domain].Artist.Add(current);
            artistDomainAvailable[domain].Artist.Remove(current);
            // Set Parents
            save.artistDomainActive[domain].Add(current.Name);
            save.isArtistLead[domain].Add(current.Lead);
            artistCard.SetParent(cardBoxes[domain]);
            current.Prod = artistCard.Find("Production").GetComponent<TextMeshProUGUI>();
            // Update Stats
            available++;
            for (int i = 0; i < current.nationRefs.Count; i++)
            {
                nations[current.nationRefs[i]].amount++;
            }
            for (int i = 0; i < current.tagRefs.Count; i++)
            {
                tags[current.tagRefs[i]].amount++;
            }
            for (int i = 0; i < current.currentRefs.Count; i++)
            {
                currents[current.currentRefs[i]].amount++;
            }
            if (current.Gender.Count > 1)
            {
                men++;
                women++;
            }
            else if (current.Gender[0] == "Female")
            {
                women++;
            }
            else
            {
                men++;
            }
            Calculation();
            // Set Cards
            Vector2 destination = new Vector2(0, 460);
            artistCard.localPosition = destination;
            for (int i = 0; i < secondCards.Length; i++)
            {
                if (secondCards[i] != selected[0].Card && secondCards[i] != selected[1].Card)
                {
                    GameObject.Destroy(secondCards[i].gameObject);
                }
            }
            GameObject detectorObject = artistCard.Find("MouseDetector").gameObject;
            detectorObject.SetActive(true);
            MouseDetector detector = detectorObject.GetComponent<MouseDetector>();
            detector.nextCard = artistCard.Find("NextCard").transform;
            detector.currentCard = artistCard;
            detector.box = cardBoxes[domain].parent.GetComponent<BoxMouse>();
            detector.cardColor = domainColors[domain];
            detector.domain = domain;
            detector.artist = current;
            if (current.Name2 == null)
            {
                detector.artistName = current.Name;
            }
            else if (current.Name3 == null && current.CommonName == null)
            {
                detector.artistName = current.Name + " & " + current.Name2;
            }
            else if (current.Name3 == null)
            {
                detector.artistName = current.Name + ", " + current.Name2 + " & " + current.Name3;
            }
            else
            {
                detector.artistName = current.Name + ", " + current.Name2 + " & " + current.Name3 + current.CommonName;
            }
            detector.effectName = new string[current.Effects.Count];
            detector.effectColor = new Color[current.Effects.Count];
            for (int i = 0; i < current.Effects.Count; i++)
            {
                detector.effectName[i] = current.Effects[i];
                Effect effectRef = effectList.Effect[current.effectRefs[i]];
                if (effectRef.Type == "Activism")
                {
                    detector.effectColor[i] = effectColors[0];
                }
                else if (effectRef.Type == "Contribution")
                {
                    detector.effectColor[i] = effectColors[1];
                }
                else if (effectRef.Type == "Feature")
                {
                    detector.effectColor[i] = effectColors[2];
                }
                else if (effectRef.Type == "Interdisciplinary current")
                {
                    detector.effectColor[i] = effectColors[3];
                }
                else if (effectRef.Type == "Area-defined current")
                {
                    detector.effectColor[i] = effectColors[4];
                }
                else if (effectRef.Type == "Domain-defined current")
                {
                    detector.effectColor[i] = effectColors[5];
                }
                else if (effectRef.Type == "Group")
                {
                    detector.effectColor[i] = effectColors[6];
                }
                else if (effectRef.Type == "Title")
                {
                    detector.effectColor[i] = effectColors[7];
                }
                else if (effectRef.Type == "Prize")
                {
                    detector.effectColor[i] = effectColors[8];
                }
                else if (effectRef.Type == "Discovery")
                {
                    detector.effectColor[i] = effectColors[9];
                }
                else if (effectRef.Type == "Genre")
                {
                    detector.effectColor[i] = effectColors[10];
                }
                else if (effectRef.Type == "Activity")
                {
                    detector.effectColor[i] = effectColors[11];
                }
                else if (effectRef.Type == "Achievement")
                {
                    detector.effectColor[i] = effectColors[12];
                }
            }
        }
        domainScroll = domainMax;
        StartCoroutine(Information());
        yield break;
    }
    IEnumerator Information()
    {
        float currentTime;
        transParagraph.alpha = 1;
        transPanel.transform.localPosition = new Vector2(37, -56);
        transClick.transform.localPosition = new Vector2(transClick.transform.localPosition.x, -20);
        transText.transform.localPosition = new Vector2(transText.transform.localPosition.x, 12);
        transTitle.transform.localPosition = new Vector2(transTitle.transform.localPosition.x, 28);
        transTitle.alpha = 0;
        transSubTitle.alpha = 0;
        RectTransform rect = transPanel.GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, 90);
        transText.fontSize = 7.5f;
        transBackground.alpha = 1;
        transText.alpha = 0;
        transClick.alpha = 0;
        transPanel.alpha = 1;
        transText.text = "Let's take a look at our group now.";
        currentTime = 0;
        while (currentTime <= 2)
        {
            currentTime += Time.deltaTime;
            transPanel.alpha = currentTime * 0.5f;
            introMenu.alpha = currentTime * 0.5f;
            transClick.alpha = currentTime * 0.5f;
            transText.alpha = currentTime * 0.5f;
            yield return null;
        }
        transPanel.alpha = 1;
        introMenu.alpha = 0;
        transText.alpha = 1;
        transClick.alpha = 1;
        click = true;
        while (click) { yield return null; }
        timeIntro = 0;
        while (timeIntro < 1)
        {
            timeIntro += Time.deltaTime;
            transClick.alpha = 1 - timeIntro;
            transText.alpha = 1 - timeIntro;
            yield return null;
        }
        transText.alpha = 1;
        Transform highlightCard = GameObject.Instantiate(highLight, domainShops[0].transform.GetChild(0)).transform;
        Transform highlightTrend = GameObject.Instantiate(highLight, domainShops[0].transform.GetChild(0)).transform;
        Transform highlightEra = GameObject.Instantiate(highLight, domainShops[0].transform.GetChild(2)).transform;
        highlightCard.SetSiblingIndex(0);
        highlightTrend.position = domainShops[0].transform.GetChild(1).position;
        highlightTrend.localScale = new Vector2(0.6f, 0.6f);
        highlightEra.SetSiblingIndex(0);
        float factor = 0.3f;
        float limitPlus = 1.2f;
        CanvasGroup previous = introMenu.transform.GetChild(0).GetComponent<CanvasGroup>();
        bool ascending = true;
        introMenu.gameObject.SetActive(true);
        while (infoOn)
        {
            for (infoRef = 0; infoRef < introTexts.Count; infoRef++)
            {
                if (!goingOn && reversing)
                {
                    infoRef -= 2;
                    if (infoRef == 0)
                    {
                        introButton = false;
                    }
                    else
                    {
                        int prevRef = infoRef - 1;
                        transText.alpha = 0;
                        transClick.alpha = 0;
                        highlightTrend.localScale = new Vector2(1f, 1f);
                        highlightTrend.gameObject.SetActive(false);
                        highlightEra.localScale = new Vector2(1f, 1f);
                        highlightEra.gameObject.SetActive(false);
                        highlightCard.localScale = new Vector2(1f, 1f);
                        highlightCard.gameObject.SetActive(false);
                        for (int i = 0; i < introPanels.Count; i++)
                        {
                            introPanels[i].alpha = 0;
                        }
                        if (prevRef == 5 || prevRef == 7 || prevRef == 10)
                        {
                            transBackground.alpha = 1;
                        }
                        else if (prevRef == 0)
                        {
                            introPanels[0].alpha = 1;
                        }
                        else if (prevRef == 1)
                        {
                            introPanels[0].alpha = 1;
                        }
                        else if (prevRef == 2)
                        {
                            introPanels[0].alpha = 1;
                        }
                        else if (prevRef == 3)
                        {
                            introPanels[1].alpha = 1;
                        }
                        else if (prevRef == 4)
                        {
                            introPanels[2].alpha = 1;
                        }
                        else if (prevRef == 8 || prevRef == 9)
                        {
                            introPanels[5].alpha = 1;
                        }
                        else if (prevRef == 13 || prevRef == 14)
                        {
                            introPanels[6].alpha = 1;
                        }
                    }
                    reversing = false;
                }
                goingOn = true;
                while (goingOn)
                {
                    transText.text = introTexts[infoRef];
                    timeIntro = 0;
                    if (infoRef == 0)
                    {
                        introPanels[0].alpha = 1;
                        introMenu.transform.GetChild(0).gameObject.SetActive(false);
                    }
                    else if (infoRef == 1)
                    {
                        introMenu.transform.GetChild(0).gameObject.SetActive(true);
                        highlightCard.gameObject.SetActive(true);
                    }
                    else if (infoRef == 2)
                    {
                        highlightEra.gameObject.SetActive(true);
                    }
                    else if (infoRef == 3)
                    {
                        highlightTrend.gameObject.SetActive(true);
                    }
                    else if (infoRef == 4)
                    {
                        introPanels[1].alpha = 1;
                    }
                    else if (infoRef == 6)
                    {
                        introPanels[3].alpha = 1;
                    }
                    else if (infoRef == 7)
                    {
                        introPanels[4].alpha = 1;
                    }
                    else if (infoRef == 8)
                    {
                        introPanels[5].alpha = 1;
                    }
                    else if (infoRef == 11)
                    {
                        transBackground.alpha = 1;
                    }
                    else if (infoRef == 12)
                    {
                        introPanels[6].alpha = 1;
                    }
                    while (timeIntro < 1)
                    {
                        timeIntro += Time.deltaTime;
                        if (infoRef == 1)
                        {
                            previous.alpha = timeIntro;
                            if (highlightCard.localScale.x >= limitPlus)
                            {
                                ascending = false;
                            }
                            if (highlightCard.localScale.x <= 1)
                            {
                                ascending = true;
                            }
                            if (ascending)
                            {
                                highlightCard.localScale = new Vector2(highlightCard.localScale.x + Time.deltaTime * factor, highlightCard.localScale.y + Time.deltaTime * factor);
                            }
                            else
                            {
                                highlightCard.localScale = new Vector2(highlightCard.localScale.x - Time.deltaTime * factor, highlightCard.localScale.y - Time.deltaTime * factor);
                            }
                        }
                        else if (infoRef == 2)
                        {
                            if (highlightEra.localScale.x > limitPlus)
                            {
                                ascending = false;
                            }
                            if (highlightEra.localScale.x < 1)
                            {
                                ascending = true;
                            }
                            if (ascending)
                            {
                                highlightEra.localScale = new Vector2(highlightEra.localScale.x + Time.deltaTime * factor, highlightEra.localScale.y + Time.deltaTime * factor);
                            }
                            else
                            {
                                highlightEra.localScale = new Vector2(highlightEra.localScale.x - Time.deltaTime * factor, highlightEra.localScale.y - Time.deltaTime * factor);
                            }
                        }
                        else if (infoRef == 3)
                        {
                            if (highlightTrend.localScale.x > 1f)
                            {
                                ascending = false;
                            }
                            if (highlightTrend.localScale.x < 0.8f)
                            {
                                ascending = true;
                            }
                            if (ascending)
                            {
                                highlightTrend.localScale = new Vector2(highlightTrend.localScale.x + Time.deltaTime * factor, highlightTrend.localScale.y + Time.deltaTime * factor);
                            }
                            else
                            {
                                highlightTrend.localScale = new Vector2(highlightTrend.localScale.x - Time.deltaTime * factor, highlightTrend.localScale.y - Time.deltaTime * factor);
                            }
                        }
                        else if (infoRef == 6 || infoRef == 8 || infoRef == 12 || infoRef == 0)
                        {
                            if (infoRef == 0)
                            {
                                introMenu.alpha = timeIntro;
                            }
                            transBackground.alpha = 1 - timeIntro;
                        }
                        else if (infoRef == 4)
                        {
                            introPanels[0].alpha = 1 - timeIntro;
                        }
                        else if (infoRef == 5)
                        {
                            introPanels[1].alpha = 1 - timeIntro;
                        }
                        else if (infoRef == 7)
                        {
                            introPanels[3].alpha = 1 - timeIntro;
                        }
                        if (infoRef < 11 && transBackground.alpha > 0)
                        {
                            transBackground.alpha = 1 - timeIntro;
                        }
                        transClick.alpha = timeIntro;
                        transText.alpha = timeIntro;
                        yield return null;
                    }
                    transText.alpha = 1;
                    click = true;
                    while (click)
                    {
                        if (infoRef == 1)
                        {
                            if (highlightCard.localScale.x >= limitPlus)
                            {
                                ascending = false;
                            }
                            if (highlightCard.localScale.x <= 1)
                            {
                                ascending = true;
                            }
                            if (ascending)
                            {
                                highlightCard.localScale = new Vector2(highlightCard.localScale.x + Time.deltaTime * factor, highlightCard.localScale.y + Time.deltaTime * factor);
                            }
                            else
                            {
                                highlightCard.localScale = new Vector2(highlightCard.localScale.x - Time.deltaTime * factor, highlightCard.localScale.y - Time.deltaTime * factor);
                            }
                        }
                        else if (infoRef == 2)
                        {
                            if (highlightEra.localScale.x >= limitPlus)
                            {
                                ascending = false;
                            }
                            if (highlightEra.localScale.x <= 1)
                            {
                                ascending = true;
                            }
                            if (ascending)
                            {
                                highlightEra.localScale = new Vector2(highlightEra.localScale.x + Time.deltaTime * factor, highlightEra.localScale.y + Time.deltaTime * factor);
                            }
                            else
                            {
                                highlightEra.localScale = new Vector2(highlightEra.localScale.x - Time.deltaTime * factor, highlightEra.localScale.y - Time.deltaTime * factor);
                            }
                        }
                        else if (infoRef == 3)
                        {
                            if (highlightTrend.localScale.x > limitPlus)
                            {
                                ascending = false;
                            }
                            if (highlightTrend.localScale.x < 1)
                            {
                                ascending = true;
                            }
                            if (ascending)
                            {
                                highlightTrend.localScale = new Vector2(highlightTrend.localScale.x + Time.deltaTime * factor, highlightTrend.localScale.y + Time.deltaTime * factor);
                            }
                            else
                            {
                                highlightTrend.localScale = new Vector2(highlightTrend.localScale.x - Time.deltaTime * factor, highlightTrend.localScale.y - Time.deltaTime * factor);
                            }
                        }
                        yield return null;
                    }
                    timeIntro = 0;
                    while (timeIntro < 1)
                    {
                        timeIntro += Time.deltaTime;
                        transText.alpha = 1 - timeIntro;
                        transClick.alpha = 1 - timeIntro;
                        if (infoRef == 5 || infoRef == 7 || infoRef == 10 || infoRef == introTexts.Count - 1)
                        {
                            transBackground.alpha = timeIntro;
                            if (infoRef == introTexts.Count - 1)
                            {
                                introMenu.alpha = 1 - timeIntro;
                            }
                        }
                        if (infoRef == 1)
                        {
                            highlightCard.localScale = Vector2.Lerp(highlightCard.localScale, new Vector2(0.9f, 0.9f), timeIntro);
                        }
                        else if (infoRef == 2)
                        {
                            highlightEra.localScale = Vector2.Lerp(highlightEra.localScale, new Vector2(0.9f, 0.9f), timeIntro);
                        }
                        else if (infoRef == 3)
                        {
                            highlightTrend.localScale = Vector2.Lerp(highlightTrend.localScale, new Vector2(0.6f, 0.6f), timeIntro);
                            introPanels[1].alpha = timeIntro;
                        }
                        else if (infoRef == 4)
                        {
                            introPanels[2].alpha = timeIntro;
                        }
                        yield return null;
                    }
                    transText.alpha = 0;
                    if (infoRef == 5 || infoRef == 7 || infoRef == 10)
                    {
                        transBackground.alpha = 1;
                        for (int i = 0; i < introPanels.Count; i++)
                        {
                            introPanels[i].alpha = 0;
                        }
                    }
                    else if (infoRef == 1)
                    {
                        highlightCard.gameObject.SetActive(false);
                    }
                    else if (infoRef == 2)
                    {
                        highlightEra.gameObject.SetActive(false);
                    }
                    else if (infoRef == 3)
                    {
                        highlightTrend.gameObject.SetActive(false);
                    }
                    else if (infoRef == introTexts.Count - 1)
                    {
                        infoOn = false;
                    }
                    goingOn = false;
                }
            }
            yield return null;
        }
        for (int i = 0; i < introPanels.Count; i++)
        {
            introPanels[i].gameObject.SetActive(false);
        }
        introPanels.Clear();
        GameObject.Find("IntroSafe").SetActive(false);
        introMenu.gameObject.SetActive(false);
        highlightTrend.gameObject.SetActive(false);
        highlightEra.gameObject.SetActive(false);
        highlightCard.gameObject.SetActive(false);
        StartCoroutine(EndBeginning());
        yield break;
    }
    public void EndTuto()
    {
        if (audioOn && goingOn)
        {
            sound2.clip = clips[10];
            sound2.volume = volume * 0.25f;
            sound2.Play();
        }
        introButton = false;
        infoRef = introTexts.Count - 1;
        click = false;
    }
    public void PreviousTuto()
    {
        if (audioOn && introButton)
        {
            sound2.clip = clips[12];
            sound2.volume = volume * 0.25f;
            sound2.Play();
        }
        reversing = true;
        goingOn = false;
        click = false;
    }
    public void ButtonIntro()
    {
        introButton = true;
    }
    public void ButtonIntroFalse()
    {
        introButton = false;
    }
    IEnumerator EndBeginning()
    {
        // Display Text
        transText.fontSize = 8;
        transClick.alpha = 0;
        transText.alpha = 0;
        transText.alignment = TextAlignmentOptions.Center;
        transText.text = "\n\nEverything is now set for your work to begin.";
        // Hide Intro
        gameOn = true;
        float currentTime = 0;
        while (currentTime <= 1)
        {
            currentTime += Time.deltaTime;
            transText.alpha = currentTime;
            yield return null;
        }
        save.loadOn = true;
        save.newsEra = 1;
        currentTime = 0;
        while (currentTime <= 2)
        {
            currentTime += Time.deltaTime;
            transBackground.alpha = 1 - currentTime * 0.5f;
            transClick.alpha = currentTime * 0.5f;
            yield return null;
        }
        transBackground.alpha = 0;
        transBackground.gameObject.SetActive(false);
        drawing = false;
        click = true;
        while (click) { yield return null; }
        currentTime = 0;
        while (currentTime <= 2)
        {
            currentTime += Time.deltaTime;
            transPanel.alpha = 1 - currentTime * 0.5f;
            yield return null;
        }
        transText.alpha = 0;
        transPanel.alpha = 0;
        transPanel.gameObject.SetActive(false);
        yield break;
    }
}
