﻿using System.Collections.Generic;

[System.Serializable]
public class Shame
{
    public string name;
    public int amount;
    public int total;
    public Shame(string AttributeName, int AttributeAmount)
    {
        name = AttributeName;
        total = AttributeAmount;
    }
}
