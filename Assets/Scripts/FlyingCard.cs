﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class FlyingCard : MonoBehaviour
{
    private ListMaker script;
    public MouseDetector mouse;
    Scroll scroll;
    public bool drag = false;
    public bool global = false;
    public int domain;
    float currentTime = 0.1f;
    Vector2 newScale = new Vector2(1f, 1f);
    Image ray;
    private void Start()
    {
        ray = this.gameObject.GetComponent<Image>();
        scroll = GameObject.Find("DomainSort").GetComponent<Scroll>();
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
    }
    private void Update()
    {
        if (currentTime < 0.1f)
        {
            currentTime += Time.deltaTime;
            this.transform.localScale = Vector2.Lerp(this.transform.localScale, newScale, currentTime * 10);
        }
    }
    public void Drag()
    {
        ray.raycastTarget = false;
        if (global)
        {
            script.HeaderDropAvailable = false;
        }
        else
        {
            script.DomainDropAvailable = false;
        }
        mouse.Drag();
    }
    public void Drop()
    {
        if (drag)
        {
            mouse.Drop();
        }
        drag = false;
    }
    public void In()
    {
        if (!script.drawOn && script.displayOn && !script.drawing && !scroll.moving && mouse.artist.Lead != 0)
        {
            currentTime = 0;
            newScale = new Vector2(1.05f, 1.05f);
            script.panel.gameObject.SetActive(true);
            script.panelText.fontSize = 5f;
            if (global)
            {
                script.panelTitle.text = "Global Trendsetter";
                script.panelText.text = "Drag and drop an Artist's name here to have their Effects enhanced for each other enrolled Artist.";
            }
            else
            {
                script.panelTitle.text = "Domain's Trendsetter";
                if (script.domainProduction[domain] > 1000000000)
                {
                    script.panelText.text = "Drag and drop an Artist's name here to have their Effects enhanced for each other enrolled Artist in the Domain.\n\n<b>Current Domain Production:</b> " + Math.Round(script.domainProduction[domain] * 0.000000001f, 1) + "B";
                }
                else if (script.domainProduction[domain] > 1000000)
                {
                    script.panelText.text = "Drag and drop an Artist's name here to have their Effects enhanced for each other enrolled Artist in the Domain.\n\n<b>Current Domain Production:</b> " + Math.Round(script.domainProduction[domain] * 0.000001f, 1) + "M";
                }
                else if (script.domainProduction[domain] > 1000)
                {
                    script.panelText.text = "Drag and drop an Artist's name here to have their Effects enhanced for each other enrolled Artist in the Domain.\n\n<b>Current Domain Production:</b> " + Math.Round(script.domainProduction[domain] * 0.001f, 1) + "K";
                }
                else
                {
                    script.panelText.text = "Drag and drop an Artist's name here to have their Effects enhanced for each other enrolled Artist in the Domain.\n\n<b>Current Domain Production:</b> " + Math.Round(script.domainProduction[domain], 1);
                }
            }
            script.panelDisplay = true;
        }
    }
    public void Out()
    {
        currentTime = 0;
        newScale = new Vector2(1f, 1f);
        script.panel.gameObject.SetActive(false);
        script.panelTitle.text = "";
        script.panelText.text = "";
        script.panelDisplay = false;
    }
}
