﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GaugeMouse : MonoBehaviour
{
    ListMaker script;
    void Start()
    {
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
    }
    public void MouseIn()
    {
        if (!script.drawOn && script.displayOn && !script.drawing)
        {
            script.panel.gameObject.SetActive(true);
            script.panelText.fontSize = 5f;
            script.panelTitle.text = "Gauge";
            script.panelText.text = "The Gauge measures how ethical your Arts Society is, and its level affects your production. If you lack diversity and enroll privileged folks only, it decreases. If you enroll activists, it increases.";
            script.panelDisplay = true;
        }
    }
    public void MouseOut()
    {
        script.panel.gameObject.SetActive(false);
        script.panelTitle.text = "";
        script.panelText.text = "";
        script.panelDisplay = false;
    }
}
