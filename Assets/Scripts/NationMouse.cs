﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NationMouse : MonoBehaviour
{
    ListMaker script;
    public bool active = false;
    public string nation;
    bool wasActive = false;
    public bool cardActive = false;
    string previousMessage;
    string previousTitle;
    public Transform pos;
    void Start()
    {
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
    }
    public void MouseIn()
    {
        if (active && (!cardActive || (!script.drawing && !script.drawOn)))
        {
            script.nationPanel.gameObject.SetActive(true);
            if (script.panel.gameObject.activeInHierarchy)
            {
                wasActive = true;
                script.panel.gameObject.SetActive(false);
            }
            else
            {
                script.nationPanel.gameObject.SetActive(true);
            }
            if (cardActive)
            {
                script.nationPanel.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y + 1f, 1000);
            }
            else
            {
                script.nationPanel.position = new Vector3(pos.position.x, pos.position.y + 2.5f, 1000);
            }
            script.nationText.text = nation;
        }
    }
    public void MouseOut()
    {
        if (wasActive)
        {
            script.panel.gameObject.SetActive(true);
            wasActive = false;
        }
        script.nationText.text = "";
        script.nationPanel.gameObject.SetActive(false);
    }
}
