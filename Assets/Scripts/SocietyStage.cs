﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class SocietyStage
{
    public string Name;
    public int Population;
}
