﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Save
{
    public List<List<string>> artistDomainActive = new List<List<string>>();
    public List<List<int>> isArtistLead = new List<List<int>>();
    public List<string> shamefulInfluenceActive = new List<string>();
    public List<int> eraList = new List<int>();
    public List<string> activeItems = new List<string>();
    public List<string> availableItems = new List<string>();
    public List<int> freeDomains = new List<int>();
    public List<int> outDomains = new List<int>();
    public int newsEra;
    public int currentSociety;
    public double stock;
    public bool audioOn;
    public bool displayOn;
    public bool loadOn;
}
