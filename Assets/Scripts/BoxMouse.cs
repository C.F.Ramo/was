﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxMouse : MonoBehaviour
{
    Transform cardSort;
    private Scroll scroll;
    bool mouse = false;
    bool down;
    public bool moving = false;
    readonly float min = -460f;
    //max = 455f;
    public int cardNumber;
    float currentPos;
    float currentTime;
    float multiplicator = 1;
    public float amount;
    public float delay;
    void Start()
    {
        scroll = GameObject.Find("DomainSort").GetComponent<Scroll>();
        cardSort = this.transform.Find("CardSort");
        currentPos = cardSort.localPosition.y;
    }
    public void In()
    {
        mouse = true;
        scroll.mouse = false;
    }
    public void Out()
    {
        mouse = false;
        scroll.mouse = true;
    }
    private void Update()
    {
        if (mouse)
        {
            if (Input.mouseScrollDelta.y < 0 && (currentPos + amount * multiplicator) <  (min + 110 + (cardNumber * 18)))
            {
                if (!down) { multiplicator = 1; }
                currentTime = 0;
                currentPos += amount * multiplicator;
                multiplicator++;
                down = true;
            }
            else if (Input.mouseScrollDelta.y > 0 && (currentPos - amount * multiplicator) > min)
            {
                if (down) { multiplicator = 1; }
                currentTime = 0;
                currentPos -= amount * multiplicator;
                multiplicator++;
                down = false;
            }
        }
        if (currentTime <= delay)
        {
            currentTime += Time.deltaTime;
            cardSort.localPosition = new Vector2(cardSort.localPosition.x, Mathf.Lerp(cardSort.localPosition.y, currentPos, currentTime / delay));
            moving = true;
        }
        else
        {
            moving = false;
            if (multiplicator > 1)
            {
                multiplicator--;
            }
            cardSort.localPosition = new Vector2(cardSort.localPosition.x, currentPos);
        }
    }
}
