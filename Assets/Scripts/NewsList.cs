﻿using System.Collections.Generic;

[System.Serializable]
public class NewsList
{
    public List<News> News = new List<News>();
}
