﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Scrollbutton : MonoBehaviour, IDragHandler
{
    ListMaker script;
    [SerializeField] public RectTransform rect;
    [SerializeField] public Scroll cam;
    [SerializeField] public Canvas canvas;
    private readonly float max = 60.9f;
    private readonly float min = -60.16f;
    private float interval;
    readonly float[] intervals = new float[20];
    float origin;
    float currentPos;
    int stage;
    private void Start()
    {
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
        stage = 1;
        origin = rect.anchoredPosition.y;
        currentPos = origin;
        interval = (max - min) / 19;
        for (int i = 0; i < intervals.Length; i++)
        {
            intervals[i] = origin - (interval * i);
        }
    }
    public void OnDrag(PointerEventData eventData)
    {
        currentPos += eventData.delta.y / canvas.scaleFactor;
    }
    public void Update()
    {
        if (stage > 0 && !cam.moving)
        {
            if (currentPos > intervals[stage - 1])
            {
                if (script.audioOn)
                {
                    script.sound2.clip = script.clips[1];
                    script.sound2.volume = script.volume * 1.5f;
                    script.sound2.Play();
                }
                stage--;
                cam.stage = stage;
                cam.currentTime = 0;
            }
        }
        if (stage < intervals.Length - 1 && !cam.moving)
        {
            if (currentPos < intervals[stage + 1] && stage < script.domainScroll)
            {
                if (script.audioOn)
                {
                    script.sound2.clip = script.clips[1];
                    script.sound2.volume = script.volume * 1.5f;
                    script.sound2.Play();
                }
                stage++;
                cam.stage = stage;
                cam.currentTime = 0;
            }
        }
        if (cam.moving && cam.stage != stage)
        {
            stage = cam.stage;
            currentPos = origin - (stage * interval);
        }
        rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, origin - (stage * interval));
    }
}