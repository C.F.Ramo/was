﻿using System.Collections.Generic;

[System.Serializable]
public class News
{
    public int Era;
    public string Text;
    public string Type;
    public bool played = false;
}
