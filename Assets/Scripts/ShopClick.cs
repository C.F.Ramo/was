﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ShopClick : MonoBehaviour
{
    private ListMaker script;
    public Item current;
    float currentTime = 0.5f;
    Vector2 newScale = new Vector2(1f, 1f);
    void Start()
    {
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
    }
    private void Update()
    {
        if (currentTime < 0.25f)
        {
            currentTime += Time.deltaTime;
            transform.localScale = Vector2.Lerp(transform.localScale, newScale, currentTime * 4);
        }
    }
    public void Click()
    {
        if (script.stock > current.Price)
        {
            if (script.audioOn)
            {
                script.sound2.clip = script.clips[9];
                script.sound2.volume = script.volume;
                script.sound2.Play();
            }
            script.stock -= current.Price;
            StartCoroutine(script.BuyItem(current));
        }
        else
        {
            if (script.audioOn)
            {
                script.sound2.clip = script.clips[7];
                script.sound2.volume = script.volume;
                script.sound2.Play();
            }
            script.panel.gameObject.SetActive(true);
            script.panelTitle.text = "Not enough ames!";
            script.panelText.text = "You need " + Math.Round(current.Price - script.stock) + " more ames before you can unlock this Current.";
        }
    }
    public void MouseIn()
    {
        if (script.audioOn)
        {
            script.sound.clip = script.clips[3];
            script.sound.volume = script.volume * 0.25f;
            script.sound.Play();
        }
        currentTime = 0;
        newScale = new Vector2(1.1f, 1.1f);
        script.overDraw = true;
        script.panel.gameObject.SetActive(true);
        script.panelTitle.text = current.Name;
        if (current.SubGenders.Count > 1)
        {
            script.panelText.fontSize = 5.5f;
            if (current.Price > 1000000000)
            {
                script.panelText.text = "Linked Artists: " + current.Total + "\n\nIncluded Currents: " + current.SubGenders.Count + "\n\nPrice: " + Math.Round(current.Price * 0.000000001f) + "B";
            }
            else if (current.Price > 1000000)
            {
                script.panelText.text = "Linked Artists: " + current.Total + "\n\nIncluded Currents: " + current.SubGenders.Count + "\n\nPrice: " + Math.Round(current.Price * 0.000001f) + "M";
            }
            else
            {
                script.panelText.text = "Linked Artists: " + current.Total + "\n\nIncluded Currents: " + current.SubGenders.Count + "\n\nPrice: " + Math.Round(current.Price * 0.001f) + "K";
            }
        }
        else
        {
            script.panelText.fontSize = 6f;
            if (current.Price > 1000000000)
            {
                script.panelText.text = "Linked Artists: " + current.Total + "\n\nPrice: " + Math.Round(current.Price * 0.000000001f) + "B";
            }
            else if (current.Price > 1000000)
            {
                script.panelText.text = "Linked Artists: " + current.Total + "\n\nPrice: " + Math.Round(current.Price * 0.000001f) + "M";
            }
            else
            {
                script.panelText.text = "Linked Artists: " + current.Total + "\n\nPrice: " + Math.Round(current.Price * 0.001f) + "K";
            }
        }
        if (!script.drawOn)
        {
            script.panelDisplay = true;
        }
        else
        {
            script.panel.position = new Vector3(-3.85f, 1.5f, 1000);
        }
    }
    public void MouseOut()
    {
        currentTime = 0;
        newScale = new Vector2(1f, 1f);
        script.overDraw = false;
        script.panelText.fontSize = 5f;
        script.panel.gameObject.SetActive(false);
        script.panelTitle.text = "";
        script.panelText.text = "";
        script.panelDisplay = false;
    }
}
