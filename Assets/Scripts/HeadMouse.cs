﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadMouse : MonoBehaviour
{
    private ListMaker script;
    void Start()
    {
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
    }
    public void StatsIn()
    {
        if (!script.drawOn && script.displayOn && !script.drawing)
        {
            script.panel.gameObject.SetActive(true);
            script.panelText.fontSize = 5f;
            script.panelTitle.text = "Ratios and Statistics";
            script.panelText.text = "If positive, the Gauge multiplies your output one time for each pack of 10 gauge points. If negative, it divides your output instead. Some Effects multiply its influence.";
            script.panelDisplay = true;
        }
    }
    public void StatsOut()
    {
        script.panel.gameObject.SetActive(false);
        script.panelTitle.text = "";
        script.panelText.text = "";
        script.panelDisplay = false;
    }
    public void ProdIn()
    {
        if (!script.drawOn && script.displayOn && !script.drawing && !script.confirmOn && !script.menuOn)
        {
            script.panel.gameObject.SetActive(true);
            script.panelText.fontSize = 5f;
            script.panelTitle.text = "Ames production";
            script.panelText.text = "Ames stand for Aesthetic Measures of Experience. They are a way to quantify what Artists produce. If you produce enough, you may enroll more Artists, or unlock new Currents and Eras.";
            script.panelDisplay = true;
        }
    }
    public void ProdOut()
    {
        script.panel.gameObject.SetActive(false);
        script.panelTitle.text = "";
        script.panelText.text = "";
        script.panelDisplay = false;
    }
    public void MenuIn()
    {
        if (!script.drawOn && script.displayOn && !script.drawing && !script.confirmOn && !script.menuOn)
        {
            script.panel.gameObject.SetActive(true);
            script.panelText.fontSize = 5f;
            script.panelTitle.text = "Menu";
            script.panelText.text = "This button opens and closes the menu.";
            script.panelDisplay = true;
        }
    }
    public void MenuOut()
    {
        script.panel.gameObject.SetActive(false);
        script.panelTitle.text = "";
        script.panelText.text = "";
        script.panelDisplay = false;
    }
    public void CardsIn()
    {
        if (!script.drawOn && script.displayOn && !script.drawing)
        {
            script.panel.gameObject.SetActive(true);
            script.panelText.fontSize = 5f;
            script.panelTitle.text = "Global Trendsetter";
            script.panelText.text = "Drag and drop an Artist's name here to have their Effects enhanced for each other enrolled Artist.";
            script.panelDisplay = true;
        }
    }
    public void CardsOut()
    {
        script.panel.gameObject.SetActive(false);
        script.panelTitle.text = "";
        script.panelText.text = "";
        script.panelDisplay = false;
    }
    public void ScrollIn()
    {
        if (!script.drawOn && script.displayOn && !script.drawing)
        {
            script.panel.gameObject.SetActive(true);
            script.panelText.fontSize = 5f;
            script.panelTitle.text = "Domains";
            script.panelText.text = "These are the domains you already unlocked. They match the columns on the right, where the Artists meet. The first column, \"Horizons\" shows a list of your unlocked Currents.";
            script.panelDisplay = true;
        }
    }
    public void ScrollOut()
    {
        script.panel.gameObject.SetActive(false);
        script.panelTitle.text = "";
        script.panelText.text = "";
        script.panelDisplay = false;
    }
}
